import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateEvaluasiInovasiDaerahTable1691567938146
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'evaluasi_inovasi_daerah',
        columns: [
          {
            name: 'id',
            type: 'bigserial',
            isPrimary: true,
            primaryKeyConstraintName: 'pk_evaluasi_inovasi_daerah'
          },
          { name: 'document_id', type: 'bigint', isNullable: true },
          {
            name: 'inovasi_indikator_id',
            type: 'bigint',
            isNullable: true
          },
          { name: 'data_saat_ini', type: 'varchar', isNullable: true },
          { name: 'catatan', type: 'varchar', isNullable: true },
          { name: 'keterangan', type: 'varchar', isNullable: true },
          { name: 'tentang', type: 'varchar', isNullable: true },
          { name: 'nomor_surat', type: 'varchar', isNullable: true },
          { name: 'nomor', type: 'varchar', isNullable: true },
          { name: 'tanggal_surat', type: 'date', isNullable: true },
          { name: 'created_by', type: 'varchar' },
          { name: 'updated_by', type: 'varchar' },
          { name: 'created_at', type: 'timestamp', default: `now()` },
          { name: 'updated_at', type: 'timestamp', default: `now()` }
        ],
        foreignKeys: [
          {
            columnNames: ['inovasi_indikator_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'inovasi_indikator',
            onDelete: 'cascade'
          },
          {
            columnNames: ['document_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'files',
            onDelete: 'cascade'
          },
          {
            columnNames: ['created_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['updated_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(
      new Table({ name: 'evaluasi_inovasi_daerah' }),
      true
    );
  }
}
