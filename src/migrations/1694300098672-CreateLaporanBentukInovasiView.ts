import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateLaporanBentukInovasiView1694300098672
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `create or replace view laporan_bentuk_inovasi as select
            gi.pemda_id,
            gi.innovation_form as bentuk_inovasi,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_form) = lower(gi.innovation_form) and lower(rid.status) = 'accept' and gi2.pemda_id = gi.pemda_id) total_disetujui,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_form) = lower(gi.innovation_form) and (lower(rid.status) = 'pending' or lower(rid.status) = 'rejected') and gi2.pemda_id = gi.pemda_id) total_ditolak,
            (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where lower(gi2.innovation_form) = lower(gi.innovation_form) and gi2.pemda_id = gi.pemda_id) total_keseluruhan
        from profil_pemda pp
        right join government_innovations gi on gi.pemda_id = pp.id
        group by gi.innovation_form, gi.pemda_id`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`drop view laporan_bentuk_inovasi`);
  }
}
