import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class UpdatePemdaIndikatorTable1693037725871
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('pemda_indikator', [
      new TableColumn({
        name: 'skor_verifikasi',
        type: 'numeric',
        isNullable: true
      }),
      new TableColumn({
        name: 'data_saat_ini',
        type: 'varchar',
        isNullable: true
      })
    ]);

    await queryRunner.changeColumn(
      'pemda_indikator',
      'nilai',
      new TableColumn({ name: 'skor', type: 'numeric', isNullable: true })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      'pemda_indikator',
      'skor',
      new TableColumn({ name: 'nilai', type: 'numeric', isNullable: true })
    );

    await queryRunner.dropColumns('pemda_indikator', [
      new TableColumn({
        name: 'skor_verifikasi',
        type: 'numeric',
        isNullable: true
      }),
      new TableColumn({
        name: 'data_saat_ini',
        type: 'varchar',
        isNullable: true
      })
    ]);
  }
}
