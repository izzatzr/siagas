import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class UpdatePemdaIndikatorTable1691464585896
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('pemda_indikator', [
      new TableColumn({
        name: 'keterangan',
        type: 'text',
        isNullable: true
      }),
      new TableColumn({
        name: 'catatan',
        type: 'text',
        isNullable: true
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('pemda_indikator', [
      new TableColumn({
        name: 'keterangan',
        type: 'text',
        isNullable: true
      }),
      new TableColumn({
        name: 'catatan',
        type: 'text',
        isNullable: true
      })
    ]);
  }
}
