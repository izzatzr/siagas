import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateIndicatorTable1678344294279 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'indicators',
        columns: [
          {
            name: 'id',
            type: 'serial4'
          },
          {
            name: 'serial_number',
            type: 'bigint',
            isNullable: true
          },
          {
            name: 'type',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'indicator',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true
          },
          {
            name: 'supporting_data',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'file_type',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'document_form',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'file_format',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'value',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'indicator_type',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'group',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'parent',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'sub',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'mandatory',
            type: 'varchar',
            length: '255',
            isNullable: true
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey('indicators', ['id'], 'pk_indicators');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('indicators', true);
  }
}
