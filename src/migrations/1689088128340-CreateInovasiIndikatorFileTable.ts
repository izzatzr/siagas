import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateInovasiIndikatorFileTable1689088128340
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'inovasi_indikator_file',
        columns: [
          { name: 'id', type: 'bigserial', isPrimary: true },
          { name: 'inovasi_indikator_id', type: 'bigint', isNullable: true },
          { name: 'file_id', type: 'bigint', isNullable: true },
          { name: 'nomor_surat', type: 'varchar', isNullable: true },
          { name: 'tanggal_surat', type: 'date', isNullable: true },
          { name: 'nama_surat', type: 'varchar', isNullable: true },
          { name: 'created_by', type: 'varchar' },
          { name: 'updated_by', type: 'varchar' },
          { name: 'created_at', type: 'timestamp', default: `now()` },
          { name: 'updated_at', type: 'timestamp', default: `now()` }
        ],
        foreignKeys: [
          {
            columnNames: ['inovasi_indikator_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'inovasi_indikator',
            onDelete: 'cascade'
          },
          {
            columnNames: ['file_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'files',
            onDelete: 'cascade'
          },
          {
            columnNames: ['created_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['updated_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(
      new Table({ name: 'inovasi_indikator_file' }),
      true
    );
  }
}
