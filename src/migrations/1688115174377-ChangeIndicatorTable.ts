import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class ChangeIndicatorTable1688115174377 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('indicators', [
      'serial_number',
      'type',
      'indicator',
      'description',
      'supporting_data',
      'file_type',
      'document_form',
      'file_format',
      'value',
      'indicator_type',
      'group',
      'parent_id',
      'mandatory',
      'created_at',
      'updated_at'
    ]);

    await queryRunner.addColumns('indicators', [
      new TableColumn({
        name: 'no_urut',
        type: 'bigint',
        isNullable: true
      }),
      new TableColumn({
        name: 'jenis_indikator',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'nama_indikator',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'keterangan',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'nama_dokumen_pendukung',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'bobot',
        type: 'bigint',
        isNullable: true
      }),
      new TableColumn({
        name: 'jenis_file',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'parent',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'mandatory',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'created_by',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'updated_by',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'created_at',
        type: 'timestamp',
        default: `now()`
      }),
      new TableColumn({
        name: 'updated_at',
        type: 'timestamp',
        default: `now()`
      })
    ]);

    await queryRunner.createForeignKeys('indicators', [
      new TableForeignKey({
        columnNames: ['created_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users'
      }),
      new TableForeignKey({
        columnNames: ['updated_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('indicators', [
      new TableForeignKey({
        columnNames: ['created_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users'
      }),
      new TableForeignKey({
        columnNames: ['updated_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users'
      })
    ]);

    await queryRunner.dropColumns('indicators', [
      new TableColumn({
        name: 'no_urut',
        type: 'bigint',
        isNullable: true
      }),
      new TableColumn({
        name: 'jenis_indikator',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'nama_indikator',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'keterangan',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'nama_dokumen_pendukung',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'bobot',
        type: 'bigint',
        isNullable: true
      }),
      new TableColumn({
        name: 'jenis_file',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'parent',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'mandatory',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'created_by',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'updated_by',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'created_at',
        type: 'timestamp',
        default: `now()`
      }),
      new TableColumn({
        name: 'updated_at',
        type: 'timestamp',
        default: `now()`
      })
    ]);

    await queryRunner.addColumns('indicators', [
      new TableColumn({
        name: 'id',
        type: 'serial4'
      }),
      new TableColumn({
        name: 'serial_number',
        type: 'bigint',
        isNullable: true
      }),
      new TableColumn({
        name: 'type',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'indicator',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'description',
        type: 'text',
        isNullable: true
      }),
      new TableColumn({
        name: 'supporting_data',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'file_type',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'document_form',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'file_format',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'value',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'indicator_type',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'group',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'parent_id',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'mandatory',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'created_at',
        type: 'timestamp',
        default: 'now()'
      }),
      new TableColumn({
        name: 'updated_at',
        type: 'timestamp',
        default: 'now()'
      })
    ]);
  }
}
