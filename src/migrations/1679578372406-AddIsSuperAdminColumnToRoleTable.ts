import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddIsSuperAdminColumnToRoleTable1679578372406
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'roles',
      new TableColumn({
        name: 'is_super_admin',
        type: 'bpchar',
        enum: ['y', 't'],
        default: `'t'`
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('roles', 'is_super_admin');
  }
}
