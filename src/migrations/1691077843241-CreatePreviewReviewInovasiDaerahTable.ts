import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreatePreviewReviewInovasiDaerahTable1691077843241
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'preview_review_inovasi_daerah',
        columns: [
          {
            name: 'id',
            type: 'bigserial',
            isPrimary: true,
            primaryKeyConstraintName: 'pk_preview_review_inovasi_daerah'
          },
          {
            name: 'komentar',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'review_inovasi_daerah_id',
            type: 'bigint',
            isNullable: true
          },
          { name: 'created_by', type: 'varchar' },
          { name: 'updated_by', type: 'varchar' },
          { name: 'created_at', type: 'timestamp', default: `now()` },
          { name: 'updated_at', type: 'timestamp', default: `now()` }
        ],
        foreignKeys: [
          {
            columnNames: ['review_inovasi_daerah_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'review_inovasi_daerah',
            onDelete: 'cascade'
          },
          {
            columnNames: ['created_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['updated_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(
      new Table({ name: 'preview_review_inovasi_daerah' }),
      true
    );
  }
}
