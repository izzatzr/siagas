import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreatePaktaIntegritasTable1688786606530
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'pakta_integritas',
        columns: [
          { name: 'id', type: 'bigserial', isPrimary: true },
          { name: 'user_id', type: 'bigint', isNullable: true },
          { name: 'upload_id', type: 'bigint', isNullable: true },
          { name: 'created_by', type: 'varchar' },
          { name: 'updated_by', type: 'varchar' },
          { name: 'created_at', type: 'timestamp', default: `now()` },
          { name: 'updated_at', type: 'timestamp', default: `now()` }
        ],
        foreignKeys: [
          {
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['created_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['updated_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['upload_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'files'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'pakta_integritas' }), true);
  }
}
