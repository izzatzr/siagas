import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddLabelInRole1699079012362 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn('roles', new TableColumn({ name: 'label', type: 'varchar', isNullable: true }));
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropColumn('roles', new TableColumn({ name: 'label', type: 'varchar', isNullable: true }));
	}
}
