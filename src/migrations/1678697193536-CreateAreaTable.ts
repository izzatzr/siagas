import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex
} from 'typeorm';

export class CreateAreaTable1678697193536 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'areas',
        columns: [
          {
            name: 'id',
            type: 'serial8'
          },
          {
            name: 'region_id',
            type: 'int'
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey('areas', ['id'], 'pk_areas');

    await queryRunner.createForeignKey(
      'areas',
      new TableForeignKey({
        columnNames: ['region_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'regions',
        name: 'fk_areas_region_id',
        onDelete: 'cascade'
      })
    );

    await queryRunner.createIndex(
      'areas',
      new TableIndex({ columnNames: ['name'], name: 'idx_areas_name' })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('areas', true);
  }
}
