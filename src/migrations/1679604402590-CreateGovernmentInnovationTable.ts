import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateGovernmentInnovationTable1679604402590
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'government_innovations',
        columns: [
          {
            name: 'id',
            type: 'serial8',
            isPrimary: true,
            primaryKeyConstraintName: 'pk_government_innovations'
          },
          {
            name: 'government_name',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'created_by',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'innovation_name',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'innovation_phase',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'innovation_initiator',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'innovation_type',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'innovation_form',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'thematic',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'first_field',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'other_fields',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'trial_time',
            type: 'date',
            isNullable: true
          },
          {
            name: 'implementation_time',
            type: 'date',
            isNullable: true
          },
          {
            name: 'design',
            type: 'text',
            isNullable: true
          },
          {
            name: 'purpose',
            type: 'text',
            isNullable: true
          },
          {
            name: 'benefit',
            type: 'text',
            isNullable: true
          },
          {
            name: 'result',
            type: 'text',
            isNullable: true
          },
          {
            name: 'budget_file_id',
            type: 'bigint',
            isNullable: true
          },
          {
            name: 'profile_file_id',
            type: 'bigint',
            isNullable: true
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ],
        foreignKeys: [
          {
            name: 'fk_budget',
            columnNames: ['budget_file_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'files'
          },
          {
            name: 'fk_profile',
            columnNames: ['profile_file_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'files'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('government_innovations', true);
  }
}
