import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class CreateRegionTable1678331371930 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'regions',
        columns: [
          {
            name: 'id',
            type: 'serial4'
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey('regions', ['id'], 'pk_regions');

    await queryRunner.createIndex(
      'regions',
      new TableIndex({
        columnNames: ['name'],
        name: 'idx_regions_name'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('regions', true);
  }
}
