import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddNamaPemdaToUserTable1688058282677
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users',
      new TableColumn({ name: 'nama_pemda', type: 'varchar', isNullable: true })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      'users',
      new TableColumn({ name: 'nama_pemda', type: 'varchar', isNullable: true })
    );
  }
}
