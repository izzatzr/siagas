import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class AddPemdaIdToGovernmentInnovationTable1692015237330
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'government_innovations',
      new TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true })
    );

    await queryRunner.createForeignKey(
      'government_innovations',
      new TableForeignKey({
        name: 'fk_pemda_id_government_innovations',
        columnNames: ['pemda_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'profil_pemda',
        onDelete: 'cascade'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'government_innovations',
      new TableForeignKey({
        name: 'fk_pemda_id_government_innovations',
        columnNames: ['pemda_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'profil_pemda',
        onDelete: 'cascade'
      })
    );

    await queryRunner.dropColumn(
      'government_innovations',
      new TableColumn({ name: 'pemda_id', type: 'bigint', isNullable: true })
    );
  }
}
