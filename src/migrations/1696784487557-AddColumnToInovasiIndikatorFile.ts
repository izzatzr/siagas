import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class AddColumnToInovasiIndikatorFile1696784487557
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('inovasi_indikator_file', [
      new TableColumn({ name: 'inovasi_id', type: 'bigint', isNullable: true }),
      new TableColumn({
        name: 'indikator_id',
        type: 'bigint',
        isNullable: true
      })
    ]);

    await queryRunner.createForeignKeys('inovasi_indikator_file', [
      new TableForeignKey({
        columnNames: ['inovasi_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'government_innovations'
      }),
      new TableForeignKey({
        columnNames: ['indikator_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'indicators'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('inovasi_indikator_file', [
      new TableColumn({ name: 'inovasi_id', type: 'bigint', isNullable: true }),
      new TableColumn({
        name: 'indikator_id',
        type: 'bigint',
        isNullable: true
      })
    ]);

    await queryRunner.dropForeignKeys('inovasi_indikator_file', [
      new TableForeignKey({
        columnNames: ['inovasi_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'government_innovations'
      }),
      new TableForeignKey({
        columnNames: ['indikator_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'indicators'
      })
    ]);
  }
}
