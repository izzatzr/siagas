import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateSettingTable1678778288144 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'settings',
        columns: [
          {
            name: 'id',
            type: 'serial8',
            isPrimary: true,
            primaryKeyConstraintName: 'pk_settings'
          },
          {
            name: 'key',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'value',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ],
        uniques: [
          {
            columnNames: ['key'],
            name: 'unique_settings_key'
          }
        ],
        indices: [
          {
            columnNames: ['key'],
            name: 'idx_settings_key'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('settings', true);
  }
}
