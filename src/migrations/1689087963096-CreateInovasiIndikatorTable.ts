import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateInovasiIndikatorTable1689087963096
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'inovasi_indikator',
        columns: [
          { name: 'id', type: 'bigserial', isPrimary: true },
          { name: 'inovasi_id', type: 'bigint', isNullable: true },
          { name: 'indikator_id', type: 'bigint', isNullable: true },
          { name: 'informasi', type: 'varchar', isNullable: true },
          { name: 'nilai', type: 'numeric', isNullable: true },
          { name: 'nilai_sebelum', type: 'numeric', isNullable: true },
          { name: 'nilai_sesudah', type: 'numeric', isNullable: true },
          { name: 'created_by', type: 'varchar' },
          { name: 'updated_by', type: 'varchar' },
          { name: 'created_at', type: 'timestamp', default: `now()` },
          { name: 'updated_at', type: 'timestamp', default: `now()` }
        ],
        foreignKeys: [
          {
            columnNames: ['inovasi_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'government_innovations',
            onDelete: 'cascade'
          },
          {
            columnNames: ['indikator_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'indicators',
            onDelete: 'cascade'
          },
          {
            columnNames: ['created_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          },
          {
            columnNames: ['updated_by'],
            referencedColumnNames: ['username'],
            referencedTableName: 'users'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'inovasi_indikator' }), true);
  }
}
