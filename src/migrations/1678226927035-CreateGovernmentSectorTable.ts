import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class CreateGovernmentSectorTable1678226927035
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'government_sectors',
        columns: [
          {
            name: 'id',
            type: 'serial4'
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      }),
      true
    );

    await queryRunner.createPrimaryKey(
      'government_sectors',
      ['id'],
      'pk_government_sectors'
    );

    await queryRunner.createIndex(
      'government_sectors',
      new TableIndex({
        columnNames: ['name'],
        name: 'idx_government_sectors_name'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('government_sectors', true);
  }
}
