import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableIndex
} from 'typeorm';

export class AddEmailColumnToUserTable1678658467059
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('users', [
      new TableColumn({ name: 'email', type: 'varchar', length: '255' }),
      new TableColumn({
        name: 'full_name',
        type: 'varchar',
        length: '255',
        isNullable: true
      }),
      new TableColumn({
        name: 'nickname',
        type: 'varchar',
        length: '255',
        isNullable: true
      })
    ]);

    await queryRunner.createIndices('users', [
      new TableIndex({
        columnNames: ['email'],
        name: 'idx_users_email',
        isUnique: true
      }),
      new TableIndex({
        columnNames: ['nickname'],
        name: 'idx_users_nickname'
      }),
      new TableIndex({
        columnNames: ['full_name'],
        name: 'idx_users_full_name'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropIndex('users', 'idx_users_email');
    await queryRunner.dropIndex('users', 'idx_users_nickname');
    await queryRunner.dropIndex('users', 'idx_users_full_name');
    await queryRunner.dropColumns('users', ['email', 'full_name', 'nickname']);
  }
}
