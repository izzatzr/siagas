import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class AddCreatedByColumnToGovernmentInnovationsTable1689089341041
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('government_innovations', [
      new TableColumn({ isNullable: true, name: 'updated_by', type: 'varchar' })
    ]);

    await queryRunner.createForeignKeys('government_innovations', [
      new TableForeignKey({
        columnNames: ['updated_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKeys('government_innovations', [
      new TableForeignKey({
        columnNames: ['updated_by'],
        referencedColumnNames: ['username'],
        referencedTableName: 'users'
      })
    ]);

    await queryRunner.dropColumns('government_innovations', [
      new TableColumn({ isNullable: true, name: 'updated_by', type: 'varchar' })
    ]);
  }
}
