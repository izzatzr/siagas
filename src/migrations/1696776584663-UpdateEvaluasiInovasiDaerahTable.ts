import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm';

export class UpdateEvaluasiInovasiDaerahTable1696776584663
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('evaluasi_inovasi_daerah', [
      'document_category_id',
      'judul',
      'document_id',
      'nomor_surat',
      'nomor',
      'tanggal_surat'
    ]);

    await queryRunner.addColumns('evaluasi_inovasi_daerah', [
      new TableColumn({ name: 'kategori', type: 'varchar', isNullable: true })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('evaluasi_inovasi_daerah', [
      new TableColumn({
        name: 'judul',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'document_category_id',
        type: 'bigint',
        isNullable: true
      }),
      new TableColumn({
        name: 'document_id',
        type: 'bigint',
        isNullable: true
      }),
      new TableColumn({
        name: 'nomor_surat',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'nomor',
        type: 'varchar',
        isNullable: true
      }),
      new TableColumn({
        name: 'tanggal_surat',
        type: 'date',
        isNullable: true
      })
    ]);

    await queryRunner.dropColumn('evaluasi_inovasi_daerah', 'kategori');

    await queryRunner.createForeignKeys('evaluasi_inovasi_daerah', [
      new TableForeignKey({
        columnNames: ['document_category_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'document_categories'
      }),
      new TableForeignKey({
        columnNames: ['document_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'documents'
      })
    ]);
  }
}
