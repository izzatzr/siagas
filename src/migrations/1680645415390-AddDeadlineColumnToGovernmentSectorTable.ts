import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddDeadlineColumnToGovernmentSectorTable1680645415390
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'government_sectors',
      new TableColumn({
        name: 'deadline',
        type: 'date'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('government_sectors', 'deadline');
  }
}
