import { AppDataSource } from 'src/data-source';
import { EntityManager } from 'typeorm';

export default class InnovativeGovernmentAwardRepository {
  manager: EntityManager;
  constructor() {
    this.manager = AppDataSource.manager;
  }
}
