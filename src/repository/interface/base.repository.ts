import { BaseEntity, EntityTarget, Repository } from 'typeorm';
import { AppDataSource } from '../../data-source';

export default class BaseRepository<
  T extends BaseEntity
> extends Repository<T> {
  protected repository: Repository<T>;

  constructor(entity: EntityTarget<T>) {
    super(entity, AppDataSource.manager);
    this.repository = AppDataSource.getRepository(entity);
  }
}
