import Role from '../entity/Role.entity';
import BaseRepository from './interface/base.repository';
import roles from '../data/seeder/role.json';

export default class RoleRepository extends BaseRepository<Role> {
  constructor() {
    super(Role);
  }
  role(): void {
    roles.forEach(async role => {
      const res = await this.findOneBy({ name: role.name });

      if (res) {
        return;
      }

      this.insert({ is_creator: role.is_creator, name: role.name }).then(
        val => {
          console.log(`Success seeder ${role.name} data`);
        },
        () => {
          console.log(`Error seeder ${role.name} data`);
        }
      );
    });
  }
}
