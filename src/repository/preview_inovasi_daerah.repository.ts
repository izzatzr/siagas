import { PreviewReviewInovasiDaerah } from 'src/entity/PreviewReviewInovasiDaerah.entity';
import BaseRepository from './interface/base.repository';

export default class PreviewInovasiDaerahRepository extends BaseRepository<PreviewReviewInovasiDaerah> {
  constructor() {
    super(PreviewReviewInovasiDaerah);
  }
}
