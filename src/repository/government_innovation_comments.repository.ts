import { GovernmentInnovationsComments } from 'src/entity/GovernmentInnovationsComments.entity';
import GovernmentInnovation from '../entity/GovernmentInnovation.entity';
import BaseRepository from './interface/base.repository';

export default class GovernmentInnovationCommentsRepository extends BaseRepository<GovernmentInnovationsComments> {
  constructor() {
    super(GovernmentInnovationsComments);
  }
}
