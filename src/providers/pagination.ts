import {
  PaginationLinkResponse,
  PaginationOptions,
  PaginationResponse,
  SortOrder
} from '../interface/pagination.interface';

class Pagination {
  public limit: number = 10;
  public maxLimit: number = 100;
  public offset: number = 0;
  public page: number = 1;
  public pages: number = 1;

  public options = (
    page: number = 0,
    length: number = 0,
    order: [[string, string]] = [['created_at', 'DESC']]
  ): PaginationOptions => {
    if (page < 0) page = 1;
    if (length <= 0 || length > this.maxLimit) length = this.limit;

    const offset: number = (page - 1) * length;
    const sort: { [key: string]: SortOrder } = this.buildOrder(order);

    return {
      page,
      offset: offset < 0 ? 0 : offset,
      limit: length,
      order: sort
    };
  };

  private buildOrder = (
    orders: [[string, string]]
  ): { [key: string]: SortOrder } => {
    let sort: { [key: string]: SortOrder } = {
      created_at: 'DESC'
    };

    orders.forEach(order => {
      sort = {
        ...sort,
        [order[0]]: (order[1] as SortOrder) ?? 'DESC'
      };
    });

    return sort;
  };

  public build = (
    url: string,
    page: number,
    length: number,
    total: number
  ): PaginationResponse => {
    let lastPage: number = 0;

    if (total > 0) lastPage = Math.ceil(total / length);

    const next = +page + +1;
    const links: PaginationLinkResponse = {
      first: url.replace(/page=([^&]*)/, `page=1`),
      last: url.replace(/page=([^&]*)/, `page=${lastPage <= 0 ? 1 : lastPage}`),
      prev:
        page > 1 ? url.replace(/page=([^&]*)/, `page=${page - 1}`) : undefined,
      next:
        page < lastPage
          ? url.replace(/page=([^&]*)/, `page=${next}`)
          : undefined
    };

    return {
      total,
      page: +page,
      pages: lastPage,
      links
    };
  };
}

export default new Pagination();
