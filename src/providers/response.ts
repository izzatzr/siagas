import { Response } from 'express';
import { IResponse } from 'src/interface/response.interface';
import { ZodError } from 'zod';
import {
  ErrorResponse,
  PaginationResponse,
  ResponseType,
  SuccessResponse,
  ValidationMessage,
  ValidationResponse
} from '../types/response.type';
import ApiError from './exceptions/api.error';

class ResponseProvider<T, P> implements IResponse<T, P> {
  private response!: ResponseType<T, P>;

  success(data: T, code: number = 200, message: string = 'Ok!'): this {
    const response: SuccessResponse<T> = {
      status: 'success',
      isSuccess: true,
      code: code,
      message: message,
      data: data
    };

    this.response = response;
    return this;
  }

  pagination(
    data: T,
    pagination: P,
    code: number = 200,
    message: string = 'Ok!'
  ): this {
    const response: PaginationResponse<P, T> = {
      status: 'success',
      isSuccess: true,
      code: code,
      message: message,
      data: data,
      pagination: pagination
    };

    this.response = response;
    return this;
  }

  error(
    error: Error | ApiError,
    code: number = 400,
    message: string = 'Error!'
  ): this {
    const response: ErrorResponse = {
      status: 'success',
      isSuccess: true,
      code: code,
      message: message,
      error: error
    };

    this.response = response;
    return this;
  }

  private buildErrorValidationMessage = (
    error: ZodError
  ): Array<ValidationMessage> => {
    const validation: Array<ValidationMessage> =
      error.issues.map<ValidationMessage>(issue => {
        return {
          object: issue.path[1].toString(),
          message: issue.message
        };
      });

    return validation;
  };

  validation(
    error: ZodError<any>,
    code: number = 422,
    message: string = 'Error validation!'
  ): this {
    const response: ValidationResponse = {
      status: 'validation',
      isSuccess: false,
      code: code,
      message: message,
      error: this.buildErrorValidationMessage(error)
    };

    this.response = response;
    return this;
  }

  create(res: Response<any, Record<string, any>>): void {
    res.status(this.response.code).send(this.response);
  }
}

export default new ResponseProvider();
