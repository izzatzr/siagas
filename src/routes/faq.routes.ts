import { Application, Router } from 'express';
import FaqController from '../controller/faq.controller';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createFaqSchema } from '../schema/faq.schema';
import BaseRoutes from './base.routes';

class FaqRoutes extends BaseRoutes<FaqController> {
  constructor(express: Application) {
    super(express, new FaqController(), '/faq');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', validate(createFaqSchema), this.controller.create);
    route.patch('/:id', validate(createFaqSchema), this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default FaqRoutes;
