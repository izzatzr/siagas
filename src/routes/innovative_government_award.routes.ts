import { InnovativeGovernmentAwardController } from 'src/controller/innovative_government_award.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

export class InnovativeGovernmentAwardRoute extends BaseRoutes<InnovativeGovernmentAwardController> {
  constructor(express: Application) {
    super(
      express,
      new InnovativeGovernmentAwardController(),
      '/innovative_government_award'
    );
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('/rangking', this.controller.rangking);
    route.get('/rangking/download/:type', this.controller.rangking_download);
    route.get('/prestasi', this.controller.prestasi);
    route.get('/prestasi/download/:type', this.controller.prestasi_download);
    route.get(
      '/peringkat_hasil_review',
      this.controller.peringkat_hasil_review
    );
    route.get(
      '/peringkat_hasil_review/download/:type',
      this.controller.peringkat_hasil_review_download
    );
    route.patch(
      '/peringkat_hasil_review/:pemda_id',
      this.controller.nominator_peringkat_hasil_review
    );

    express.use(this.prefixRoutes, route);
    return express;
  }
}
