import ProfilPemdaController from 'src/controller/profil_pemda.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';
import { storage, upload } from 'src/middleware/file.middleware';

class ProfilPemdaRoutes extends BaseRoutes<ProfilPemdaController> {
	constructor(express: Application) {
		super(express, new ProfilPemdaController(), '/profil_pemda');
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.use(authenticated);
		route.get('', this.controller.get);
		route.get('/download/:type', this.controller.getDownload);
		route.get('/:id', this.controller.detail);
		route.get('/:id/download/:type', this.controller.downloadDetail);
		route.post('', upload(storage('public/upload/profil_pemda/')).single('file'), this.controller.create);
		route.patch('/:id', upload(storage('public/upload/profil_pemda/')).single('file'), this.controller.update);
		route.delete('/:id', this.controller.delete);

		express.use(this.prefixRoutes, route);
		return express;
	}
}

export default ProfilPemdaRoutes;
