import { ReviewInovasiDaerahController } from "src/controller/review_inovasi_daerah.controller";
import BaseRoutes from "./base.routes";
import { Application, Router } from "express";
import { authenticated } from "src/middleware/jwt.middleware";

export class ReviewInovasiDaerahRoutes extends BaseRoutes<ReviewInovasiDaerahController> {
	constructor(express: Application) {
		super(
			express,
			new ReviewInovasiDaerahController(),
			"/review_inovasi_daerah"
		);
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.use(authenticated);
		route.get("", this.controller.get);
		route.get("/download/:type", this.controller.getDownload);
		route.get("/:id", this.controller.detail);
		route.get("/:id/profil_inovasi", this.controller.profilInovasi);
		route.get("/:id/indikator", this.controller.indikator);
		route.get("/:id/indikator/:indikator_id", this.controller.indikatorDetail);
		route.get(
			"/:id/indikator/:indikator_id/evaluasi",
			this.controller.getEvaluasi
		);
		route.post("", this.controller.create);
		route.patch("/:id", this.controller.update);
		route.patch(
			"/:id/indikator/:indikator_id/evaluasi",
			this.controller.evaluasi
		);
		route.delete("/:id", this.controller.delete);

		express.use(this.prefixRoutes, route);
		return express;
	}
}
