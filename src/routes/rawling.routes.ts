import { RawlingController } from 'src/controller/rawling.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

export class RawlingRoute extends BaseRoutes<RawlingController> {
  constructor(express: Application) {
    super(express, new RawlingController(), '/rawlog');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.rawling);

    express.use(this.prefixRoutes, route);
    return express;
  }
}
