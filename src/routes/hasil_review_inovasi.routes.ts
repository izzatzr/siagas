import { HasilReviewInovasiController } from "src/controller/hasil_review_inovasi.controller";
import BaseRoutes from "./base.routes";
import { Application, Router } from "express";
import { authenticated } from "src/middleware/jwt.middleware";

export class HasilReviewInovasiRoute extends BaseRoutes<HasilReviewInovasiController> {
	constructor(express: Application) {
		super(express, new HasilReviewInovasiController(), "/hasil_review_inovasi_daerah");
	}

	routes(): Application {
		let express = this.express;
		const route = Router();

		route.use(authenticated);
		route.get("", this.controller.get);
		route.get("/download/:type", this.controller.getDownload);
		route.get("/:review_inovasi_id", this.controller.detail);
		route.delete("/:review_inovasi_id", this.controller.delete);

		express.use(this.prefixRoutes, route);
		return express;
	}
}
