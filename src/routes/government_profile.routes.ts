import { Application, Router } from 'express';
import GovernmentProfileController from '../controller/government_profile.controller';
import { storage, upload } from '../middleware/file.middleware';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createGovernmentProfileSchema } from '../schema/government_profile.schema';
import BaseRoutes from './base.routes';

class GovernmentProfileRoutes extends BaseRoutes<GovernmentProfileController> {
  constructor(express: Application) {
    super(express, new GovernmentProfileController(), '/profil_pemda');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post(
      '',
      upload(storage('public/upload/profil_pemda/')).single('file'),
      validate(createGovernmentProfileSchema),
      this.controller.create
    );
    route.patch(
      '/:id',
      upload(storage('public/upload/profil_pemda/')).single('file'),
      validate(createGovernmentProfileSchema),
      this.controller.update
    );
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default GovernmentProfileRoutes;
