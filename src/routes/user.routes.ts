import { Application, Router } from 'express';
import UserController from '../controller/user.controller';
import BaseRoutes from './base.routes';
import { authenticated } from '../middleware/jwt.middleware';

class UserRoutes extends BaseRoutes<UserController> {
  constructor(express: Application) {
    super(express, new UserController(), '/user');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.post('', this.controller.create);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.patch('/:id', this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, authenticated, route);
    return express;
  }
}

export default UserRoutes;
