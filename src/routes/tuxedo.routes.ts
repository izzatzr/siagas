import { Application, Router } from 'express';
import TuxedoController from '../controller/tuxedo.controller';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { createTuxedoSchema } from '../schema/tuxedo.schema';
import BaseRoutes from './base.routes';

class TuxedoRoutes extends BaseRoutes<TuxedoController> {
  constructor(express: Application) {
    super(express, new TuxedoController(), '/tuxedo');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', validate(createTuxedoSchema), this.controller.create);
    route.patch('/:id', validate(createTuxedoSchema), this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default TuxedoRoutes;
