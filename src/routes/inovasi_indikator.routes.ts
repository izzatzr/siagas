import InovasiIndikatorController from 'src/controller/inovasi_indikator.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

class InovasiIndikatorRoutes extends BaseRoutes<InovasiIndikatorController> {
  constructor(express: Application) {
    super(
      express,
      new InovasiIndikatorController(),
      '/inovasi_pemerintah_daerah'
    );
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('/:pemda_id/indikator', this.controller.get);
    route.get('/:pemda_id/indikator/:indikator_id', this.controller.detail);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default InovasiIndikatorRoutes;
