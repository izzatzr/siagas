import { Application, Router } from 'express';
import IndicatorScaleController from '../controller/indicator_scale.controller';
import BaseRoutes from './base.routes';

class IndicatorScaleRoutes extends BaseRoutes<IndicatorScaleController> {
  constructor(express: Application) {
    super(express, new IndicatorScaleController(), '/indikator');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.get('/:indikator_id/skala', this.controller.get);
    route.get('/skala/:id', this.controller.detail);
    route.post('/:indikator_id/skala', this.controller.create);
    route.patch('/skala/:id', this.controller.update);
    route.delete('/skala/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);

    return express;
  }
}

export default IndicatorScaleRoutes;
