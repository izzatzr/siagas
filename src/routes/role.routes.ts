import { Application, Router } from 'express';
import RoleController from '../controller/role.controller';
import BaseRoutes from './base.routes';

class RoleRoutes extends BaseRoutes<RoleController> {
  constructor(express: Application) {
    super(express, new RoleController(), '/role');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', this.controller.create);
    route.patch('/:id', this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default RoleRoutes;
