import { Application } from 'express';

export default class BaseRoutes<C> {
  public controller: C;
  public express: Application;
  public prefixRoutes: string;

  constructor(express: Application, controller: C, prefix: string) {
    this.controller = controller;
    this.express = express;
    this.prefixRoutes = prefix;
  }
}
