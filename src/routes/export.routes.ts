import { Application, Router } from 'express';
import ExportController from '../controller/export.controller';
import BaseRoutes from './base.routes';

class ExportRoutes extends BaseRoutes<ExportController> {
  constructor(express: Application) {
    super(express, new ExportController(), '/export');
  }
  routes(): Application {
    let express = this.express;
    const route = Router();

    route.post('', this.controller.export);

    express.use(this.prefixRoutes, route);

    return express;
  }
}

export default ExportRoutes;
