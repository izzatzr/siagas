import { Application, Router } from 'express';
import multer from 'multer';
import FileController from '../controller/file.controller';
import { storage, upload } from '../middleware/file.middleware';
import { authenticated } from '../middleware/jwt.middleware';
import { validate } from '../middleware/validation.middleware';
import { uploadFileSchema } from '../schema/file.schema';
import BaseRoutes from './base.routes';

class FileRoutes extends BaseRoutes<FileController> {
  constructor(express: Application) {
    super(express, new FileController(), '/file');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.post(
      '',
      upload(storage()).single('file'),
      validate(uploadFileSchema),
      this.controller.upload
    );

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default FileRoutes;
