import DistrikController from "src/controller/distrik.controller";
import BaseRoutes from "./base.routes";
import { Application } from "express";
import { authenticated } from "src/middleware/jwt.middleware";

class DistrikRoutes extends BaseRoutes<DistrikController> {
	constructor(express: Application) {
		super(express, new DistrikController(), "/distrik");
	}

	routes(): Application {
		let express = this.express;
		express.use(authenticated);
		express.get(this.prefixRoutes + "", this.controller.get);
		express.get(this.prefixRoutes + "/:id", this.controller.detail);
		express.post(this.prefixRoutes + "", this.controller.create);
		express.patch(this.prefixRoutes + "/:id", this.controller.update);
		express.delete(this.prefixRoutes + "/:id", this.controller.delete);

		return express;
	}
}

export default DistrikRoutes;
