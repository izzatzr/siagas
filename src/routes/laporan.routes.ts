import { LaporanController } from 'src/controller/laporan.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

export class LaporanRoute extends BaseRoutes<LaporanController> {
  constructor(express: Application) {
    super(express, new LaporanController(), '/laporan');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('/indeks', this.controller.laporanIndeks);
    route.get('/indeks/download/:type', this.controller.laporanIndeksDownload);
    route.get('/jenis_inovasi', this.controller.laporanJenisInovasi);
    route.get('/bentuk_inovasi', this.controller.laporanBentuk);
    route.get('/inisiator_inovasi', this.controller.laporanInisiator);
    route.get(
      '/inisiator_inovasi/download/:type',
      this.controller.laporanInisiatorInovasiDownload
    );
    route.get('/urusan_inovasi', this.controller.laporanUrusan);
    route.get(
      '/urusan_inovasi/download/:type',
      this.controller.laporanUrusanInovasiDownload
    );

    express.use(this.prefixRoutes, route);
    return express;
  }
}
