import { Application, Router } from 'express';
import BaseRoutes from './base.routes';
import { PemdaIndikatorFileController } from 'src/controller/pemda_indikator_file.controller';
import { authenticated } from 'src/middleware/jwt.middleware';
import { storage, upload } from 'src/middleware/file.middleware';
import { validate } from 'src/middleware/validation.middleware';
import { uploadPemdaIndikatorFileSchema } from 'src/schema/pemda_indikator_file.schema';

export class PemdaIndikatorFileRoutes extends BaseRoutes<PemdaIndikatorFileController> {
  constructor(express: Application) {
    super(
      express,
      new PemdaIndikatorFileController(),
      '/profil_pemda/indikator'
    );
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('/:pemda_id/:indikator_id/files', this.controller.get);
    route.get(
      '/:pemda_id/:indikator_id/files/:file_id',
      this.controller.detail
    );
    route.post(
      '/:pemda_id/:indikator_id/upload',
      upload(storage('public/upload/pemda/dokumen/')).single('dokumen'),
      validate(uploadPemdaIndikatorFileSchema),
      this.controller.upload
    );
    route.delete(
      '/:pemda_id/:indikator_id/delete/:file_id',
      this.controller.delete
    );

    express.use(this.prefixRoutes, route);
    return express;
  }
}
