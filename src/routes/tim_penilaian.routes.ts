import TimPenilaianController from 'src/controller/tim_penilaian.controller';
import BaseRoutes from './base.routes';
import { Application, Router } from 'express';
import { authenticated } from 'src/middleware/jwt.middleware';

class TimPenilaianRoutes extends BaseRoutes<TimPenilaianController> {
  constructor(express: Application) {
    super(express, new TimPenilaianController(), '/tim_penilaian');
  }

  routes(): Application {
    let express = this.express;
    const route = Router();

    route.use(authenticated);
    route.get('', this.controller.get);
    route.get('/:id', this.controller.detail);
    route.post('', this.controller.create);
    route.patch('/:id', this.controller.update);
    route.delete('/:id', this.controller.delete);

    express.use(this.prefixRoutes, route);
    return express;
  }
}

export default TimPenilaianRoutes;
