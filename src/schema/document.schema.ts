import { number, object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createDocumentSchema = object({
  body: object({
    title: string({ required_error: 'Title is required' }),
    content: string()
  })
});

export type CreateDocumentRequest = z.infer<
  typeof createDocumentSchema
>['body'] & {
  category_id?: number;
  document?: Express.Multer.File;
};

export interface GetDocumentRequest extends BasePaginationRequest {
  q?: string;
  category?: number;
}
