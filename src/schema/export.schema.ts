import { object, string, unknown, z } from 'zod';

export const createExportRequest = object({
  body: object({
    name: string(),
    format: z.enum(['xlsx', 'csv']),
    headers: object({
      header: string(),
      key: string()
    }).array(),
    data: unknown().array().array()
  })
});

export type CreateExportRequest = z.infer<typeof createExportRequest>['body'];
