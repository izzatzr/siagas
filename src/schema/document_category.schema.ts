import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createDocumentCategorySchema = object({
  body: object({
    name: string({ required_error: 'Name is required' }),
    slug: string({ required_error: 'Slug is required' })
      .refine(data => !/[A-Z]/g.test(data), {
        message: 'Slug cannot contain upper case',
        path: ['body', 'slug']
      })
      .refine(data => !data.includes(' '), {
        message: 'Slug cannot contain space or blank space',
        path: ['body', 'slug']
      })
  })
});

export type CreateDocumentCategoryRequest = z.infer<
  typeof createDocumentCategorySchema
>['body'];

export interface GetDocumentCategoryRequest extends BasePaginationRequest {
  q?: string;
}
