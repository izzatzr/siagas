import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { PemdaIndikatorFile } from 'src/entity/PemdaIndikatorFile.entity';

export const uploadPemdaIndikatorFileSchema = z.object({
  body: z.object({
    nomor_dokumen: z.string(),
    tanggal_dokumen: z.string(),
    tentang: z.string(),
    name: z.string().optional()
  })
});

export type UploadPemdaIndikatorFileRequest = z.infer<
  typeof uploadPemdaIndikatorFileSchema
>['body'] & {
  dokumen?: Express.Multer.File;
};

export interface PemdaIndikatorFileParams extends BasePaginationRequest {
  q?: string;
}

export type ListPemdaIndikatorFiles = PemdaIndikatorFile[];
