import { number, object, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createClusterDetailSchema = object({
  body: object({
    region_id: number({ required_error: 'Region is required' })
  })
});

export type CreateClusterDetailRequest = z.infer<
  typeof createClusterDetailSchema
>['body'];

export interface GetClusterDetailRequest extends BasePaginationRequest {
  q?: string;
}
