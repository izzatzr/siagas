import { PaktaIntegritas } from 'src/entity/PaktaIntegritas.entity';
import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';

const createPaktaIntegritas = z.object({
  body: z.object({
    user_id: z.string()
  })
});

export type CreatePaktaIntegritasRequest = z.infer<
  typeof createPaktaIntegritas
>['body'] & { file?: Express.Multer.File };

export interface PaktaIntegritasParams extends BasePaginationRequest {
  q?: string;
}

export type ListPaktaIntegritas = PaktaIntegritas[];
