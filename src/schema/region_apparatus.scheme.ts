import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createRegionApparatusScheme = object({
  body: object({
    name: string({ required_error: 'Name is required' })
  })
});

export type CreateRegionApparatusRequest = z.infer<
  typeof createRegionApparatusScheme
>['body'];

export interface GetRegionApparatusRequest extends BasePaginationRequest {
  q?: string;
}
