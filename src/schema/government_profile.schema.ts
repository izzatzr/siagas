import { number, object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createGovernmentProfileSchema = object({
  body: object({
    daerah: string(),
    name: string(),
    opd_menangani: string(),
    alamat: string(),
    email: string().email(),
    nomor_telepon: string(),
    nama_admin: string()
  })
});

export type CreateGovernmentProfileRequest = z.infer<
  typeof createGovernmentProfileSchema
>['body'] & {
  file?: Express.Multer.File;
};

export interface GetGovernmentProfileRequest extends BasePaginationRequest {
  daerah?: string;
  q?: string;
}
