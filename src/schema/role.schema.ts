import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createRoleSchema = object({
  body: object({
    name: string({
      required_error: 'Name is required',
      invalid_type_error: 'Name must be string'
    })
  })
});

export type CreateRoleRequest = z.infer<typeof createRoleSchema>['body'];

export interface GetRoleRequest extends BasePaginationRequest {
  name?: string;
}
