import { object, string, z } from "zod";
import { BasePaginationRequest } from "./base.schema";

export const createDistrikSchema = object({
	body: object({
		name: string({ required_error: "Name is required" }),
	}),
});

export type CreateDistrikRequest = z.infer<typeof createDistrikSchema>["body"];

export interface GetDistrikRequest extends BasePaginationRequest {
	name?: string;
}
