import { object, string, z } from 'zod';
import { BasePaginationRequest } from './base.schema';

export const createClusterSchema = object({
  body: object({
    name: string({ required_error: 'Name is required' })
  })
});

export type CreateClusterRequest = z.infer<typeof createClusterSchema>['body'];

export interface GetClusterRequest extends BasePaginationRequest {
  q?: string;
}
