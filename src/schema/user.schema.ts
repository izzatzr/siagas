import { number, object, string, z } from 'zod';
import { AppDataSource } from '../data-source';
import User from '../entity/User.entity';
import { BasePaginationRequest } from './base.schema';
import { OmitType } from 'src/types/lib';

export const createUserSchema = object({
	body: object({
		role_id: number({ required_error: 'Role is required' }),
		nama_lengkap: string().optional(),
		nama_pemda: string().optional(),
		nama_panggilan: string().optional(),
		email: string({ required_error: 'Email is required' }).email('Format email not valid'),
		username: string({ required_error: 'Username is required' })
			.min(6, 'Username minimal 6 character length')
			.refine(
				async value => {
					const ds = AppDataSource;
					const repo = ds.getRepository(User);

					const user = await repo.exist({ where: { username: value } });

					return !user;
				},
				{ message: 'Username already used' }
			),
		password: string({ required_error: 'Password is required' }).min(6, 'Password minimal 6 character length'),
	}),
});

export type CreateUserRequest = z.infer<typeof createUserSchema>['body'];

export interface GetUserRequest extends Partial<BasePaginationRequest> {
	/**
	 * Search user by username or email or full name
	 */
	q?: string;

	role_id?: number;
}

export type UserResponse = OmitType<User, 'createPassword'>;

export type UserResponses = UserResponse[];
