import { z } from 'zod';
import { BasePaginationRequest } from './base.schema';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';

const createProfilPemda = z.object({
	body: z.object({
		nama_pemda: z.string(),
		nama_daerah: z.string().optional(),
		opd_yang_menangani: z.string().optional(),
		alamat_pemda: z.string().optional(),
		email: z.string().optional(),
		no_telpon: z.string().optional(),
		nama_admin: z.string().optional(),
	}),
});

export type CreateProfilPemdaRequest = z.infer<typeof createProfilPemda>['body'] & { file?: Express.Multer.File };

export interface ProfilPemdaParams extends BasePaginationRequest {
	q?: string;
}

export type ListProfilPemda = ProfilPemda[];

export interface ProfilPemdaFilterDownload {
	q?: string;
}

export interface ProfilPemdaDownloadResponse {
	no: number;
	daerah: string;
	nama_pemda: string;
	opd_yang_menangani: string;
	alamat_pemda: string;
	email: string;
	no_telepon: string;
	nama_admin: string;
}

export const toDownload = (data: ProfilPemda, no: number): ProfilPemdaDownloadResponse => {
	return {
		no,
		daerah: data.nama_daerah ?? '',
		nama_pemda: data.user?.nama_pemda ?? '',
		opd_yang_menangani: data.opd_yang_menangani ?? '',
		alamat_pemda: data.alamat_pemda ?? '',
		email: data.email ?? '',
		no_telepon: data.no_telpon ?? '',
		nama_admin: data.nama_admin ?? '',
	};
};
