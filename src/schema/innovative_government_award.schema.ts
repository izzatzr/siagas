import { PeringkatHasilReview } from 'src/entity/PeringkatHasilReview.entity';
import { BasePaginationRequest } from './base.schema';

export interface PeringkatHasilReviewParams
  extends Partial<BasePaginationRequest> {
  q?: string;
  pemda_id?: number;
}

export interface PeringkatHasilReviewResponse {
  pemda_id: number;
  nama_pemda: string;
  jumlah_inovasi: number;
  isp: number;
  rata_rata: number;
  skor_total: number;
  predikat: string;
  nominator: string;
}

export interface PeringkatHasilReviewNominatorRequest {
  nominator: string;
}

export const toPeringkatResponse = (
  data: PeringkatHasilReview,
  jumlah_pemda: number
): PeringkatHasilReviewResponse => {
  return {
    pemda_id: data.id ?? 0,
    nama_pemda: data.nama_daerah ?? '',
    jumlah_inovasi: data.jumlah_inovasi ?? 0,
    isp: data.isp ?? 0,
    rata_rata: data.jumlah_inovasi ? data.jumlah_inovasi / jumlah_pemda : 0,
    skor_total:
      data.total_skor_verifikasi && data.jumlah_inovasi
        ? data.total_skor_verifikasi + data.jumlah_inovasi * 0.38
        : 0,
    predikat: data.predikat ?? '',
    nominator: data.nominator ?? 'Tidak'
  };
};

export interface PrestasiResponse {
  pemda_id: number;
  nama_pemda: string;
  skor_pengukuran: number;
  presentasi: number;
  validasi_lapangan: number;
  skor_akhir: number;
}

export const toPrestasiResponse = (
  data: PeringkatHasilReview,
  jumlah_pemda: number
): PrestasiResponse => {
  return {
    pemda_id: data.id ?? 0,
    nama_pemda: data.nama_daerah ?? '',
    skor_pengukuran: data.jumlah_inovasi
      ? data.jumlah_inovasi / jumlah_pemda
      : 0,
    presentasi: data.skor ?? 0,
    validasi_lapangan: data.skor_evaluasi ?? 0,
    skor_akhir:
      data.total_skor_verifikasi && data.jumlah_inovasi
        ? data.total_skor_verifikasi + data.jumlah_inovasi * 0.38
        : 0
  };
};

export interface RangkingResponse {
  pemda_id: number;
  nama_pemda: string;
  skor_indeks: number;
  skor_penilaian: number;
  skor_akhir: number;
  predikat: string;
}

export const toRangkingResponse = (
  data: PeringkatHasilReview,
  jumlah_pemda: number
): RangkingResponse => {
  return {
    pemda_id: data.id ?? 0,
    nama_pemda: data.nama_daerah ?? '',
    skor_indeks:
      data.total_skor_mandiri &&
      data.jumlah_inovasi &&
      data.total_skor_mandiri / data.jumlah_inovasi > 0
        ? data.total_skor_mandiri / data.jumlah_inovasi
        : 0,
    skor_penilaian: data.total_skor_verifikasi,
    skor_akhir:
      data.total_skor_verifikasi && data.jumlah_inovasi
        ? data.total_skor_verifikasi + data.jumlah_inovasi * 0.38
        : 0,
    predikat: data.predikat ?? ''
  };
};
