import { DataSource } from 'typeorm';
import Role from '../../entity/Role.entity';
import roles from '../seeder/role.json';

export const roleSeed = async (ds: DataSource): Promise<void> => {
  const repo = ds.getRepository(Role);
  const entities: Role[] = [];

  for (let i = 0; i < roles.length; i++) {
    const { name, is_creator, is_super_admin } = roles[i];

    const entity = await repo.findOneBy({ name });
    if (!entity) {
      const data = repo.create({
        name,
        is_creator,
        is_super_admin
      });
      entities.push(data);
    }
  }

  await repo.insert(entities).then(
    () => console.log(`Upsert role data`),
    err => console.log(`Error upsert data: ${err}`)
  );
};
