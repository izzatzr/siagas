import { DataSource } from 'typeorm';
import { roleSeed } from './role.seed';
import { userSeed } from './user.seed';

export const seed = async (ds: DataSource): Promise<void> => {
	roleSeed(ds);
	userSeed(ds);
};
