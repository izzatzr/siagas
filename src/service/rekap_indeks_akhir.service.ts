import RekapIndeksAkhirRepository from 'src/repository/rekap_indeks_akhir.repository';
import { Body, Get, Hidden, Patch, Queries, Query, Route, Security, Tags } from 'tsoa';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { PaginationResponse as PaginationResponseInterface } from 'src/interface/pagination.interface';
import {
	DownloadRekapIndeksAkhirResponse,
	RekapIndeksAkhirParams,
	UpdateNominatorRequest,
	toDownloadResponse,
} from 'src/schema/rekap_indeks_akhir.schema';
import { RekapIndeksAkhir } from 'src/entity/RekapIndeksAkhir.entity';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import User from 'src/entity/User.entity';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import { DownloadType } from 'src/types/lib';
import ApiError from 'src/providers/exceptions/api.error';
import ExportService from './export.service';

@Route('/rekap_indeks_akhir')
@Tags('Verifikasi Index', 'Verifikasi Index | Rekap Indeks Akhir')
@Security('bearer')
export default class RekapIndeksAkhirService {
	private repo = new RekapIndeksAkhirRepository();
	private exportService = new ExportService();

	@Get('')
	async get(
		@Query() @Hidden() url = '',
		@Query() @Hidden() username = '',
		@Queries() req: RekapIndeksAkhirParams
	): Promise<PaginationResponse<PaginationResponseInterface, RekapIndeksAkhir[]> | PaginationData<RekapIndeksAkhir[]>> {
		const query = this.repo.repository.createQueryBuilder('ri').where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${req.q ?? ''}%` });

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(req.page, req.limit);

		const user = await this.repo.repository.manager.getRepository(User).findOneByOrFail({ username });
		if (user && user.role && user.role.is_super_admin === 't') {
			query.andWhere(`ri.created_by = (:created_by)`, {
				created_by: username,
			});
		}

		query.limit(limit).offset(offset);

		query.orderBy(`ri.indeks`, 'ASC');

		const res = await query.getMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Patch('/{id}')
	async nominator(id: number, @Body() req: UpdateNominatorRequest): Promise<SuccessResponse<string> | boolean> {
		const { affected } = await this.repo.repository.manager.getRepository(ProfilPemda).update({ id }, { nominator: req.nominator });

		if (affected && affected > 0) {
			return true;
		}
		return false;
	}

	@Tags('Export')
	@Get('/download/{type}')
	async download(type: DownloadType, @Query() @Hidden() username = '', @Queries() req: RekapIndeksAkhirParams): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			const query = this.repo.repository.createQueryBuilder('ri').where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${req.q ?? ''}%` });

			const user = await this.repo.repository.manager.getRepository(User).findOneByOrFail({ username });
			if (user && user.role && user.role.is_super_admin === 't') {
				query.andWhere(`ri.created_by = (:created_by)`, {
					created_by: username,
				});
			}

			query.orderBy(`ri.indeks`, 'ASC');

			const data = await query.getMany();

			const path = await this.exportService.download({
				data: data.map<DownloadRekapIndeksAkhirResponse>((value, idx) => toDownloadResponse(value, idx + 1)),
				name: 'rekap-indeks-akhir',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}
}
