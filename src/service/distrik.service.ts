import { Distrik } from "src/entity/Distrik.entity";
import ApiError from "src/providers/exceptions/api.error";
import DistrikRepository from "src/repository/distrik.repository";
import { CreateDistrikRequest, GetDistrikRequest } from "src/schema/distrik.schema";
import { PaginationResponse, SuccessResponse } from "../types/response.type";
import { HttpCode } from "../types/http_code.enum";
import { PaginationResponse as PaginationResponseInterface } from "../interface/pagination.interface";
import { Body, Delete, Get, Hidden, Patch, Post, Queries, Query, Route, Security, Tags } from "tsoa";
import pagination from "src/providers/pagination";

@Route("/distrik")
@Security("bearer")
@Tags("Master | Distrik")
export default class DistrikService {
	private repo: DistrikRepository;
	constructor() {
		this.repo = new DistrikRepository();
	}

	@Post("")
	async create(@Body() req: CreateDistrikRequest): Promise<SuccessResponse<Distrik> | Distrik> {
		const regions = this.repo.create({ nama_distrik: req.name });

		await this.repo.insert(regions);

		return regions;
	}

	@Get("/{id}")
	async detail(id: number): Promise<SuccessResponse<Distrik> | Distrik> {
		const res = await this.repo.findOneBy({ id: id });

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: "Data not found",
			});
		}

		return res;
	}

	@Get("")
	async get(
		@Query("url") @Hidden() url: string = "http://localhost",
		@Queries() req: GetDistrikRequest
	): Promise<PaginationResponse<PaginationResponseInterface, Array<Distrik>> | { paging: PaginationResponseInterface; res: Array<Distrik> }> {
		const query = this.repo.createQueryBuilder().where(`lower(nama_distrik) like lower(:name)`, { name: `%${req.name ?? ""}%` });

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		for (const key in order) {
			query.addOrderBy(key, order[key]);
		}

		const res = await query.getMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Delete("/{id}")
	async delete(id: number): Promise<SuccessResponse<string> | boolean> {
		const { affected } = await this.repo.delete({ id: id });

		if (affected && affected > 0) {
			return true;
		}
		return false;
	}

	@Patch("/{id}")
	async update(id: number, @Body() req: CreateDistrikRequest): Promise<SuccessResponse<string> | boolean> {
		const { affected } = await this.repo.update(id, { nama_distrik: req.name });

		if (!affected) {
			return false;
		}
		return true;
	}
}
