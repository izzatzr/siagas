import { Body, Delete, Get, Hidden, Patch, Post, Queries, Query, Route, Security, Tags } from 'tsoa';
import { DeepPartial } from 'typeorm';
import { AppDataSource } from '../data-source';
import Role from '../entity/Role.entity';
import User from '../entity/User.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import pagination from '../providers/pagination';
import UserRepository from '../repository/user.repository';
import { PaginationData } from '../schema/base.schema';
import { CreateUserRequest, GetUserRequest, UserResponse, UserResponses } from '../schema/user.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';

@Route('/user')
@Tags('Konfigurasi | User Account')
@Security('bearer')
export default class UserService {
	private repo: UserRepository;
	constructor() {
		this.repo = new UserRepository();
	}

	@Post('')
	async create(@Body() req: CreateUserRequest): Promise<SuccessResponse<UserResponse> | User> {
		const role: Role = AppDataSource.getRepository(Role).create({
			id: req.role_id,
		});
		const user: DeepPartial<User> = {
			username: req.username,
			email: req.email,
			full_name: req.nama_lengkap,
			nickname: req.nama_panggilan,
			password: req.password,
			nama_pemda: req.nama_pemda,
			role: role,
		};
		const create = this.repo.create(user);

		await this.repo.insert(create);

		return create;
	}

	@Get('')
	async findAll(
		@Queries() filter: GetUserRequest,
		@Query('url') @Hidden() url: string = 'http://localhost',
		@Query() @Hidden() username: string = ''
	): Promise<PaginationResponse<PaginationResponseInterface, UserResponses> | Partial<PaginationData<User[]>>> {
		const query = this.repo.createQueryBuilder('u').leftJoinAndSelect('u.role', 'r').leftJoinAndSelect('u.createdBy', 'cb');

		if (filter.q)
			query
				.where(`lower(u.username) like :q`, {
					q: `%${filter.q.toLowerCase()}%`,
				})
				.orWhere(`lower(r.name) like :q`, { q: `%${filter.q.toLowerCase()}%` });
		if (filter.role_id) {
			query.where(`u.role_id = :role_id`, { role_id: filter.role_id });
		}
		const user = await this.repo.manager.getRepository(User).findOneBy({ username });
		if (username && user && user.role && user.role.name.toLowerCase() === 'user') {
			query.andWhere('u.created_by = :created_by', { created_by: user.id });
		}

		const total = await query.getCount();
		const { limit, offset, page } = pagination.options(filter.page, filter.limit);

		if (filter.limit && filter.page) query.limit(limit).offset(offset);

		const res = await query.getMany();
		let paging = pagination.build(url, page, limit, total);

		return { paging: filter.limit && filter.page ? paging : undefined, res };
	}

	@Get('/{id}')
	async detail(id: number): Promise<SuccessResponse<User> | User> {
		const user = await this.repo.findOneByOrFail({ id: id });

		return user;
	}

	@Patch('/{id}')
	async update(id: number, @Body() req: CreateUserRequest): Promise<SuccessResponse<User> | User> {
		const { email, role_id, username, nama_lengkap, nama_panggilan } = req;
		const entity = this.repo.create({
			id,
			nickname: nama_panggilan,
			full_name: nama_lengkap,
			nama_pemda: req.nama_pemda,
			username,
			email,
			role_id,
		});

		return await this.repo.save(entity);
	}

	@Delete('/{id}')
	async delete(id: number): Promise<SuccessResponse<string> | string> {
		const { affected } = await this.repo.delete(id);

		return affected && affected > 0 ? 'Success delete' : 'Error deleting';
	}
}
