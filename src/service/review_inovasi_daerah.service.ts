import dayjs from 'dayjs';
import { EvaluasiInovasiDaerah } from 'src/entity/EvaluasiInovasiDaerah.entity';
import Indicator from 'src/entity/Indicator.entity';
import { InovasiIndikator } from 'src/entity/InovasiIndikator.entity';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import { ReviewInovasiDaerah } from 'src/entity/ReviewInovasiDaerah.entity';
import User from 'src/entity/User.entity';
import { PaginationResponse as PaginationResponseInterface } from 'src/interface/pagination.interface';
import ApiError from 'src/providers/exceptions/api.error';
import pagination from 'src/providers/pagination';
import ReviewInovasiDaerahRepository from 'src/repository/review_inovasi_daerah.repository';
import { PaginationData } from 'src/schema/base.schema';
import {
	CreateReviewInovasiDaerahRequest,
	EvaluasiResponse,
	GetReviewInovasiRequest,
	IndikatorResponse,
	ProfilInovasiResponse,
	ReviewInovasiDaerahDownload,
	ReviewInovasiDaerahResponse,
	UpdateEvaluasiRequest,
	toReviewInovasiDaerahDownload,
} from 'src/schema/review_inovasi_daerah.schema';
import { HttpCode } from 'src/types/http_code.enum';
import { DownloadType, Nullable } from 'src/types/lib';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { Body, Delete, Get, Hidden, Patch, Post, Queries, Query, Route, Security, Tags } from 'tsoa';
import { FindOptionsWhere, ILike } from 'typeorm';
import ExportService from './export.service';

@Route('/review_inovasi_daerah')
@Tags('Verifikasi Index | Review Inovasi Daerah')
@Security('bearer')
export default class ReviewInovasiDaerahService {
	private repo = new ReviewInovasiDaerahRepository();
	private exportService = new ExportService();

	@Post('')
	async create(
		@Query() @Hidden() username: string = '',
		@Body() req: CreateReviewInovasiDaerahRequest
	): Promise<SuccessResponse<ReviewInovasiDaerah> | ReviewInovasiDaerah> {
		const indicator = this.repo.create({
			...req,
			random_number: Math.floor(Math.random() * (1000000000000 - 100000000000)) + 100000000000,
			created_by: username,
			updated_by: username,
		});

		await this.repo.insert(indicator);

		return indicator;
	}

	@Tags('Export')
	@Get('/download/{type}')
	async download(type: DownloadType, @Query() @Hidden() username = '', @Queries() req: GetReviewInovasiRequest): Promise<string> {
		try {
			if (type === 'pdf')
				throw new ApiError({
					httpCode: 403,
					message: 'Download PDF under maintenance',
				});

			let where: FindOptionsWhere<ReviewInovasiDaerah> | FindOptionsWhere<ReviewInovasiDaerah>[] = {};

			if (req.pemda_id) where = { ...where, inovasi: { pemda_id: +req.pemda_id } };

			if (username) {
				const user = await this.repo.manager.getRepository(User).findOneByOrFail({ username });
				if (user && user.role && user.role.is_super_admin === 't') where = { ...where, created_by: username };
			}

			const data = await this.repo.find({
				where,
				relations: { inovasi: { profilPemda: true } },
			});

			const path = await this.exportService.download({
				data: data.map<ReviewInovasiDaerahDownload>((value, idx) => toReviewInovasiDaerahDownload(value, idx + 1)),
				name: 'review-inovasi-daerah',
			});

			return path;
		} catch (error) {
			throw error;
		}
	}

	@Get('/{id}')
	async detail(id: number): Promise<SuccessResponse<ReviewInovasiDaerahResponse> | ReviewInovasiDaerahResponse> {
		const res = await this.repo.findOne({
			where: { id: +id },
			relations: {
				inovasi: {
					profilPemda: true,
				},
				previews: {
					evaluasi: true,
				},
			},
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		return this.toResponse(res);
	}

	@Get('/{id}/profil_inovasi')
	async getProfilInovasi(id: number): Promise<SuccessResponse<ProfilInovasiResponse> | ProfilInovasiResponse> {
		const res = await this.repo.findOne({
			where: { id: +id },
			relations: {
				inovasi: {
					profilPemda: true,
					urusan: true,
				},
			},
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		return await this.toProfilResponse(res);
	}

	@Get('/{id}/indikator')
	async getIndikator(id: number): Promise<SuccessResponse<IndikatorResponse[]> | IndikatorResponse[]> {
		const indikator = await this.repo.manager.find(Indicator, {
			where: { jenis_indikator: 'si' },
		});
		const res = await this.repo.findOne({
			where: { id: +id },
			relations: {
				inovasi: true,
			},
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		const data = indikator.map(async indikator => {
			const inovasi_indikator = await this.repo.manager.findOne(InovasiIndikator, {
				where: {
					inovasi_id: res.inovasi_id,
					indikator_id: indikator.id,
				},
				relations: {
					evaluasi: true,
				},
			});

			const { evaluasi } = inovasi_indikator ?? { evaluasi: undefined };
			return this.toIndikatorRespons(indikator, evaluasi ? evaluasi[0] : undefined);
		});

		return await Promise.all(data);
	}

	@Get('/{id}/indikator/{indikator_id}')
	async getIndikatorDetail(id: number, indikator_id: string): Promise<SuccessResponse<IndikatorResponse> | IndikatorResponse> {
		const indikator = await this.repo.manager.findOneOrFail(Indicator, {
			where: { jenis_indikator: 'si', id: +indikator_id },
		});
		const res = await this.repo.findOne({
			where: { id: +id },
			relations: {
				inovasi: true,
			},
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		const inovasi_indikator = await this.repo.manager.findOneOrFail(InovasiIndikator, {
			where: {
				inovasi_id: res.inovasi_id,
				indikator_id: indikator.id,
			},
			relations: {
				evaluasi: true,
			},
		});

		const { evaluasi } = inovasi_indikator ?? { evaluasi: undefined };

		return this.toIndikatorRespons(indikator, evaluasi ? evaluasi[0] : undefined);
	}

	@Patch('/{id}/indikator/{indikator_id}/evaluasi')
	async updateEvaluasi(
		id: number,
		indikator_id: string,
		@Query() @Hidden() username = '',
		@Body() data: UpdateEvaluasiRequest
	): Promise<SuccessResponse<EvaluasiInovasiDaerah> | EvaluasiInovasiDaerah> {
		const review_inovasi_daerah = await this.repo.findOneOrFail({ where: { id: id } });
		let inovasi_indikator = await this.repo.manager.findOne(InovasiIndikator, {
			where: { inovasi_id: review_inovasi_daerah.inovasi_id, indikator_id: +indikator_id },
		});
		if (!inovasi_indikator) {
			await this.repo.manager.create(InovasiIndikator, { inovasi_id: review_inovasi_daerah.inovasi_id, indikator_id: +indikator_id }).save();
		}
		inovasi_indikator = await this.repo.manager.findOneOrFail(InovasiIndikator, {
			where: { inovasi_id: review_inovasi_daerah.inovasi_id, indikator_id: +indikator_id },
		});
		const res = await this.repo.manager
			.create(EvaluasiInovasiDaerah, {
				id: data.evaluasi_id ? +data.evaluasi_id : undefined,
				inovasi_indikator_id: inovasi_indikator.id,
				data_saat_ini: data.data_saat_ini,
				keterangan: data.keterangan,
				kategori: data.kategori,
				created_by: data.evaluasi_id ? undefined : username,
				updated_by: username,
			})
			.save();

		return res;
	}

	@Get('/{id}/indikator/{indikator_id}/evaluasi')
	async getEvaluasi(
		id: number,
		indikator_id: number,
		@Query() @Hidden() username = ''
	): Promise<SuccessResponse<EvaluasiResponse> | EvaluasiResponse> {
		const review = await this.repo.manager.findOneOrFail(ReviewInovasiDaerah, {
			where: { id },
		});
		const res = await this.repo.manager.findOne(InovasiIndikator, {
			where: {
				indikator_id,
				inovasi_id: review.inovasi_id,
				created_by: username,
			},
			relations: {
				evaluasi: true,
				inovasi: true,
			},
		});

		if (!res) {
			throw new ApiError({
				httpCode: HttpCode.NOT_FOUND,
				message: 'Data not found',
			});
		}

		return this.toEvaluasiResponse(res, review);
	}

	@Get()
	async get(
		@Query('url') @Hidden() url: string = '',
		@Query() @Hidden() username: string = '',
		@Queries() req: GetReviewInovasiRequest
	): Promise<PaginationResponse<PaginationResponseInterface, ReviewInovasiDaerahResponse[]> | PaginationData<ReviewInovasiDaerahResponse[]>> {
		const query = this.repo
			.createQueryBuilder(`e`)
			.leftJoinAndSelect('e.inovasi', 'i')
			.leftJoinAndSelect('i.profilPemda', 'ip')
			.leftJoinAndSelect('e.previews', 'ep')
			.leftJoinAndSelect('ep.evaluasi', 'epv')
			.where(`(lower(i.innovation_name) like lower(:q) or lower(ip.nama_daerah) ilike :nama_daerah)`, {
				q: `%${req.q ?? ''}%`,
				nama_daerah: `%${req.q ?? ''}%`,
			});

		if (req.pemda_id) {
			query.where('ip.id = :pemda_id', { pemda_id: req.pemda_id });
		}

		const user = await this.repo.manager.getRepository(User).findOneByOrFail({ username });
		if (user && user.role && user.role.name.toLowerCase() === 'user') {
			query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
		}

		const total = await query.getCount();
		const { limit, offset, page, order } = pagination.options(req.page, req.limit);

		query.limit(limit).offset(offset);

		for (const key in order) {
			query.addOrderBy(`e.${key}`, order[key]);
		}

		const res = (await query.getMany()).map<ReviewInovasiDaerahResponse>(response => this.toResponse(response));
		let paging = pagination.build(url, page, limit, total);

		return { paging, res };
	}

	@Delete('/{id}')
	async delete(id: number): Promise<SuccessResponse<string> | boolean> {
		const { affected } = await this.repo.delete({ id: id });

		if (affected && affected > 0) {
			return true;
		}
		return false;
	}

	@Patch('/{id}')
	async update(
		id: number,
		@Query() @Hidden() username: string = '',
		@Body() req: CreateReviewInovasiDaerahRequest
	): Promise<SuccessResponse<string> | boolean> {
		const update = this.repo.create({
			...req,
			updated_by: username,
		});
		const { affected } = await this.repo.update(id, update);

		if (!affected) {
			return false;
		}

		return true;
	}

	toEvaluasiResponse(data: InovasiIndikator, review: ReviewInovasiDaerah): EvaluasiResponse {
		const { evaluasi } = data;
		const res: EvaluasiResponse = {
			review_id: review.id.toString() ?? null,
			inovasi_id: data.inovasi_id.toString() ?? null,
			nama_inovasi: data.inovasi?.innovation_name ?? null,
			evaluasi_id: null,
			data_saat_ini: null,
			kategori: null,
			judul: null,
			keterangan: null,
		};

		if (evaluasi && evaluasi.length > 0) {
			const latestEval = evaluasi[evaluasi.length - 1];
			res.evaluasi_id = latestEval.id.toString();
			res.kategori = latestEval.kategori;
			res.data_saat_ini = latestEval.data_saat_ini;
			res.keterangan = latestEval.keterangan;
		}

		return res;
	}

	toIndikatorRespons(data: Indicator, evaluasi?: EvaluasiInovasiDaerah): IndikatorResponse {
		let skor = 0;
		let skorEvaluasi = 0;

		if (evaluasi) {
			switch (evaluasi.data_saat_ini?.toLowerCase()) {
				case 'sk kepala perangkat daerah':
					skor = 6;
					break;
				case 'sk kepala daerah':
					skor = 3;
					break;
				case 'peraturan kepala daerah':
				case 'daerah':
					skor = 9;
					break;

				default:
					skor = 0;
					break;
			}
		}
		skorEvaluasi = skor;

		if (evaluasi) {
			switch (evaluasi.catatan?.toLowerCase()) {
				case 'sesuai':
					skorEvaluasi += 50;
					break;
				case 'tidak sesuai':
					skorEvaluasi += 0;
					break;
				default:
					skorEvaluasi += 0;
					break;
			}
		}

		const res: IndikatorResponse = {
			indikator_id: data.id?.toString() ?? null,
			name: data.nama_indikator ?? null,
			informasi: data.keterangan ?? null,
			skor: skor ?? null,
			skor_evaluasi: skorEvaluasi ?? null,
		};

		return res;
	}

	async toProfilResponse(data: ReviewInovasiDaerah): Promise<ProfilInovasiResponse> {
		const { inovasi } = data;
		const { profilPemda, urusan } = inovasi;
		const profil = await this.repo.manager.findOne(ProfilPemda, {
			where: {
				id: profilPemda?.id,
			},
		});
		const res: Nullable<ProfilInovasiResponse> = {
			id: profilPemda?.id.toString() ?? null,
			nama_pemda: profilPemda?.nama_daerah ?? null,
			nama_inovasi: inovasi.government_name ?? null,
			tahapan_inovasi: inovasi.innovation_phase ?? null,
			inisiator_daerah: profilPemda?.nama_daerah ?? null,
			jenis_inovasi: inovasi.innovation_type ?? null,
			bentuk_inovasi_daerah: inovasi.innovation_form ?? null,
			tematik: inovasi.thematic ?? null,
			detail_tematik: inovasi.thematic_detail ?? null,
			urusan_pemerintah: urusan?.name ?? null,
			tingkatan: inovasi.innovation_initiator ?? null,
			waktu_uji_coba: inovasi.trial_time ?? null,
			waktu_penerapan: inovasi.implementation_time ?? null,
			opd: profil?.opd_yang_menangani ?? null,
			pemda: profil
				? {
						id: profil?.id?.toString() ?? null,
						email: profil?.email ?? null,
						nama_admin: profil?.nama_admin ?? null,
						name: profil?.nama_daerah ?? null,
						no_telp: profil?.no_telpon ?? null,
				  }
				: null,
			dokumen: null,
		};

		return res;
	}

	toResponse(data: ReviewInovasiDaerah): ReviewInovasiDaerahResponse {
		let skor = 0;
		switch (data.inovasi.innovation_phase) {
			case 'inisiatif':
				skor = 50;
				break;
			case 'uji coba':
				skor = 102;
				break;
			case 'penerapan':
				skor = 105;
				break;

			default:
				skor = 0;
				break;
		}
		let skor_verifikasi = skor;
		if (data.status == 'Rejected') skor_verifikasi = 10;
		if (data.status == 'Accept') skor_verifikasi += 50;
		return {
			id: data.id.toString(),
			judul: data.inovasi.innovation_name,
			nomor: data.random_number.toString(),
			waktu_penerapan: data.inovasi.implementation_time,
			skor: skor.toString(),
			skor_verifikasi: skor_verifikasi.toString(),
			waktu_pengiriman: data.inovasi.created_at.toISOString(),
			inovasi_id: data.inovasi_id?.toString(),
			qc: data.status ?? null,
			pemda:
				data.inovasi && data.inovasi.profilPemda
					? {
							id: data.inovasi.profilPemda.id.toString(),
							nama_pemda: data.inovasi.profilPemda.nama_daerah,
					  }
					: null,
			preview:
				data.previews && data.previews.length > 0
					? {
							id: data.previews[data.previews.length - 1].id.toString(),
							pemda: data.inovasi.profilPemda
								? {
										id: data.inovasi.profilPemda.id.toString(),
										nama_pemda: data.inovasi.profilPemda.nama_daerah,
								  }
								: null,
							judul: data.inovasi.innovation_name,
							status: data.status ?? '',
							alasan_ditolak:
								data.previews[data.previews.length - 1] && data.previews[data.previews.length - 1].evaluasi.length > 0
									? data.previews[data.previews.length - 1].evaluasi[data.previews[data.previews.length - 1].evaluasi.length - 1].keterangan
									: '',
					  }
					: null,
		};
	}
}
