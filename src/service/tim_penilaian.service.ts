import { TimPenilaian } from 'src/entity/TimPenilaian.entity';
import ApiError from 'src/providers/exceptions/api.error';
import TimPenilaianRepository from 'src/repository/tim_penilaian.repository';
import {
  CreateTimPenilaianRequest,
  ListTimPenilaian,
  ListTimPenilaianParams
} from 'src/schema/tim_penilaian.schema';
import { HttpCode } from 'src/types/http_code.enum';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import User from 'src/entity/User.entity';

@Route('/tim_penilaian')
@Tags('Master | Tim Penilaian')
@Security('bearer')
export default class TimPenilaianService {
  private repo = new TimPenilaianRepository();

  @Post('')
  async create(
    @Body() req: CreateTimPenilaianRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<TimPenilaian> | TimPenilaian> {
    const create = this.repo.create({
      ...req,
      created_by: username,
      updated_by: username
    });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<TimPenilaian> | TimPenilaian> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Query() @Hidden() username: string = '',
    @Queries() req: ListTimPenilaianParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, ListTimPenilaian>
    | PaginationData<ListTimPenilaian>
  > {
    const query = this.repo
      .createQueryBuilder('tp')
      .where(`lower(tp.nama) like lower(:q)`, { q: `%${req.q ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    const user = await this.repo.manager
      .getRepository(User)
      .findOneByOrFail({ username });
    if (user && user.role && user.role.is_super_admin === 't') {
      query.andWhere(`tp.created_by = (:created_by)`, { created_by: username });
    }

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`tp.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateTimPenilaianRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      ...req,
      updated_by: username
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
