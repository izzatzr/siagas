import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import Area from '../entity/Area.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import AreaRepository from '../repository/area.repository';
import { CreateAreaRequest, GetAreaRequest } from '../schema/area.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';

@Route('/wilayah')
@Tags('Master | Wilayah')
@Security('bearer')
export default class AreaService {
  private repo: AreaRepository;
  constructor() {
    this.repo = new AreaRepository();
  }

  @Post('')
  async create(
    @Body() req: CreateAreaRequest
  ): Promise<SuccessResponse<Area> | Area> {
    const regions = this.repo.create({
      name: req.name,
      region_id: req.region_id
    });

    await this.repo.insert(regions);

    return regions;
  }

  @Get('/{id}')
  async detail(id: number): Promise<SuccessResponse<Area> | Area> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = 'http://localhost',
    @Queries() req: GetAreaRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Array<Area>>
    | { paging: PaginationResponseInterface; res: Array<Area> }
  > {
    const query = this.repo
      .createQueryBuilder('a')
      .leftJoinAndSelect('a.region', 'region')
      .where(`lower(a.name) like lower(:name)`, {
        name: `%${req.q ?? ''}%`
      });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`a.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateAreaRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      name: req.name,
      region_id: req.region_id
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
