import { InovasiIndikator } from 'src/entity/InovasiIndikator.entity';
import { PaginationResponse as PaginationResponseInterface } from 'src/interface/pagination.interface';
import ApiError from 'src/providers/exceptions/api.error';
import pagination from 'src/providers/pagination';
import InovasiIndikatorRepository from 'src/repository/inovasi_indikator.repository';
import { PaginationData } from 'src/schema/base.schema';
import {
  InovasiIndikatorParams,
  ListInovasiIndikator
} from 'src/schema/inovasi_indikator.schema';
import { HttpCode } from 'src/types/http_code.enum';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { Get, Hidden, Queries, Query, Route, Security, Tags } from 'tsoa';

@Route('/inovasi_pemerintah_daerah')
@Tags('Database Inovasi Daerah | Inovasi Daerah | Inovasi Indikator')
@Security('bearer')
export default class InovasiIndikatorService {
  private repo = new InovasiIndikatorRepository();

  @Get('/{inovasi_id}/indikator')
  async get(
    inovasi_id: string,
    @Query('url') @Hidden() url: string = '',
    @Query() @Hidden() username: string = '',
    @Queries() req: InovasiIndikatorParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, ListInovasiIndikator>
    | PaginationData<ListInovasiIndikator>
  > {
    const query = this.repo
      .createQueryBuilder(`ii`)
      .leftJoinAndSelect(`ii.indikator`, 'i')
      .leftJoinAndSelect(`ii.files`, 'f')
      .where(`ii.inovasi_id = :inovasi_id`, { inovasi_id });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`ii.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Get('/{inovasi_id}/indikator/{indikator_id}')
  async detail(
    inovasi_id: number,
    indikator_id: number
  ): Promise<SuccessResponse<InovasiIndikator> | InovasiIndikator> {
    const res = await this.repo.findOneBy({ inovasi_id, indikator_id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }
}
