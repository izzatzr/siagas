import PemdaIndikatorRepository from 'src/repository/pemda_indikator.repository';
import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import {
  CreatePemdaIndikatorRequest,
  ListPemdaIndikator,
  PemdaIndikatorParams
} from 'src/schema/pemda_indikator.schema';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';
import { PemdaIndikator } from 'src/entity/PemdaIndikator.entity';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';

@Route('/profil_pemda')
@Tags('Database Inovasi Daerah | Profil Pemda | Pemda Indikator')
@Security('bearer')
export default class PemdaIndikatorService {
  private repo = new PemdaIndikatorRepository();

  @Post('/{pemda_id}/indikator')
  async create(
    pemda_id: string,
    @Body() data: CreatePemdaIndikatorRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<PemdaIndikator> | PemdaIndikator> {
    const indikator = await this.repo
      .create({
        pemda_id: +pemda_id,
        informasi: data.informasi,
        keterangan: data.keterangan,
        indikator_id: data.indikator_id,
        catatan: data.catatan,
        created_by: username,
        updated_by: username
      })
      .save();

    return indikator;
  }

  @Patch('/{pemda_id}/indikator/{pemda_indikator_id}')
  async update(
    pemda_id: string,
    pemda_indikator_id: string,
    @Query() @Hidden() username: string = '',
    @Body() data: CreatePemdaIndikatorRequest
  ): Promise<SuccessResponse<PemdaIndikator> | PemdaIndikator> {
    const indikator = await this.repo.findOne({
      where: { id: +pemda_indikator_id }
    });

    if (!indikator) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    await this.repo
      .create({
        ...indikator,
        ...data,
        id: +pemda_indikator_id,
        updated_by: username
      })
      .save();

    return indikator;
  }

  @Delete('/{pemda_id}/indikator/{pemda_indikator_id}')
  async delete(
    pemda_id: string,
    pemda_indikator_id: string
  ): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: +pemda_indikator_id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Get('/{pemda_id}/indikator')
  async get(
    pemda_id: string,
    @Query('url') @Hidden() url: string = '',
    @Query() @Hidden() username: string = '',
    @Queries() req: PemdaIndikatorParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, ListPemdaIndikator>
    | PaginationData<ListPemdaIndikator>
  > {
    const query = this.repo
      .createQueryBuilder(`pi`)
      .leftJoinAndSelect(`pi.indicator`, 'i')
      .leftJoinAndSelect(`pi.files`, 'f')
      .where(`pi.pemda_id = :pemda_id`, { pemda_id });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`pi.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Get('/{pemda_id}/indikator/{indikator_id}')
  async detail(
    pemda_id: number,
    indikator_id: number
  ): Promise<SuccessResponse<PemdaIndikator> | PemdaIndikator> {
    const res = await this.repo.findOneBy({ pemda_id, indikator_id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }
}
