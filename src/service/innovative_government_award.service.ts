import InnovativeGovernmentAwardRepository from 'src/repository/innovative_government_award.repository';
import {
  PeringkatHasilReviewNominatorRequest,
  PeringkatHasilReviewParams,
  PeringkatHasilReviewResponse,
  PrestasiResponse,
  RangkingResponse,
  toPeringkatResponse,
  toPrestasiResponse,
  toRangkingResponse
} from 'src/schema/innovative_government_award.schema';
import {
  Body,
  Get,
  Hidden,
  Patch,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import { PaginationResponse as PaginationResponseInterface } from 'src/interface/pagination.interface';
import pagination from 'src/providers/pagination';
import { PaginationData } from 'src/schema/base.schema';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { PeringkatHasilReview } from 'src/entity/PeringkatHasilReview.entity';
import User from 'src/entity/User.entity';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import { DownloadType } from 'src/types/lib';
import ApiError from 'src/providers/exceptions/api.error';
import { FindOptionsWhere, ILike } from 'typeorm';
import ExportService from './export.service';

@Route('/innovative_government_award')
@Tags('Innovative Government Award')
@Security('bearer')
export default class InnovativeGovernmentAwardService {
  private repo = new InnovativeGovernmentAwardRepository();
  private exportService = new ExportService();

  @Get('/peringkat_hasil_review')
  async peringkat_hasil_review(
    @Query() @Hidden() url = '',
    @Query() @Hidden() username = '',
    @Queries() req: PeringkatHasilReviewParams
  ): Promise<
    | PaginationResponse<
        PaginationResponseInterface,
        PeringkatHasilReviewResponse[]
      >
    | PaginationData<PeringkatHasilReviewResponse[]>
  > {
    const query = this.repo.manager
      .createQueryBuilder(PeringkatHasilReview, 'phr')
      .where(
        `lower(nama_daerah) ilike lower(:q) or lower(predikat) ilike lower(:q)`,
        { q: `%${req.q ?? ''}%` }
      );

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    const user = await this.repo.manager
      .getRepository(User)
      .findOneByOrFail({ username });
    if (user && user.role && user.role.is_super_admin === 't') {
      query.andWhere(`phr.created_by = (:created_by)`, {
        created_by: username
      });
    }

    query.limit(limit).offset(offset);

    const res = (await query.getMany()).map<PeringkatHasilReviewResponse>(
      value => toPeringkatResponse(value, total)
    );
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Tags('Export')
  @Get('/peringkat_hasil_review/download/{type}')
  async peringkat_hasil_review_download(
    type: DownloadType,
    @Query() @Hidden() username = '',
    @Queries() req: PeringkatHasilReviewParams
  ): Promise<string> {
    try {
      if (type === 'pdf')
        throw new ApiError({
          httpCode: 403,
          message: 'Download PDF under maintenance'
        });

      let where:
        | FindOptionsWhere<PeringkatHasilReview>
        | FindOptionsWhere<PeringkatHasilReview>[] = {
        nama_daerah: ILike(`%${req.q ?? ''}%`)
      };

      const user = await this.repo.manager
        .getRepository(User)
        .findOneByOrFail({ username });
      if (user && user.role && user.role.is_super_admin === 't') {
        where = { ...where, created_by: username };
      }

      const [data, total] = await this.repo.manager.findAndCount(
        PeringkatHasilReview,
        {
          where
        }
      );

      const path = await this.exportService.download({
        data: data.map<PeringkatHasilReviewResponse>(laporan =>
          toPeringkatResponse(laporan, total)
        ),
        name: 'peringkat-hasil-review'
      });

      return path;
    } catch (error) {
      throw error;
    }
  }

  @Patch('/peringkat_hasil_review/{pemda_id}')
  async nominator_peringkat_hasil_review(
    pemda_id: number,
    @Body() req: PeringkatHasilReviewNominatorRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.manager
      .getRepository(ProfilPemda)
      .update({ id: pemda_id }, { nominator: req.nominator });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Get('/prestasi')
  async prestasi(
    @Query() @Hidden() url = '',
    @Query() @Hidden() username = '',
    @Queries() req: PeringkatHasilReviewParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, PrestasiResponse[]>
    | PaginationData<PrestasiResponse[]>
  > {
    const query = this.repo.manager
      .createQueryBuilder(PeringkatHasilReview, 'phr')
      .where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${req.q ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    const user = await this.repo.manager
      .getRepository(User)
      .findOneByOrFail({ username });
    if (user && user.role && user.role.is_super_admin === 't') {
      query.andWhere(`phr.created_by = (:created_by)`, {
        created_by: username
      });
    }

    query.limit(limit).offset(offset);

    const res = (await query.getMany()).map<PrestasiResponse>(value =>
      toPrestasiResponse(value, total)
    );
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Tags('Export')
  @Get('/prestasi/download/{type}')
  async prestasi_download(
    type: DownloadType,
    @Query() @Hidden() username = '',
    @Queries() req: PeringkatHasilReviewParams
  ): Promise<string> {
    try {
      if (type === 'pdf')
        throw new ApiError({
          httpCode: 403,
          message: 'Download PDF under maintenance'
        });

      let where:
        | FindOptionsWhere<PeringkatHasilReview>
        | FindOptionsWhere<PeringkatHasilReview>[] = {
        nama_daerah: ILike(`%${req.q ?? ''}%`)
      };

      const user = await this.repo.manager
        .getRepository(User)
        .findOneByOrFail({ username });
      if (user && user.role && user.role.is_super_admin === 't') {
        where = { ...where, created_by: username };
      }

      const [data, total] = await this.repo.manager.findAndCount(
        PeringkatHasilReview,
        {
          where
        }
      );

      const path = await this.exportService.download({
        data: data.map<PrestasiResponse>(laporan =>
          toPrestasiResponse(laporan, total)
        ),
        name: 'prestasi-dan-hasil-lapangan'
      });

      return path;
    } catch (error) {
      throw error;
    }
  }

  @Get('/rangking')
  async rangking(
    @Query() @Hidden() url = '',
    @Query() @Hidden() username = '',
    @Queries() req: PeringkatHasilReviewParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, RangkingResponse[]>
    | PaginationData<RangkingResponse[]>
  > {
    const query = this.repo.manager
      .createQueryBuilder(PeringkatHasilReview, 'phr')
      .where(`lower(nama_daerah) ilike lower(:q)`, { q: `%${req.q ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    const user = await this.repo.manager
      .getRepository(User)
      .findOneByOrFail({ username });
    if (user && user.role && user.role.is_super_admin === 't') {
      query.andWhere(`phr.created_by = (:created_by)`, {
        created_by: username
      });
    }

    query.limit(limit).offset(offset);

    const res = (await query.getMany()).map<RangkingResponse>(value =>
      toRangkingResponse(value, total)
    );
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Tags('Export')
  @Get('/rangking/download/{type}')
  async rangking_download(
    type: DownloadType,
    @Query() @Hidden() username = '',
    @Queries() req: PeringkatHasilReviewParams
  ): Promise<string> {
    try {
      if (type === 'pdf')
        throw new ApiError({
          httpCode: 403,
          message: 'Download PDF under maintenance'
        });

      let where:
        | FindOptionsWhere<PeringkatHasilReview>
        | FindOptionsWhere<PeringkatHasilReview>[] = {
        nama_daerah: ILike(`%${req.q ?? ''}%`)
      };

      const user = await this.repo.manager
        .getRepository(User)
        .findOneByOrFail({ username });
      if (user && user.role && user.role.is_super_admin === 't') {
        where = { ...where, created_by: username };
      }

      const [data, total] = await this.repo.manager.findAndCount(
        PeringkatHasilReview,
        {
          where
        }
      );

      const path = await this.exportService.download({
        data: data.map<RangkingResponse>(laporan =>
          toRangkingResponse(laporan, total)
        ),
        name: 'rangking-siagas'
      });

      return path;
    } catch (error) {
      throw error;
    }
  }
}
