import { PaktaIntegritas } from 'src/entity/PaktaIntegritas.entity';
import PaktaIntegritasRepository from 'src/repository/pakta-integritas.repository';
import {
  Delete,
  FormField,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags,
  UploadedFile
} from 'tsoa';
import FileService from './file.service';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import User from 'src/entity/User.entity';
import File from 'src/entity/File.entity';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import {
  ListPaktaIntegritas,
  PaktaIntegritasParams
} from 'src/schema/pakta-integritas.schema';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from 'src/schema/base.schema';
import pagination from 'src/providers/pagination';

@Route('/pakta_integritas')
@Tags('Database Inovasi Daerah | Pakta Integritas')
@Security('bearer')
export default class PaktaIntegritasService {
  private repo = new PaktaIntegritasRepository();
  private fileService: FileService = new FileService();

  @Post('')
  async create(
    @FormField() user_id: string,
    @UploadedFile() file?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<PaktaIntegritas> | PaktaIntegritas> {
    const create = this.repo.create({
      user: await this.repo.manager
        .getRepository(User)
        .findOneByOrFail({ id: +user_id }),
      created_by: username,
      updated_by: username
    });

    if (file) {
      const uploaded = await this.fileService.create(file, undefined, username);
      create.upload = uploaded as File;
    }

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<PaktaIntegritas> | PaktaIntegritas> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('/latest')
  async getLatest(
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<PaktaIntegritas> | PaktaIntegritas> {
    const query = await this.repo
      .createQueryBuilder('e')
      .leftJoinAndSelect('e.user', 'u')
      .leftJoinAndSelect('e.upload', 'd');

    const user = await this.repo.manager
      .getRepository(User)
      .findOneByOrFail({ username });
    if (user && user.role && user.role.is_super_admin === 't') {
      query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
    }

    query.addOrderBy(`e.id`, 'DESC');

    const res = await query.getOneOrFail();
    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Query() @Hidden() username: string = '',
    @Queries() req: PaktaIntegritasParams
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, ListPaktaIntegritas>
    | PaginationData<ListPaktaIntegritas>
  > {
    const query = this.repo
      .createQueryBuilder('e')
      .leftJoinAndSelect('e.user', 'u')
      .leftJoinAndSelect('e.upload', 'd')
      .where(`lower(u.nama_pemda) like lower(:q)`, { q: `%${req.q ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    const user = await this.repo.manager
      .getRepository(User)
      .findOneByOrFail({ username });
    if (user && user.role && user.role.is_super_admin === 't') {
      query.andWhere(`e.created_by = (:created_by)`, { created_by: username });
    }

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`e.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @FormField() user_id: string,
    @UploadedFile() file?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      user: await this.repo.manager
        .getRepository(User)
        .findOneByOrFail({ id: +user_id }),
      updated_by: username
    });

    if (file) {
      const uploaded = await this.fileService.create(file, undefined, username);
      update.upload = uploaded as File;
    }

    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
