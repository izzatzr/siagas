import { IHealthService } from './interface/ihealth.service';

class HealthService implements IHealthService {
  async health(): Promise<string> {
    return 'Server is healthy';
  }
}

export default HealthService;
