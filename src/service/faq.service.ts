import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import Faq from '../entity/Faq.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import FaqRepository from '../repository/faq.repository';
import { PaginationData } from '../schema/base.schema';
import { CreateFaqRequest } from '../schema/faq.schema';
import { GetSettingRequest } from '../schema/setting.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';

@Route('/faq')
@Tags('Master | FAQ')
@Security('bearer')
export default class FaqService {
  private repo: FaqRepository = new FaqRepository();

  @Post('')
  async create(
    @Body() req: CreateFaqRequest
  ): Promise<SuccessResponse<Omit<Faq, ''>> | Faq> {
    const create = this.repo.create({
      answer: req.answer,
      question: req.question
    });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(id: number): Promise<SuccessResponse<Omit<Faq, ''>> | Faq> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetSettingRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Omit<Faq, ''>[]>
    | PaginationData<Omit<Faq, ''>[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(answer) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      })
      .orWhere(`lower(question) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      });

    const total = await query.getCount();
    const { limit, offset, page } = pagination.options(req.page, req.limit);

    query.limit(limit).offset(offset);

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateFaqRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      answer: req.answer,
      question: req.question
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
