import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import TuxedoRepository from '../repository/tuxedo.repository';
import { CreateTuxedoRequest, GetTuxedoRequest } from '../schema/tuxedo.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import Tuxedo from '../entity/Tuxedo.entity';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import { PaginationData } from '../schema/base.schema';
import pagination from '../providers/pagination';

@Route('/tuxedo')
@Tags('Konfigurasi | Tuxedo')
@Security('bearer')
export default class TuxedoService {
  private repo: TuxedoRepository = new TuxedoRepository();

  @Post('')
  async create(
    @Body() req: CreateTuxedoRequest
  ): Promise<SuccessResponse<Omit<Tuxedo, ''>> | Tuxedo> {
    const create = this.repo.create({
      title: req.title,
      slug: req.slug,
      section: req.section,
      content: req.content
    });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<Omit<Tuxedo, ''>> | Tuxedo> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetTuxedoRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Omit<Tuxedo, ''>[]>
    | PaginationData<Omit<Tuxedo, ''>[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(title) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      })
      .orWhere(`lower(slug) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      })
      .orWhere(`lower(section) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      });

    const total = await query.getCount();
    const { limit, offset, page } = pagination.options(req.page, req.limit);

    query.limit(limit).offset(offset);

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateTuxedoRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      title: req.title,
      slug: req.slug,
      section: req.section,
      content: req.content
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
