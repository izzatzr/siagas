import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import Cluster from '../entity/Cluster.entity';
import ApiError from '../providers/exceptions/api.error';
import ClusterRepository from '../repository/cluster.repository';
import ClusterDetailRepository from '../repository/cluster_detail.repository';
import { PaginationData } from '../schema/base.schema';
import {
  CreateClusterRequest,
  GetClusterRequest
} from '../schema/cluster.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import pagination from '../providers/pagination';
import {
  CreateClusterDetailRequest,
  GetClusterDetailRequest
} from '../schema/clusterDetail.schema';
import ClusterDetail from '../entity/ClusterDetail.entity';

@Route('/cluster')
@Tags('Master | Daerah | Kluster')
@Security('bearer')
export default class ClusterService {
  private mainRepo: ClusterRepository = new ClusterRepository();
  private detailRepo: ClusterDetailRepository = new ClusterDetailRepository();

  @Post('')
  async create(
    @Body() req: CreateClusterRequest
  ): Promise<SuccessResponse<Omit<Cluster, 'details'>> | Cluster> {
    const create = this.mainRepo.create({
      name: req.name
    });

    await this.mainRepo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<Omit<Cluster, 'details'>> | Cluster> {
    const res = await this.mainRepo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetClusterRequest
  ): Promise<
    | PaginationResponse<
        PaginationResponseInterface,
        Omit<Cluster, 'details'>[]
      >
    | PaginationData<Omit<Cluster, 'details'>[]>
  > {
    const query = this.mainRepo
      .createQueryBuilder()
      .where(`lower(name) like lower(:q)`, { q: `%${req.q ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(key, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.mainRepo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateClusterRequest,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.mainRepo.create({
      name: req.name
    });
    const { affected } = await this.mainRepo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }

  @Get('/{id}/detail')
  async getDetails(
    id: number,
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetClusterDetailRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, ClusterDetail[]>
    | PaginationData<ClusterDetail[]>
  > {
    const query = this.detailRepo
      .createQueryBuilder('d')
      .leftJoinAndSelect('d.region', 'r')
      .leftJoinAndSelect('d.cluster', 'c')
      .where(`lower(r.name) like lower(:q)`, { q: `%${req.q ?? ''}%` })
      .orWhere(`lower(c.name) like lower(:q)`, { q: `%${req.q ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(`d.${key}`, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Post('/{id}/detail')
  async createDetail(
    id: number,
    @Body() req: CreateClusterDetailRequest
  ): Promise<SuccessResponse<ClusterDetail> | ClusterDetail> {
    const detail = this.detailRepo.create({
      cluster_id: id,
      region_id: req.region_id
    });

    await this.detailRepo.insert(detail);

    return detail;
  }

  @Get('/{id}/detail/{region_id}')
  async getDetail(
    id: number,
    region_id: number
  ): Promise<SuccessResponse<ClusterDetail> | ClusterDetail> {
    const detail = await this.detailRepo.findOneBy({
      cluster_id: id,
      region_id: region_id
    });

    if (!detail) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found!'
      });
    }

    return detail;
  }

  @Patch('/{id}/detail/{region_id}')
  async updateDetail(
    id: number,
    region_id: number,
    @Body() req: CreateClusterDetailRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.detailRepo.delete({
      cluster_id: id,
      region_id: region_id
    });

    if (!affected || affected <= 0) {
      throw new ApiError({
        httpCode: HttpCode.BAD_REQUEST,
        message: 'Error update data!'
      });
    }

    const detail = this.detailRepo.create({
      cluster_id: id,
      region_id: req.region_id
    });

    const { identifiers } = await this.detailRepo.insert(detail);

    if (!identifiers) {
      return false;
    }
    return true;
  }

  @Delete('/{id}/detail/{region_id}')
  async deleteDetail(
    id: number,
    region_id: number
  ): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.detailRepo.delete({
      cluster_id: id,
      region_id: region_id
    });

    if (affected && affected > 0) {
      return true;
    }

    return false;
  }
}
