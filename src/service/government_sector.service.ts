import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import GovernmentSector from '../entity/GovernmentSector';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import GovernmentSectorRepository from '../repository/governmentSector.repository';
import {
  CreateGovernmentSectorRequest,
  GetGovernmentSectorRequest
} from '../schema/government_sector.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';

@Route('/urusan_pemerintahan')
@Tags('Master | Urusan Pemerintah')
@Security('bearer')
export default class GovernmentSectorService {
  private repo: GovernmentSectorRepository;

  constructor() {
    this.repo = new GovernmentSectorRepository();
  }

  /**
   * parameter name: Nama dari urusan pemerintah
   * paremeter deadline: diisi dengan format YYYY-MM-DD
   */
  @Post('')
  async create(
    @Body() req: CreateGovernmentSectorRequest
  ): Promise<GovernmentSector> {
    const govSect: GovernmentSector = this.repo.create({
      name: req.name,
      deadline: req.deadline
    });
    await this.repo.insert(govSect);

    return govSect;
  }

  @Get('')
  async get(
    @Queries() req: GetGovernmentSectorRequest,
    @Query('url') @Hidden() url: string = 'http://localhost'
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Array<GovernmentSector>>
    | { paging: PaginationResponseInterface; res: Array<GovernmentSector> }
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(name) like lower(:name)`, { name: `%${req.name ?? ''}%` });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(key, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Get('/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<GovernmentSector> | GovernmentSector> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  /**
   * parameter name: Nama dari urusan pemerintah
   * paremeter deadline: diisi dengan format YYYY-MM-DD
   */
  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateGovernmentSectorRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({ name: req.name, deadline: req.deadline });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
