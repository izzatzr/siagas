import {
  Delete,
  FormField,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags,
  UploadedFile
} from 'tsoa';
import Document from '../entity/Document.entity';
import File from '../entity/File.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import DocumentRepository from '../repository/document.repository';
import { PaginationData } from '../schema/base.schema';
import { GetDocumentRequest } from '../schema/document.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import FileService from './file.service';

@Route('/dokumen')
@Tags('Master | Dokumen')
@Security('bearer')
export default class DocumentService {
  private repo: DocumentRepository = new DocumentRepository();
  private fileService: FileService = new FileService();

  @Post('')
  async create(
    @FormField() content: string,
    @FormField() title: string,
    @FormField() category_id?: number,
    @UploadedFile() document?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<Omit<Document, ''>> | Document> {
    const create = this.repo.create({
      title: title,
      content: content
    });
    if (category_id) {
      create.category_id = category_id;
    }

    if (document) {
      const file = await this.fileService.create(document, undefined, username);
      create.document = file as File;
    }

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(
    id: number,
    @Query() @Hidden() host: string = ''
  ): Promise<SuccessResponse<Omit<Document, ''>> | Document> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    if (res.document) {
      res.document.full_path = host + '/' + res.document.path;
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Query('host') @Hidden() host: string = '',
    @Queries() req: GetDocumentRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Omit<Document, ''>[]>
    | PaginationData<Omit<Document, ''>[]>
  > {
    const query = this.repo
      .createQueryBuilder('a')
      .leftJoinAndSelect('a.document', 'f')
      .leftJoinAndSelect('a.category', 'c')
      .addSelect(`'${host}/' || f.path`, 'f_full_path')
      .where(`lower(title) like lower(:q)`, {
        q: `%${req.q ?? ''}%`
      });

    if (req.category) {
      query.andWhere(`a.category_id = :category`, { category: req.category });
    }

    const total = await query.getCount();
    const { limit, offset, page } = pagination.options(req.page, req.limit);

    query.limit(limit).offset(offset);

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @FormField() content: string,
    @FormField() title: string,
    @FormField() category_id?: number,
    @UploadedFile() document?: Express.Multer.File,
    @Query() @Hidden() username: string = ''
  ): Promise<SuccessResponse<string> | boolean> {
    let file_id: number = 0;
    if (document) {
      const oldData = (await this.detail(id)) as Document;

      if (
        !oldData.document ||
        (oldData.document && oldData.document.name != document.filename)
      ) {
        const file = (await this.fileService.create(
          document,
          undefined,
          username
        )) as File;
        file_id = file.id;
      }
    }

    const update = this.repo.create({
      title: title,
      content: content
    });
    if (category_id) {
      update.category_id = category_id;
    }
    if (file_id > 0) {
      update.document_id = file_id;
    }
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
