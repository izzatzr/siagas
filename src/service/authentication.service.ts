import { Body, Get, Post, Query, Route, Security, Tags } from 'tsoa';
import { signJwt } from '../middleware/jwt.middleware';
import Config from '../providers/config';
import ApiError from '../providers/exceptions/api.error';
import Hash from '../providers/hash';
import RoleRepository from '../repository/role.repository';
import UserRepository from '../repository/user.repository';
import { LoginRequest, TokenPayload, TokenResponse } from '../schema/authentication.schema';
import { HttpCode } from '../types/http_code.enum';
import { SuccessResponse } from '../types/response.type';

@Route('/authentication')
@Tags('Authentication')
export default class AuthenticationService {
	private repo: UserRepository;
	constructor() {
		this.repo = new UserRepository();
	}

	@Post('/login')
	async login(@Body() req: LoginRequest): Promise<SuccessResponse<TokenResponse> | TokenResponse> {
		const userPass = await this.repo
			.createQueryBuilder('u')
			.select('u.id', 'id')
			.addSelect('u.password')
			.where(`u.username = :username`, { username: req.username })
			.getOne();

		if (!userPass || !new Hash().compare(req.password, userPass.password)) {
			throw new ApiError({
				httpCode: HttpCode.UNAUTHORIZED,
				message: 'Unauthorized!',
			});
		}

		const user = await this.repo.findOneBy({ username: req.username });
		if (!user) {
			throw new ApiError({
				httpCode: HttpCode.UNAUTHORIZED,
				message: 'User not found!',
			});
		}
		const role = await new RoleRepository().findOneByOrFail({
			id: user.role_id,
		});
		const token = signJwt({ user_id: user.id, username: user.username, role: role }, req.remember_me);

		const now = new Date();
		const expiredAt = now.setDate(now.getDate() + Config.load().token_expired_in_day);
		const res: TokenResponse = {
			token: token,
			role: role,
			user: user,
			token_expired_at: req.remember_me ? null : expiredAt,
		};
		return res;
	}

	@Get('/reserved_email')
	async checkEmail(@Query() email: string): Promise<SuccessResponse<boolean> | boolean> {
		const user = await this.repo.findOneBy({ email: email });

		if (!user) {
			return false;
		}
		return true;
	}

	@Get('/me')
	@Security('bearer')
	async getMe(): Promise<SuccessResponse<TokenPayload>> {
		throw new ApiError({ httpCode: HttpCode.OK, message: 'Success' });
	}
}
