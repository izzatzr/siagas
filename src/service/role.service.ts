import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Security,
  Tags
} from 'tsoa';
import Role from '../entity/Role.entity';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import ApiError from '../providers/exceptions/api.error';
import pagination from '../providers/pagination';
import RoleRepository from '../repository/role.repository';
import { PaginationData } from '../schema/base.schema';
import { CreateRoleRequest, GetRoleRequest } from '../schema/role.schema';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';

@Route('/role')
@Tags('Role')
@Security('bearer')
export default class RoleService {
  private repo: RoleRepository;
  constructor() {
    this.repo = new RoleRepository();
  }

  @Post('')
  async create(
    @Body() req: CreateRoleRequest
  ): Promise<SuccessResponse<Role> | Role> {
    const create = this.repo.create({ name: req.name, is_creator: 't' });

    await this.repo.insert(create);

    return create;
  }

  @Get('/{id}')
  async detail(id: number): Promise<SuccessResponse<Role> | Role> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('')
  async get(
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetRoleRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, Role[]>
    | PaginationData<Role[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(name) like lower(:q)`, { q: `%${req.name ?? ''}%` })
      .where(`is_creator = 't'`);

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    query.limit(limit).offset(offset);

    for (const key in order) {
      query.addOrderBy(key, order[key]);
    }

    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/{id}')
  async update(
    id: number,
    @Body() req: CreateRoleRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      name: req.name,
      is_creator: 't'
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
