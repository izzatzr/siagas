import {
  Body,
  FormField,
  Hidden,
  Post,
  Query,
  Route,
  Security,
  Tags,
  UploadedFile
} from 'tsoa';
import * as path from 'path';
import File from '../entity/File.entity';
import FileRepository from '../repository/file.repository';
import { UploadFileRequest } from '../schema/file.schema';
import { SuccessResponse } from '../types/response.type';

@Route('/file')
@Tags('File')
@Security('bearer')
export default class FileService {
  private repo: FileRepository = new FileRepository();

  @Post('')
  async create(
    @UploadedFile() file: Express.Multer.File,
    @FormField() name?: string,
    @Query()
    @Hidden()
    username: string = ''
  ): Promise<SuccessResponse<File> | File> {
    const filepath = `${file.destination.replace('public/', '')}${
      file.filename
    }`;
    const create = this.repo.create({
      name: name ? name : file.filename,
      path: filepath,
      extension: path.extname(file.originalname),
      size: file.size.toString(),
      uploaded_by: username
    });

    await this.repo.insert(create);

    return create;
  }
}
