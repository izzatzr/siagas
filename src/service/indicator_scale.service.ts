import {
  Body,
  Delete,
  Get,
  Hidden,
  Patch,
  Post,
  Queries,
  Query,
  Route,
  Tags
} from 'tsoa';
import IndicatorScale from '../entity/IndicatorScale.entity';
import ApiError from '../providers/exceptions/api.error';
import IndicatorScaleRepository from '../repository/indicator_scale.repository';
import {
  CreateIndicatorScaleRequest,
  GetIndicatorScaleRequest
} from '../schema/indicator_scale.scheme';
import { HttpCode } from '../types/http_code.enum';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import pagination from '../providers/pagination';
import IndicatorRepository from '../repository/indicator.repository';

@Route('/indikator')
@Tags('Master | Indikator | Skala Indikator')
export default class IndicatorScaleService {
  private repo: IndicatorScaleRepository;
  private indicatorRepo: IndicatorRepository;

  constructor() {
    this.repo = new IndicatorScaleRepository();
    this.indicatorRepo = new IndicatorRepository();
  }

  @Post('/{indikator_id}/skala')
  async create(
    indikator_id: number,
    @Body() req: CreateIndicatorScaleRequest
  ): Promise<SuccessResponse<IndicatorScale> | IndicatorScale> {
    const scale = this.repo.create({
      value: req.nilai,
      start_at: req.mulai_dari,
      finish_at: req.sampai_dengan,
      description: req.keterangan
    });

    const { identifiers } = await this.repo.insert(scale);
    const res = await this.repo.findOneBy({ id: identifiers[0].id });
    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Error saved data'
      });
    }

    return res;
  }

  @Get('/skala/{id}')
  async detail(
    id: number
  ): Promise<SuccessResponse<IndicatorScale> | IndicatorScale> {
    const res = await this.repo.findOneBy({ id: id });

    if (!res) {
      throw new ApiError({
        httpCode: HttpCode.NOT_FOUND,
        message: 'Data not found'
      });
    }

    return res;
  }

  @Get('/{indikator_id}/skala')
  async get(
    indikator_id: number,
    @Query('url') @Hidden() url: string = '',
    @Queries() req: GetIndicatorScaleRequest
  ): Promise<
    | PaginationResponse<PaginationResponseInterface, IndicatorScale[]>
    | PaginationData<IndicatorScale[]>
  > {
    const query = this.repo
      .createQueryBuilder()
      .where(`lower(description) like lower(:q)`, { q: `%${req.q ?? ''}%` })
      .andWhere(`indicator_id = :indikator_id`, { indikator_id: indikator_id });

    const total = await query.getCount();
    const { limit, offset, page, order } = pagination.options(
      req.page,
      req.limit
    );

    for (const key in order) {
      query.addOrderBy(key, order[key]);
    }

    query.limit(limit).offset(offset);
    const res = await query.getMany();
    let paging = pagination.build(url, page, limit, total);

    return { paging, res };
  }

  @Delete('/skala/{id}')
  async delete(id: number): Promise<SuccessResponse<string> | boolean> {
    const { affected } = await this.repo.delete({ id: id });

    if (affected && affected > 0) {
      return true;
    }
    return false;
  }

  @Patch('/skala/{id}')
  async update(
    id: number,
    @Body() req: CreateIndicatorScaleRequest
  ): Promise<SuccessResponse<string> | boolean> {
    const update = this.repo.create({
      value: req.nilai,
      start_at: req.mulai_dari,
      finish_at: req.sampai_dengan,
      description: req.keterangan
    });
    const { affected } = await this.repo.update(id, update);

    if (!affected) {
      return false;
    }
    return true;
  }
}
