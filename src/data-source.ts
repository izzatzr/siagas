import { DataSource } from 'typeorm';
import Config from './providers/config';
import * as path from 'path';

var types = require('pg').types
types.setTypeParser(types.builtins.NUMERIC, (value: string) => parseFloat(value));

const migrationsDir = path.normalize(path.join(__dirname, 'migrations', '*.{ts,js}'));
const entityDir = path.normalize(path.join(__dirname, 'entity', '*.{ts,js}'));
if (process.platform == 'win32') {
	migrationsDir.replace(/\\/g, '/');
	entityDir.replace(/\\/g, '/');
}

export const AppDataSource = new DataSource({
	type: 'postgres',
	host: Config.load().database_host,
	port: parseInt(Config.load().database_port),
	username: Config.load().database_user,
	password: Config.load().database_password,
	database: Config.load().database_name,
	synchronize: false,
	entities: [entityDir],
	migrationsRun: false,
	// migrations: [migrationsDir],
	logging: ['error', 'warn', 'log'],
	// migrationsTableName: `${Config.load().app_name.toLowerCase()}_migrations`,
});

AppDataSource.initialize()
	.then(() => {
		console.log('Data Source has been initialized!');
	})
	.catch(err => {
		console.error('Error during Data Source initialization', err);
	});
