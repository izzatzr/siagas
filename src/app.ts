import moment from 'moment';
import Express from './providers/express';

class App {
  public loadServer(): void {
    moment.locale('id');
    Express.init();
  }
}

export default new App();
