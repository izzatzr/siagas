import { Entity, ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
  expression: `select
    gi.pemda_id,
    gs.name as urusan_pemerintahan,
    (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where gi2.government_sector_id = gi.government_sector_id and lower(rid.status) = 'accept' and gi2.pemda_id = gi.pemda_id) total_disetujui,
    (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where gi2.government_sector_id = gi.government_sector_id and (lower(rid.status) = 'pending' or lower(rid.status) = 'rejected') and gi2.pemda_id = gi.pemda_id) total_ditolak,
    (select count(rid.id) from review_inovasi_daerah rid left join government_innovations gi2 on gi2.id = rid.inovasi_id where gi2.government_sector_id = gi.government_sector_id and gi2.pemda_id = gi.pemda_id) total_keseluruhan
  from government_sectors gs
  left join government_innovations gi on gi.government_sector_id = gs.id
  group by gi.pemda_id, gs.name, gi.government_sector_id`
})
export class LaporanUrusanInovasi {
  @ViewColumn()
  pemda_id: number;

  @ViewColumn()
  urusan_pemerintahan: string;

  @ViewColumn()
  total_disetujui: number;

  @ViewColumn()
  total_ditolak: number;

  @ViewColumn()
  total_keseluruhan: number;
}
