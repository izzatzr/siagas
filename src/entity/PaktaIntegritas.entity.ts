import { CustomBaseEntity, OmitType } from 'src/types/lib';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import User from './User.entity';
import File from './File.entity';

@Entity({ name: 'pakta_integritas' })
export class PaktaIntegritas extends CustomBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: string;

  @Column()
  upload_id: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => File, {})
  @JoinColumn({ name: 'upload_id' })
  upload: OmitType<File, 'announcements'>;

  @ManyToOne(() => User, {})
  @JoinColumn({ name: 'user_id' })
  user: User;
}
