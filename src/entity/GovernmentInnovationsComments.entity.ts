import { AppBaseEntity, CustomBaseEntity, OmitType } from 'src/types/lib';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import User from './User.entity';
import GovernmentInnovation from './GovernmentInnovation.entity';

@Entity({ name: 'government_innovations_comments' })
export class GovernmentInnovationsComments extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, { eager: true })
  @JoinColumn({ name: 'user_id' })
  @Column()
  user_id: User;

  @OneToMany(
    () => GovernmentInnovation,
    pemdaInovasi => pemdaInovasi.id)
  @Column()
  gov_innov_id: number;

  @Column()
  date: Date;

  @Column({ type: 'json' })
  comment: Object;
}
