import { BaseEntity, BeforeInsert, Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import Hash from '../providers/hash';
import Role from './Role.entity';

@Entity({ name: 'users' })
export default class User extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ name: 'role_id' })
	role_id: number;

	@Column()
	username: string;

	@Column({ select: false })
	password: string;

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;

	@Column()
	email: string;

	@Column()
	full_name: string;

	@Column()
	nickname: string;

	@Column()
	nama_pemda: string;

	@Column()
	created_by: number;

	@ManyToOne(() => User)
	@JoinColumn({ name: 'created_by' })
	createdBy: User;

	@ManyToOne(() => Role, role => role.users, { eager: true })
	@JoinColumn({ name: 'role_id' })
	role: Role;

	@BeforeInsert()
	createPassword() {
		if (this.password) {
			this.password = new Hash().generate(this.password);
		}
	}
}
