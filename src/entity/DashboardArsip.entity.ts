import { Column, Double, ValueTransformer, ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
  expression: `select
	gi.id,
  pp.nama_daerah,
  gi.innovation_name,
  gi.innovation_phase,
  gi.trial_time,
  gi.implementation_time,
  gi.skor_kematangan as skor,
  gi.created_by
from government_innovations gi
left join profil_pemda pp on gi.pemda_id = pp.id`
})
export class DashboardArsip {
  @ViewColumn()
  id: number;

  @ViewColumn()
  pemda_id: number;

  @ViewColumn()
  nama_daerah: string;

  @ViewColumn()
  innovation_name: string;

  @ViewColumn()
  innovation_phase: string;

  @ViewColumn()
  trial_time: string;

  @ViewColumn()
  implementation_time: string;

  @ViewColumn()
  skor: any;

  @ViewColumn()
  created_by: string;
}
