import { AppBaseEntity, CustomBaseEntity, OmitType } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import File from './File.entity';
import User from './User.entity';
import { PemdaIndikator } from './PemdaIndikator.entity';
import GovernmentInnovation from './GovernmentInnovation.entity';

@Entity({ name: 'profil_pemda' })
export class ProfilPemda extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: string;

  @Column()
  nama_daerah: string;

  @Column()
  opd_yang_menangani: string;

  @Column()
  alamat_pemda: string;

  @Column()
  email: string;

  @Column()
  no_telpon: string;

  @Column()
  nama_admin: string;

  @Column()
  document_id: string;

  @Column()
  nama_pemda: string;

  @Column()
  nominator: string;

  @Column()
  nominator_rekap_indeks_akhir: string;

  @ManyToOne(() => File, { eager: true })
  @JoinColumn({ name: 'document_id' })
  document: OmitType<File, 'announcements'>;

  @ManyToOne(() => User, { eager: true })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToMany(() => PemdaIndikator, pemdaIndikator => pemdaIndikator.pemda, {
    eager: true,
    cascade: ['insert']
  })
  indicators: PemdaIndikator[];

  @OneToMany(
    () => GovernmentInnovation,
    pemdaInovasi => pemdaInovasi.profilPemda
  )
  innovations: GovernmentInnovation[];
}
