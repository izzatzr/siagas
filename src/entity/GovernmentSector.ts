import { AfterLoad, BaseEntity, Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import GovernmentInnovation from "./GovernmentInnovation.entity";

@Entity({ name: "government_sectors" })
export default class GovernmentSector extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	name: string;

	@Column()
	deadline: string;

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;

	@OneToMany(() => GovernmentInnovation, innovations => innovations.urusan)
	innovations: GovernmentInnovation[];

	deadline_in_days: number;

	@AfterLoad()
	setDeadline() {
		const now = new Date();
		const deadline = new Date(new Date(this.deadline).getTime() - new Date(this.deadline).getTimezoneOffset() * 60000);

		const diff = deadline.getTime() - now.getTime();
		const dayDiff = Math.round(diff / (1000 * 60 * 60 * 24));
		this.deadline_in_days = dayDiff > 0 ? dayDiff : 0;
		this.deadline = deadline.toISOString().split("T")[0];
	}
}
