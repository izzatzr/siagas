import { BaseEntity, Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import User from './User.entity';

@Entity({ name: 'roles' })
export default class Role extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	name: string;

	@Column({ enum: ['y', 't'] })
	is_creator: string;

	@Column({ enum: ['y', 't'] })
	is_super_admin: string;

	@Column()
	label: string;

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;

	@OneToMany(() => User, users => users.role)
	users: User[];
}
