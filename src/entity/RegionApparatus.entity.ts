import { CustomBaseEntity } from 'src/types/lib';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import Uptd from './Uptd.entity';

@Entity({ name: 'regional_apparatus' })
export default class RegionalApparatus extends CustomBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => Uptd, uptd => uptd.regionalApparatus)
  uptds: Uptd[];
}
