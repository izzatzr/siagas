import { AppBaseEntity } from 'src/types/lib';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PemdaIndikator } from './PemdaIndikator.entity';

@Entity({ name: 'indicators' })
export default class Indicator extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  no_urut: number;

  @Column()
  jenis_indikator: string;

  @Column()
  nama_indikator: string;

  @Column()
  keterangan: string;

  @Column()
  nama_dokumen_pendukung: string;

  @Column()
  bobot: number;

  @Column()
  jenis_file: string;

  @Column()
  parent: string;

  @Column()
  mandatory: string;

  @OneToMany(() => PemdaIndikator, pemdaIndikator => pemdaIndikator.indicator)
  pemdaIndikator: PemdaIndikator[];
}
