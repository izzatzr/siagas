import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { PreviewReviewInovasiDaerah } from './PreviewReviewInovasiDaerah.entity';
import File from './File.entity';

@Entity({ name: 'evaluasi_review_inovasi_daerah' })
export class EvaluasiReviewInovasiDaerah extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  data_saat_ini: string;

  @Column('varchar')
  catatan: string;

  @Column('varchar')
  keterangan: string;

  @Column('bigint')
  preview_review_inovasi_daerah_id: number;

  @Column('bigint')
  document_id: number;

  @ManyToOne(() => PreviewReviewInovasiDaerah)
  @JoinColumn({ name: 'preview_review_inovasi_daerah_id' })
  preview: PreviewReviewInovasiDaerah;

  @ManyToOne(() => File)
  @JoinColumn({ name: 'document_id' })
  document: File;
}
