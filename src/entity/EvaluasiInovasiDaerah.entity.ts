import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import File from './File.entity';
import { InovasiIndikator } from './InovasiIndikator.entity';
import DocumentCategory from './DocumentCategory.entity';

@Entity({ name: 'evaluasi_inovasi_daerah' })
export class EvaluasiInovasiDaerah extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  data_saat_ini: string;

  @Column('varchar')
  catatan: string;

  @Column('varchar')
  keterangan: string;

  @Column('varchar')
  tentang: string;

  @Column('varchar')
  kategori: string;

  @Column('bigint')
  inovasi_indikator_id: number;

  @ManyToOne(() => InovasiIndikator)
  @JoinColumn({ name: 'inovasi_indikator_id' })
  inovasi_indikator: InovasiIndikator;
}
