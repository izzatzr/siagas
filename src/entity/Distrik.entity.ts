import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Distrik extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column("varchar")
	nama_distrik: string;

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;
}
