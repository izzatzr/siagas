import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import Area from './Area.entity';
import ClusterDetail from './ClusterDetail.entity';

@Entity({ name: 'regions' })
export default class Regions extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => Area, area => area.region)
  areas: Area[];

  @OneToMany(() => ClusterDetail, detail => detail.region)
  clusters: ClusterDetail[];
}
