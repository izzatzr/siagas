import {
  BaseEntity,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn
} from 'typeorm';
import Cluster from './Cluster.entity';
import Regions from './Region.entity';

@Entity({ name: 'cluster_details' })
export default class ClusterDetail extends BaseEntity {
  @PrimaryColumn()
  cluster_id: number;

  @PrimaryColumn()
  region_id: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Cluster, cluster => cluster.details)
  @JoinColumn({ name: 'cluster_id' })
  cluster: Cluster;

  @ManyToOne(() => Regions, region => region.clusters)
  @JoinColumn({ name: 'region_id' })
  region: Regions;
}
