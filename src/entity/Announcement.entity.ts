import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import File from './File.entity';

@Entity({ name: 'announcements', orderBy: { created_at: 'DESC' } })
export default class Announcement extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  slug: string;

  @Column()
  content: string;

  @Column({ nullable: true })
  file_id: number | null;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => File, file => file.announcements, { eager: true })
  @JoinColumn({ name: 'file_id' })
  file: File;
}
