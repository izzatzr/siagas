import { AppBaseEntity } from 'src/types/lib';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import GovernmentInnovation from './GovernmentInnovation.entity';
import { PreviewReviewInovasiDaerah } from './PreviewReviewInovasiDaerah.entity';

@Entity({ name: 'review_inovasi_daerah' })
export class ReviewInovasiDaerah extends AppBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('bigint')
  random_number: number;

  @Column('float')
  skor: number;

  @Column('varchar')
  status?: string;

  @Column('bigint')
  inovasi_id?: number;

  @ManyToOne(() => GovernmentInnovation)
  @JoinColumn({ name: 'inovasi_id' })
  inovasi: GovernmentInnovation;

  @OneToMany(
    () => PreviewReviewInovasiDaerah,
    preview => preview.review_inovasi_daerah
  )
  previews: PreviewReviewInovasiDaerah[];
}
