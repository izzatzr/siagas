import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { unlink } from 'fs';
import { join } from 'path';
import { ParsedQs } from 'qs';
import GovernmentProfile from '../entity/GovernmentProfile.entity';
import ApiError from '../providers/exceptions/api.error';
import response from '../providers/response';
import { PaginationData } from '../schema/base.schema';
import {
  CreateGovernmentProfileRequest,
  GetGovernmentProfileRequest
} from '../schema/government_profile.schema';
import GovernmentProfileService from '../service/government_profile.service';
import { HttpCode } from '../types/http_code.enum';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';

class GovernmentProfileController
  extends BaseController<GovernmentProfileService>
  implements IBaseController
{
  constructor() {
    super(new GovernmentProfileService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateGovernmentProfileRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const file = req.file;
    try {
      const {
        alamat,
        daerah,
        email,
        nama_admin,
        name,
        nomor_telepon,
        opd_menangani
      } = req.body;
      const result = await this.service.create(
        daerah,
        name,
        opd_menangani,
        alamat,
        email,
        nomor_telepon,
        nama_admin,
        file,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      if (file) {
        unlink(
          join(
            __dirname,
            '..',
            '..',
            'public',
            'upload',
            'profil_pemda',
            file.filename
          ),
          err => {
            if (err) {
              console.log(`${file.filename} was deleted`);
            }
          }
        );
      }
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetGovernmentProfileRequest,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(req.query, url)) as PaginationData<
        GovernmentProfile[]
      >;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.detail(req.params.id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateGovernmentProfileRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const file = req.file;
    try {
      const {
        alamat,
        daerah,
        email,
        nama_admin,
        name,
        nomor_telepon,
        opd_menangani
      } = req.body;
      const result = await this.service.update(
        req.params.id,
        daerah,
        name,
        opd_menangani,
        alamat,
        email,
        nomor_telepon,
        nama_admin,
        file,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.delete(req.params.id);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      } else {
        response.success('Berhasil hapus data').create(res);
      }
    } catch (error) {
      next(error);
    }
  };
}

export default GovernmentProfileController;
