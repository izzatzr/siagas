import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import AnnouncementService from '../service/announcement.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import {
  CreateAnnouncementRequest,
  GetAnnouncementRequest
} from '../schema/announcement.schema';
import Announcement from '../entity/Announcement.entity';
import response from '../providers/response';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import { unlink } from 'fs';
import { join } from 'path';

class AnnouncementController
  extends BaseController<AnnouncementService>
  implements IBaseController
{
  constructor() {
    super(new AnnouncementService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateAnnouncementRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<Announcement>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const document = req.file;
    try {
      const result = await this.service.create(
        req.body.content,
        req.body.title,
        req.body.slug,
        document,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      unlink(
        join(
          __dirname,
          '..',
          '..',
          'public',
          'upload',
          'pengumuman',
          (document as Express.Multer.File).filename
        ),
        err => {
          if (err) {
            console.log(
              `${(document as Express.Multer.File).filename} was deleted`
            );
          }
        }
      );
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetAnnouncementRequest,
      Record<string, any>
    >,
    res: Response<
      PaginationResponse<PaginationResponseInterface, Announcement>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const host = req.protocol + '://' + req.get('host');
      const url = host + req.originalUrl;
      const result = (await this.service.get(
        url,
        host,
        req.query
      )) as PaginationData<Announcement[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const host = req.protocol + '://' + req.get('host');
      const result = await this.service.detail(req.params.id, host);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateAnnouncementRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const document = req.file;
    try {
      const result = await this.service.update(
        req.params.id,
        req.body.content,
        req.body.title,
        req.body.slug,
        document,
        res.locals.user.username
      );
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal ubah data'
          })
        );
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      if (document) {
        unlink(
          join(
            __dirname,
            '..',
            '..',
            'public',
            'upload',
            'pengumuman',
            (document as Express.Multer.File).filename
          ),
          err => {
            if (err) {
              console.log(
                `${(document as Express.Multer.File).filename} was deleted`
              );
            }
          }
        );
      }
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default AnnouncementController;
