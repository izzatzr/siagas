import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { createWriteStream, unlink, unlinkSync } from 'fs';
import { join } from 'path';
import { ParsedQs } from 'qs';
import response from '../providers/response';
import { CreateExportRequest } from '../schema/export.schema';
import ExportService from '../service/export.service';
import { HttpCode } from '../types/http_code.enum';

class ExportController {
  export = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateExportRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const stream = createWriteStream(
        join(
          __dirname,
          '..',
          '..',
          'public',
          'tmp',
          `${req.body.name}.${req.body.format}`
        )
      );

      const service = new ExportService();
      await service.export(req.body, stream);

      stream.on('open', () => {
        console.log('Opened');
        stream.pipe(res);
      });
      stream.on('close', () => {
        unlinkSync(
          join(
            __dirname,
            '..',
            '..',
            'public',
            'tmp',
            `${req.body.name}.${req.body.format}`
          )
        );
        console.log('Closed');
      });
      res.download(
        join(__dirname, '..', '..', 'public', 'tmp', `${req.body.name}.xlsx`)
      );
    } catch (error) {
      next(error);
    }
  };
}

export default ExportController;
