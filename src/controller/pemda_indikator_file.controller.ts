import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import { InovasiIndikatorFile } from 'src/entity/InovasiIndikatorFile.entity';
import ApiError from 'src/providers/exceptions/api.error';
import response from 'src/providers/response';
import {
  ListPemdaIndikatorFiles,
  PemdaIndikatorFileParams,
  UploadPemdaIndikatorFileRequest
} from 'src/schema/pemda_indikator_file.schema';
import PemdaIndikatorFileService from 'src/service/pemda_indikator_file.service';
import { HttpCode } from 'src/types/http_code.enum';
import { PaginationResponse, SuccessResponse } from 'src/types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationData } from 'src/schema/base.schema';

export class PemdaIndikatorFileController
  extends BaseController<PemdaIndikatorFileService>
  implements IBaseController
{
  constructor() {
    super(new PemdaIndikatorFileService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      UploadPemdaIndikatorFileRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    throw new Error('Method not implemented');
  };

  upload = async (
    req: Request<
      ParamsDictionary & { pemda_id: string; indikator_id: string },
      any,
      UploadPemdaIndikatorFileRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<InovasiIndikatorFile>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const document = req.file;
    if (!document)
      throw new ApiError({
        httpCode: HttpCode.BAD_REQUEST,
        message: 'Invalid request'
      });
    try {
      const result = await this.service.upload(
        req.params.pemda_id,
        req.params.indikator_id,
        req.body.nomor_dokumen,
        req.body.tanggal_dokumen,
        req.body.tentang,
        document,
        req.body.name,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary & { pemda_id: string; indikator_id: string },
      any,
      any,
      ParsedQs & PemdaIndikatorFileParams,
      Record<string, any>
    >,
    res: Response<
      PaginationResponse<PaginationResponseInterface, Document>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const host = req.protocol + '://' + req.get('host');
      const url = host + req.originalUrl;
      const result = (await this.service.get(
        req.params.pemda_id,
        req.params.indikator_id,
        url,
        res.locals.user.username,
        req.query
      )) as PaginationData<ListPemdaIndikatorFiles>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & {
        pemda_id: number;
        indikator_id: number;
        file_id: number;
      },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.detail(
        req.params.pemda_id,
        req.params.indikator_id,
        req.params.file_id
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    throw new Error('Method not implemented');
  };

  delete = async (
    req: Request<
      ParamsDictionary & {
        pemda_id: number;
        indikator_id: number;
        file_id: number;
      },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { pemda_id, indikator_id, file_id } = req.params;

      const result = await this.service.delete(pemda_id, indikator_id, file_id);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}
