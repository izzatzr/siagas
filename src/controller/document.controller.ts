import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import response from '../providers/response';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import { unlink } from 'fs';
import { join } from 'path';
import DocumentService from '../service/document.service';
import {
  CreateDocumentRequest,
  GetDocumentRequest
} from '../schema/document.schema';
import Document from '../entity/Document.entity';

class DocumentController
  extends BaseController<DocumentService>
  implements IBaseController
{
  constructor() {
    super(new DocumentService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateDocumentRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<Document>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const document = req.file;
    try {
      const result = await this.service.create(
        req.body.content,
        req.body.title,
        req.body.category_id,
        document,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetDocumentRequest,
      Record<string, any>
    >,
    res: Response<
      PaginationResponse<PaginationResponseInterface, Document>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const host = req.protocol + '://' + req.get('host');
      const url = host + req.originalUrl;
      const result = (await this.service.get(
        url,
        host,
        req.query
      )) as PaginationData<Document[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const host = req.protocol + '://' + req.get('host');
      const result = await this.service.detail(req.params.id, host);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateDocumentRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const document = req.file;
    try {
      const result = await this.service.update(
        req.params.id,
        req.body.content,
        req.body.title,
        req.body.category_id,
        document,
        res.locals.user.username
      );
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal ubah data'
          })
        );
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      unlink(
        join(
          __dirname,
          '..',
          '..',
          'public',
          'upload',
          'dokumen',
          (document as Express.Multer.File).filename
        ),
        err => {
          if (err) {
            console.log(
              `${(document as Express.Multer.File).filename} was deleted`
            );
          }
        }
      );
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default DocumentController;
