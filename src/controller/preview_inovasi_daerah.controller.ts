import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import BaseController from './base.controller';
import PreviewReviewInovasiDaerahService from 'src/service/preview_inovasi_daerah.service';
import { IBaseController } from './interface/ibase.controller';
import {
  CreatePreviewInovasiDaerahRequest,
  GetPreviewInovasiRequest
} from 'src/schema/preview_inovasi_daerah.schema';
import { SuccessResponse } from 'src/types/response.type';
import { PreviewReviewInovasiDaerah } from 'src/entity/PreviewReviewInovasiDaerah.entity';
import response from 'src/providers/response';
import { PaginationData } from 'src/schema/base.schema';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';

export class PreviewReviewInovasiDaerahController
  extends BaseController<PreviewReviewInovasiDaerahService>
  implements IBaseController
{
  constructor() {
    super(new PreviewReviewInovasiDaerahService());
  }

  create = async (
    req: Request<
      ParamsDictionary & {
        review_id: number;
      },
      any,
      CreatePreviewInovasiDaerahRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<
      SuccessResponse<PreviewReviewInovasiDaerah>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const result = await this.service.create(
        req.params.review_id,
        username,
        req.body
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary & {
        review_id: number;
      },
      any,
      any,
      ParsedQs & GetPreviewInovasiRequest,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(
        req.params.review_id,
        url,
        username,
        req.query
      )) as PaginationData<PreviewReviewInovasiDaerah[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number; review_id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(req.params.review_id, id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number; review_id: number },
      any,
      CreatePreviewInovasiDaerahRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const { username } = res.locals.user;
      const result = await this.service.update(
        req.params.review_id,
        id,
        username,
        req.body
      );
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number; review_id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(req.params.review_id, id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}
