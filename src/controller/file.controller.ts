import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { UploadFileRequest } from '../schema/file.schema';
import { ParsedQs } from 'qs';
import FileService from '../service/file.service';
import BaseController from './base.controller';
import { SuccessResponse } from '../types/response.type';
import File from '../entity/File.entity';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import response from '../providers/response';

class FileController extends BaseController<FileService> {
  constructor() {
    super(new FileService());
  }

  upload = async (
    req: Request<
      ParamsDictionary,
      any,
      UploadFileRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<File>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const body = req.body;
      const file = req.file;
      if (!file) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'File invalid'
          })
        );
        return;
      }
      body.file = file;
      const result = await this.service.create(
        file,
        req.body.name,
        res.locals.user.username
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default FileController;
