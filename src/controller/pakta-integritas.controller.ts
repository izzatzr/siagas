import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import PaktaIntegritasService from 'src/service/pakta-integritas.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import {
  CreatePaktaIntegritasRequest,
  ListPaktaIntegritas,
  PaktaIntegritasParams
} from 'src/schema/pakta-integritas.schema';
import { PaktaIntegritas } from 'src/entity/PaktaIntegritas.entity';
import { SuccessResponse } from 'src/types/response.type';
import response from 'src/providers/response';
import { unlink } from 'fs';
import { join } from 'path';
import { PaginationData } from 'src/schema/base.schema';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';

class PaktaIntegritasController
  extends BaseController<PaktaIntegritasService>
  implements IBaseController
{
  constructor() {
    super(new PaktaIntegritasService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreatePaktaIntegritasRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<PaktaIntegritas>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const file = req.file;
    try {
      const { username } = res.locals.user;
      const { user_id } = req.body;
      const result = await this.service.create(user_id, file, username);
      response.success(result).create(res);
    } catch (error) {
      if (file) {
        unlink(
          join(
            __dirname,
            '..',
            '..',
            'public',
            'upload',
            'pakta_integritas',
            file.filename
          ),
          err => {
            if (err) {
              console.log(`${file.filename} was deleted`);
            }
          }
        );
      }
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & PaktaIntegritasParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(
        url,
        username,
        req.query
      )) as PaginationData<ListPaktaIntegritas>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  getLatest = async (
    req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const result = await this.service.getLatest(username);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreatePaktaIntegritasRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    const file = req.file;
    try {
      const { id } = req.params;
      const { username } = res.locals.user;
      const { user_id } = req.body;
      const result = await this.service.update(id, user_id, file, username);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      if (file) {
        unlink(
          join(
            __dirname,
            '..',
            '..',
            'public',
            'upload',
            'pakta_integritas',
            file.filename
          ),
          err => {
            if (err) {
              console.log(`${file.filename} was deleted`);
            }
          }
        );
      }
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default PaktaIntegritasController;
