import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import response from '../providers/response';
import { PaginationResponse, SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import { PaginationData } from '../schema/base.schema';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import DocumentCategoryService from '../service/document_category.service';
import {
  CreateDocumentCategoryRequest,
  GetDocumentCategoryRequest
} from '../schema/document_category.schema';
import DocumentCategory from '../entity/DocumentCategory.entity';

class DocumentCategoryController
  extends BaseController<DocumentCategoryService>
  implements IBaseController
{
  constructor() {
    super(new DocumentCategoryService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateDocumentCategoryRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<DocumentCategory>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.create(req.body);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetDocumentCategoryRequest,
      Record<string, any>
    >,
    res: Response<
      PaginationResponse<PaginationResponseInterface, DocumentCategory>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(url, req.query)) as PaginationData<
        DocumentCategory[]
      >;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.detail(req.params.id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateDocumentCategoryRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.update(req.params.id, req.body);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal ubah data'
          })
        );
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default DocumentCategoryController;
