import { NextFunction, Request, Response } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { Distrik } from "src/entity/Distrik.entity";
import { CreateDistrikRequest, GetDistrikRequest } from "src/schema/distrik.schema";
import DistrikService from "src/service/distrik.service";
import { PaginationResponse } from "../interface/pagination.interface";
import ApiError from "../providers/exceptions/api.error";
import response from "../providers/response";
import { HttpCode } from "../types/http_code.enum";
import { SuccessResponse } from "../types/response.type";
import BaseController from "./base.controller";
import { IBaseController } from "./interface/ibase.controller";

class DistrikController extends BaseController<DistrikService> implements IBaseController {
	constructor() {
		const service = new DistrikService();
		super(service);
	}

	create = async (
		req: Request<ParamsDictionary, SuccessResponse<Distrik>, CreateDistrikRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.create(req.body);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};
	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & GetDistrikRequest, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.get(url, req.query)) as {
				paging: PaginationResponse;
				res: Distrik[];
			};
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};
	detail = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.detail(id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};
	update = async (
		req: Request<ParamsDictionary & { id: number }, any, CreateDistrikRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;
			const request = req.body;

			const result = await this.service.update(id, request);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: "Gagal ubah data",
				});
			}
			response.success("Berhasil ubah data").create(res);
		} catch (error) {
			next(error);
		}
	};
	delete = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.delete(id);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: "Gagal hapus data",
				});
			}
			response.success("Berhasil hapus data").create(res);
		} catch (error) {
			next(error);
		}
	};
}

export default DistrikController;
