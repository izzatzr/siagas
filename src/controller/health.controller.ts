import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import response from '../providers/response';
import HealthService from '../service/health.service';
import BaseController from './base.controller';
import { IHealthController } from './interface/ihealth.controller';

class HealthController extends BaseController<HealthService> implements IHealthController {
	constructor(service: HealthService = new HealthService()) {
		super(service);
	}

	async health(
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> {
		try {
			response.success('Server is healthy').create(res);
		} catch (error) {
			next(error);
		}
	}

	async template(
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> {
		try {
			return res.render('main');
		} catch (error) {
			next(error);
		}
	}
}

export default HealthController;
