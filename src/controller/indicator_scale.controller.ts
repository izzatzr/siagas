import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import IndicatorScale from '../entity/IndicatorScale.entity';
import ApiError from '../providers/exceptions/api.error';
import response from '../providers/response';
import { PaginationData } from '../schema/base.schema';
import {
  CreateIndicatorScaleRequest,
  GetIndicatorScaleRequest
} from '../schema/indicator_scale.scheme';
import IndicatorScaleService from '../service/indicator_scale.service';
import { HttpCode } from '../types/http_code.enum';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';

class IndicatorScaleController
  extends BaseController<IndicatorScaleService>
  implements IBaseController
{
  constructor() {
    const service = new IndicatorScaleService();
    super(service);
  }
  create = async (
    req: Request<
      ParamsDictionary & { indikator_id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.create(
        req.params.indikator_id,
        req.body
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
  get = async (
    req: Request<
      ParamsDictionary & { indikator_id: number },
      any,
      any,
      ParsedQs & GetIndicatorScaleRequest,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(
        req.params.indikator_id,
        url,
        req.query
      )) as PaginationData<IndicatorScale[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };
  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateIndicatorScaleRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const request = req.body;

      const result = await this.service.update(id, request);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };
  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default IndicatorScaleController;
