import { NextFunction, Request, Response } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { unlinkSync } from "fs";
import { ParsedQs } from "qs";
import { LaporanBentukInovasi } from "src/entity/LaporanBentukInovasi.entity";
import { LaporanInisiatorInovasi } from "src/entity/LaporanInisatorInovasi.entity";
import { LaporanJenisInovasi } from "src/entity/LaporanJenisInovasi.entity";
import { LaporanUrusanInovasi } from "src/entity/LaporanUrusanInovasi.entity";
import response from "src/providers/response";
import { PaginationData } from "src/schema/base.schema";
import {
	LaporanBentukInovasiParams,
	LaporanIndeksParams,
	LaporanIndeksResponse,
	LaporanInisiatorInovasiParams,
	LaporanJenisInovasiParams,
	LaporanUrusanInovasiParams,
} from "src/schema/laporan.schema";
import LaporanService from "src/service/laporan.service";
import { DownloadType } from "src/types/lib";
import BaseController from "./base.controller";

export class LaporanController extends BaseController<LaporanService> {
	constructor() {
		super(new LaporanService());
	}

	laporanUrusan = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & LaporanUrusanInovasiParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.laporanUrusanInovasi(url, req.query)) as PaginationData<any[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	laporanInisiator = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & LaporanInisiatorInovasiParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.laporanInisiatorInovasi(url, req.query)) as PaginationData<any[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	laporanBentuk = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & LaporanBentukInovasiParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.laporanBentukInovasi(url, req.query)) as PaginationData<any[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	laporanIndeks = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & LaporanIndeksParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.laporanIndeks(url, req.query)) as PaginationData<LaporanIndeksResponse[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	laporanIndeksDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & LaporanIndeksParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.laporanIndeksdownload(req.params.type, req.query);
			res.download(result, err => {
				if (!err) unlinkSync(result);
				else console.log(err);
			});
		} catch (error) {
			next(error);
		}
	};

	laporanUrusanInovasiDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & LaporanUrusanInovasiParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.laporanUrusanInovasidownload(req.params.type, req.query);
			res.download(result, err => {
				if (!err) unlinkSync(result);
				else console.log(err);
			});
		} catch (error) {
			next(error);
		}
	};

	laporanInisiatorInovasiDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & LaporanInisiatorInovasiParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const result = await this.service.laporanInisiatorInovasidownload(req.params.type, req.query);
			res.download(result, err => {
				if (!err) unlinkSync(result);
				else console.log(err);
			});
		} catch (error) {
			next(error);
		}
	};

	laporanJenisInovasi = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & LaporanJenisInovasiParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const url = req.protocol + "://" + req.get("host") + req.originalUrl;
			const result = (await this.service.laporanJenisInovasi(url, req.query)) as PaginationData<any[]>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};
}
