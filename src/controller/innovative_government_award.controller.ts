import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { unlinkSync } from 'fs';
import { ParsedQs } from 'qs';
import ApiError from 'src/providers/exceptions/api.error';
import response from 'src/providers/response';
import { PaginationData } from 'src/schema/base.schema';
import {
  PeringkatHasilReviewNominatorRequest,
  PeringkatHasilReviewParams,
  PeringkatHasilReviewResponse,
  PrestasiResponse,
  RangkingResponse
} from 'src/schema/innovative_government_award.schema';
import InnovativeGovernmentAwardService from 'src/service/innovative_government_award.service';
import { HttpCode } from 'src/types/http_code.enum';
import { DownloadType } from 'src/types/lib';
import BaseController from './base.controller';

export class InnovativeGovernmentAwardController extends BaseController<InnovativeGovernmentAwardService> {
  constructor() {
    super(new InnovativeGovernmentAwardService());
  }

  peringkat_hasil_review = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & PeringkatHasilReviewParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.peringkat_hasil_review(
        url,
        username,
        req.query
      )) as PaginationData<PeringkatHasilReviewResponse[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  peringkat_hasil_review_download = async (
    req: Request<
      ParamsDictionary & { type: DownloadType },
      any,
      any,
      ParsedQs & PeringkatHasilReviewParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const result = await this.service.peringkat_hasil_review_download(
        req.params.type,
        username,
        req.query
      );
      res.download(result, err => {
        if (!err) unlinkSync(result);
      });
    } catch (error) {
      next(error);
    }
  };

  prestasi_download = async (
    req: Request<
      ParamsDictionary & { type: DownloadType },
      any,
      any,
      ParsedQs & PeringkatHasilReviewParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const result = await this.service.prestasi_download(
        req.params.type,
        username,
        req.query
      );
      res.download(result, err => {
        if (!err) unlinkSync(result);
      });
    } catch (error) {
      next(error);
    }
  };

  rangking_download = async (
    req: Request<
      ParamsDictionary & { type: DownloadType },
      any,
      any,
      ParsedQs & PeringkatHasilReviewParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const result = await this.service.rangking_download(
        req.params.type,
        username,
        req.query
      );
      res.download(result, err => {
        if (!err) unlinkSync(result);
      });
    } catch (error) {
      next(error);
    }
  };

  nominator_peringkat_hasil_review = async (
    req: Request<
      ParamsDictionary & { pemda_id: number },
      PeringkatHasilReviewNominatorRequest,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ) => {
    try {
      const { pemda_id } = req.params;

      const result = await this.service.nominator_peringkat_hasil_review(
        pemda_id,
        req.body
      );
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  prestasi = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & PeringkatHasilReviewParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.prestasi(
        url,
        username,
        req.query
      )) as PaginationData<PrestasiResponse[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  rangking = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & PeringkatHasilReviewParams,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { username } = res.locals.user;
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.rangking(
        url,
        username,
        req.query
      )) as PaginationData<RangkingResponse[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };
}
