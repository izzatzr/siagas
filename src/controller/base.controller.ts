export default class BaseController<S> {
  public service: S;

  constructor(service: S) {
    this.service = service;
  }
}
