import { Request, Response, NextFunction } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import {
  CreateClusterRequest,
  GetClusterRequest
} from '../schema/cluster.schema';
import ClusterService from '../service/cluster.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { SuccessResponse, PaginationResponse } from '../types/response.type';
import { PaginationResponse as PaginationResponseInterface } from '../interface/pagination.interface';
import Cluster from '../entity/Cluster.entity';
import response from '../providers/response';
import { PaginationData } from '../schema/base.schema';
import ApiError from '../providers/exceptions/api.error';
import { HttpCode } from '../types/http_code.enum';
import {
  CreateClusterDetailRequest,
  GetClusterDetailRequest
} from '../schema/clusterDetail.schema';
import ClusterDetail from '../entity/ClusterDetail.entity';

class ClusterController
  extends BaseController<ClusterService>
  implements IBaseController
{
  constructor() {
    super(new ClusterService());
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateClusterRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<Cluster>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.create(req.body);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetClusterRequest,
      Record<string, any>
    >,
    res: Response<
      PaginationResponse<PaginationResponseInterface, Cluster>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(url, req.query)) as PaginationData<
        Cluster[]
      >;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.detail(req.params.id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateClusterRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.update(req.params.id, req.body);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal ubah data'
          })
        );
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };

  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<string>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };

  /**
   *  Detail Section
   */
  createDetail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateClusterDetailRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<ClusterDetail>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.createDetail(req.params.id, req.body);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  getDetails = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs & GetClusterDetailRequest,
      Record<string, any>
    >,
    res: Response<
      PaginationResponse<PaginationResponseInterface, ClusterDetail[]>,
      Record<string, any>
    >,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.getDetails(
        req.params.id,
        url,
        req.body
      )) as PaginationData<ClusterDetail[]>;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };

  getDetail = async (
    req: Request<
      ParamsDictionary & { id: number; region_id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<ClusterDetail>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.getDetail(
        req.params.id,
        req.params.region_id
      );
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };

  updateDetail = async (
    req: Request<
      ParamsDictionary & { id: number; region_id: number },
      any,
      CreateClusterDetailRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<ClusterDetail>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.updateDetail(
        req.params.id,
        req.params.region_id,
        req.body
      );
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal update data'
          })
        );
      }
      response.success('Success update data').create(res);
    } catch (error) {
      next(error);
    }
  };

  deleteDetail = async (
    req: Request<
      ParamsDictionary & { id: number; region_id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.deleteDetail(
        req.params.id,
        req.params.region_id
      );
      if (!result) {
        next(
          new ApiError({
            httpCode: HttpCode.BAD_REQUEST,
            message: 'Gagal hapus data'
          })
        );
      }
      response.success('Success hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default ClusterController;
