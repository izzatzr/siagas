import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import ProfilPemdaService from 'src/service/profil_pemda.service';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';
import { CreateProfilPemdaRequest, ListProfilPemda, ProfilPemdaFilterDownload, ProfilPemdaParams } from 'src/schema/profil_pemda.schema';
import { SuccessResponse } from 'src/types/response.type';
import { ProfilPemda } from 'src/entity/ProfilePemda.entity';
import response from 'src/providers/response';
import { unlink, unlinkSync } from 'fs';
import { join } from 'path';
import { PaginationData } from 'src/schema/base.schema';
import ApiError from 'src/providers/exceptions/api.error';
import { HttpCode } from 'src/types/http_code.enum';
import { DownloadType } from 'src/types/lib';

class ProfilPemdaController extends BaseController<ProfilPemdaService> implements IBaseController {
	constructor() {
		super(new ProfilPemdaService());
	}

	create = async (
		req: Request<ParamsDictionary, any, CreateProfilPemdaRequest, ParsedQs, Record<string, any>>,
		res: Response<SuccessResponse<ProfilPemda>, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		const file = req.file;
		try {
			const { username } = res.locals.user;
			const { nama_pemda, alamat_pemda, email, nama_admin, nama_daerah, no_telpon, opd_yang_menangani } = req.body;
			const result = await this.service.create(
				nama_pemda,
				nama_daerah,
				opd_yang_menangani,
				alamat_pemda,
				email,
				no_telpon,
				nama_admin,
				file,
				username
			);
			response.success(result).create(res);
		} catch (error) {
			if (file) {
				unlink(join(__dirname, '..', '..', 'public', 'upload', 'profil_pemda', file.filename), err => {
					if (err) {
						console.log(`${file.filename} was deleted`);
					}
				});
			}
			next(error);
		}
	};

	getDownload = async (
		req: Request<ParamsDictionary & { type: DownloadType }, any, any, ParsedQs & ProfilPemdaFilterDownload, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.download(req.params.type, username, req.query);
			res.download(result, err => !err && unlinkSync(result));
		} catch (error) {
			next(error);
		}
	};

	downloadDetail = async (
		req: Request<ParamsDictionary & { type: DownloadType; id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const result = await this.service.downloadProfil(req.params.id, req.params.type);
			res.download(result, err => {
				if (!err) unlinkSync(result);
			});
		} catch (error) {
			next(error);
		}
	};

	get = async (
		req: Request<ParamsDictionary, any, any, ParsedQs & ProfilPemdaParams, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { username } = res.locals.user;
			const url = req.protocol + '://' + req.get('host') + req.originalUrl;
			const result = (await this.service.get(url, username, req.query)) as PaginationData<ListProfilPemda>;
			response.pagination(result.res, result.paging).create(res);
		} catch (error) {
			next(error);
		}
	};

	detail = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.detail(id);
			response.success(result).create(res);
		} catch (error) {
			next(error);
		}
	};

	update = async (
		req: Request<ParamsDictionary & { id: number }, any, CreateProfilPemdaRequest, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		const file = req.file;
		try {
			const { id } = req.params;
			const { username } = res.locals.user;
			const { nama_pemda, alamat_pemda, email, nama_admin, nama_daerah, no_telpon, opd_yang_menangani } = req.body;
			const result = await this.service.update(
				id,
				nama_pemda,
				nama_daerah,
				opd_yang_menangani,
				alamat_pemda,
				email,
				no_telpon,
				nama_admin,
				file,
				username
			);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: 'Gagal ubah data',
				});
			}
			response.success('Berhasil ubah data').create(res);
		} catch (error) {
			if (file) {
				unlink(join(__dirname, '..', '..', 'public', 'upload', 'profil_pemda', file.filename), err => {
					if (err) {
						console.log(`${file.filename} was deleted`);
					}
				});
			}
			next(error);
		}
	};

	delete = async (
		req: Request<ParamsDictionary & { id: number }, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction
	): Promise<void> => {
		try {
			const { id } = req.params;

			const result = await this.service.delete(id);
			if (!result) {
				throw new ApiError({
					httpCode: HttpCode.BAD_REQUEST,
					message: 'Gagal hapus data',
				});
			}
			response.success('Berhasil hapus data').create(res);
		} catch (error) {
			next(error);
		}
	};
}

export default ProfilPemdaController;
