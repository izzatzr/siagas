import { NextFunction, Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { ParsedQs } from 'qs';
import Indicator from '../entity/Indicator.entity';
import ApiError from '../providers/exceptions/api.error';
import response from '../providers/response';
import { PaginationData } from '../schema/base.schema';
import { CreateIndicatorRequest } from '../schema/indicator.schema';
import { GetRegionRequest } from '../schema/region.schema';
import IndicatorService from '../service/indicator.service';
import { HttpCode } from '../types/http_code.enum';
import { SuccessResponse } from '../types/response.type';
import BaseController from './base.controller';
import { IBaseController } from './interface/ibase.controller';

class IndicatorController
  extends BaseController<IndicatorService>
  implements IBaseController
{
  constructor() {
    const service = new IndicatorService();
    super(service);
  }

  create = async (
    req: Request<
      ParamsDictionary,
      any,
      CreateIndicatorRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<SuccessResponse<Indicator>, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const result = await this.service.create(req.body);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
  get = async (
    req: Request<
      ParamsDictionary,
      any,
      any,
      ParsedQs & GetRegionRequest,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const url = req.protocol + '://' + req.get('host') + req.originalUrl;
      const result = (await this.service.get(url, req.query)) as PaginationData<
        Indicator[]
      >;
      response.pagination(result.res, result.paging).create(res);
    } catch (error) {
      next(error);
    }
  };
  detail = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.detail(id);
      response.success(result).create(res);
    } catch (error) {
      next(error);
    }
  };
  update = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      CreateIndicatorRequest,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const request = req.body;

      const result = await this.service.update(id, request);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal ubah data'
        });
      }
      response.success('Berhasil ubah data').create(res);
    } catch (error) {
      next(error);
    }
  };
  delete = async (
    req: Request<
      ParamsDictionary & { id: number },
      any,
      any,
      ParsedQs,
      Record<string, any>
    >,
    res: Response<any, Record<string, any>>,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { id } = req.params;

      const result = await this.service.delete(id);
      if (!result) {
        throw new ApiError({
          httpCode: HttpCode.BAD_REQUEST,
          message: 'Gagal hapus data'
        });
      }
      response.success('Berhasil hapus data').create(res);
    } catch (error) {
      next(error);
    }
  };
}

export default IndicatorController;
