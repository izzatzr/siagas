import User from 'src/entity/User.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  JoinColumn,
  ManyToOne,
  UpdateDateColumn
} from 'typeorm';

export type RequiredField<T, K extends keyof T> = T & Required<Pick<T, K>>;

export type OmitType<T, K extends keyof T> = Omit<T, K>;

export type Nullable<T> = { [K in keyof T]: T[K] | null };

export class CustomBaseEntity extends BaseEntity {
  @Column()
  created_by: string;

  @Column()
  updated_by: string;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'created_by', referencedColumnName: 'username' })
  userCreator: User;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'updated_by', referencedColumnName: 'username' })
  userUpdater: User;
}

export class AppBaseEntity extends CustomBaseEntity {
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export type DownloadType = 'xlsx' | 'pdf';
