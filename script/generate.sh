#!/bin/sh
option="${1}"
case "${option}" in
    -c)
        FILENAME=$2
        if [ ! -f "./src/controllers/${FILENAME}.controller.ts" ]
        then
            touch ./src/controllers/${FILENAME}.controller.ts
        fi
    ;;
    -s)
        FILENAME=$2
        if [ ! -f "./src/services/${FILENAME}.service.ts" ]
        then
            touch ./src/services/${FILENAME}.service.ts
        fi
    ;;
    -sc)
        FILENAME=$2
        if [ ! -f "./src/schema/${FILENAME}.schema.ts" ]
        then
            touch ./src/schema/${FILENAME}.schema.ts
        fi
    ;;
    -r)
        FILENAME=$2
        if [ ! -f "./src/routes/${FILENAME}.route.ts" ]
        then
            touch ./src/routes/${FILENAME}.route.ts
        fi
    ;;
    -m)
        FILENAME=$2
        npm run migration:create ./src/migrations/${FILENAME}
    ;;
    -rp)
        FILENAME=$2
        if [ ! -f "./src/repository/${FILENAME}.repository.ts" ]
        then
            touch ./src/repository/${FILENAME}.repository.ts
        fi
    ;;
    -e)
        FILENAME=$2
        if [ ! -f "./src/entity/${FILENAME}.entity.ts" ]
        then
            npm run entity:create ./src/entity/${FILENAME}.entity
        fi
    ;;
    -res)
        FILENAME=$2
        if [ ! -f "./src/repository/${FILENAME}.repository.ts" ]
        then
            touch ./src/repository/${FILENAME}.repository.ts
        fi
        if [ ! -f "./src/routes/${FILENAME}.routes.ts" ]
        then
            touch ./src/routes/${FILENAME}.routes.ts
        fi
        if [ ! -f "./src/schema/${FILENAME}.schema.ts" ]
        then
            touch ./src/schema/${FILENAME}.schema.ts
        fi
        if [ ! -f "./src/service/${FILENAME}.service.ts" ]
        then
            touch ./src/service/${FILENAME}.service.ts
        fi
        if [ ! -f "./src/controller/${FILENAME}.controller.ts" ]
        then
            touch ./src/controller/${FILENAME}.controller.ts
        fi
    ;;
    *)
        echo "Invalid argument"
    ;;
esac