# Installation

Using docker to install

# Build using docker

Run command

```bash
cp .env.example .env
```

Change what you needed except for DATABASE_HOST because is redirect to postgres docker.
Then run command

```bash
docker-compose up -d
```

# Run for development purpose

Run command

```bash
cp .env.example .env
```

Change what you needed. And run this command sequentially

```bash
npm run dev
```

make sure database connection already prepared and changed in .env
