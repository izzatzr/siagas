--
-- PostgreSQL database dump
--

-- Dumped from database version 12.18
-- Dumped by pg_dump version 12.18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: announcements; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.announcements (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    file_id bigint NOT NULL,
    content text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: announcements_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.announcements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: announcements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.announcements_id_seq OWNED BY public.announcements.id;


--
-- Name: areas; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.areas (
    id bigint NOT NULL,
    region_id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: areas_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.areas_id_seq OWNED BY public.areas.id;


--
-- Name: cluster_details; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.cluster_details (
    cluster_id bigint NOT NULL,
    region_id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: clusters; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.clusters (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: clusters_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.clusters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: clusters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.clusters_id_seq OWNED BY public.clusters.id;


--
-- Name: government_innovations; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.government_innovations (
    id bigint NOT NULL,
    government_name character varying,
    created_by character varying,
    innovation_name character varying,
    innovation_phase character varying,
    innovation_initiator character varying,
    innovation_type character varying,
    innovation_form character varying,
    thematic character varying,
    first_field character varying,
    other_fields character varying,
    trial_time date,
    implementation_time date,
    design text,
    purpose text,
    benefit text,
    result text,
    budget_file_id bigint,
    profile_file_id bigint,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    thematic_detail text,
    government_sector_id bigint,
    updated_by character varying,
    pemda_id bigint,
    foto_id bigint
);



--
-- Name: profil_pemda; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.profil_pemda (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    document_id bigint,
    nama_daerah character varying,
    opd_yang_menangani character varying,
    alamat_pemda character varying,
    email character varying,
    no_telpon character varying,
    nama_admin character varying,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    nama_pemda character varying,
    nominator character varying,
    nominator_rekap_indeks_akhir character varying
);



--
-- Name: dashboard; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.dashboard AS
 SELECT ( SELECT count(profil_pemda.id) AS count
           FROM public.profil_pemda) AS total_pemda,
    ( SELECT count(government_innovations.id) AS count
           FROM public.government_innovations) AS total_inovasi,
    ( SELECT count(gi.id) AS count
           FROM public.government_innovations gi
          WHERE (lower((gi.innovation_phase)::text) = 'uji coba'::text)) AS total_uji_coba,
    ( SELECT count(gi.id) AS count
           FROM public.government_innovations gi
          WHERE (lower((gi.innovation_phase)::text) = 'penerapan'::text)) AS total_penerapan,
    ( SELECT count(gi.id) AS count
           FROM public.government_innovations gi
          WHERE (lower((gi.innovation_phase)::text) = 'inisiatif'::text)) AS total_inisiatif,
    ( SELECT lowest.nama_daerah
           FROM ( SELECT pp.nama_daerah,
                    count(gi.id) AS total_innovation
                   FROM (public.government_innovations gi
                     LEFT JOIN public.profil_pemda pp ON ((pp.id = gi.pemda_id)))
                  GROUP BY pp.nama_daerah
                  ORDER BY (count(gi.id)) DESC
                 LIMIT 1) lowest) AS daerah_tertinggi,
    ( SELECT lowest.nama_daerah
           FROM ( SELECT pp.nama_daerah,
                    count(gi.id) AS total_innovation
                   FROM (public.government_innovations gi
                     LEFT JOIN public.profil_pemda pp ON ((pp.id = gi.pemda_id)))
                  GROUP BY pp.nama_daerah
                  ORDER BY (count(gi.id))
                 LIMIT 1) lowest) AS daerah_terendah;



--
-- Name: dashboard_arsip; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.dashboard_arsip AS
 SELECT gi.id,
    pp.id AS pemda_id,
    pp.nama_daerah,
    gi.innovation_name,
    gi.innovation_phase,
    gi.trial_time,
    gi.implementation_time,
        CASE
            WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 3
            WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 6
            WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 9
            ELSE 0
        END AS skor,
    gi.created_by
   FROM (public.government_innovations gi
     LEFT JOIN public.profil_pemda pp ON ((gi.pemda_id = pp.id)));



--
-- Name: distrik; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.distrik (
    id bigint NOT NULL,
    nama_distrik character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: distrik_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.distrik_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: distrik_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.distrik_id_seq OWNED BY public.distrik.id;


--
-- Name: document_categories; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.document_categories (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: document_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.document_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: document_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.document_categories_id_seq OWNED BY public.document_categories.id;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.documents (
    id bigint NOT NULL,
    category_id bigint,
    title character varying(255) NOT NULL,
    content text NOT NULL,
    document_id bigint,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.documents_id_seq OWNED BY public.documents.id;


--
-- Name: evaluasi_inovasi_daerah; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.evaluasi_inovasi_daerah (
    id bigint NOT NULL,
    inovasi_indikator_id bigint,
    data_saat_ini character varying,
    catatan character varying,
    keterangan character varying,
    tentang character varying,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    kategori character varying
);



--
-- Name: evaluasi_inovasi_daerah_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.evaluasi_inovasi_daerah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: evaluasi_inovasi_daerah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.evaluasi_inovasi_daerah_id_seq OWNED BY public.evaluasi_inovasi_daerah.id;


--
-- Name: evaluasi_review_inovasi_daerah; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.evaluasi_review_inovasi_daerah (
    id bigint NOT NULL,
    data_saat_ini character varying,
    catatan character varying,
    keterangan character varying,
    preview_review_inovasi_daerah_id bigint,
    document_id bigint,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: evaluasi_review_inovasi_daerah_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.evaluasi_review_inovasi_daerah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: evaluasi_review_inovasi_daerah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.evaluasi_review_inovasi_daerah_id_seq OWNED BY public.evaluasi_review_inovasi_daerah.id;


--
-- Name: faq; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.faq (
    id bigint NOT NULL,
    question text NOT NULL,
    answer text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: faq_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.faq_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: faq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.faq_id_seq OWNED BY public.faq.id;


--
-- Name: files; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.files (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    extension character varying(255) NOT NULL,
    size character varying(255) NOT NULL,
    uploaded_by character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.files_id_seq OWNED BY public.files.id;


--
-- Name: government_indicators; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.government_indicators (
    id integer NOT NULL,
    value character varying,
    value_before character varying,
    value_after character varying,
    other_value character varying,
    score character varying,
    scale_id bigint,
    indicator_id bigint NOT NULL,
    gov_profile_id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    deleted_at timestamp without time zone
);



--
-- Name: government_indicators_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.government_indicators_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: government_indicators_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.government_indicators_id_seq OWNED BY public.government_indicators.id;


--
-- Name: government_innovations_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.government_innovations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: government_innovations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.government_innovations_id_seq OWNED BY public.government_innovations.id;


--
-- Name: government_profiles; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.government_profiles (
    id bigint NOT NULL,
    region_id bigint NOT NULL,
    name character varying NOT NULL,
    pic character varying NOT NULL,
    address character varying NOT NULL,
    email character varying NOT NULL,
    phone character varying NOT NULL,
    admin_name character varying NOT NULL,
    file_id bigint,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: government_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.government_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: government_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.government_profiles_id_seq OWNED BY public.government_profiles.id;


--
-- Name: government_sectors; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.government_sectors (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    deadline date NOT NULL
);



--
-- Name: government_sectors_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.government_sectors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: government_sectors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.government_sectors_id_seq OWNED BY public.government_sectors.id;


--
-- Name: indicator_scales; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.indicator_scales (
    id integer NOT NULL,
    indicator_id integer NOT NULL,
    value double precision DEFAULT 0 NOT NULL,
    start_at double precision DEFAULT 0 NOT NULL,
    finish_at double precision DEFAULT 0 NOT NULL,
    description character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: indicator_scales_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.indicator_scales_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: indicator_scales_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.indicator_scales_id_seq OWNED BY public.indicator_scales.id;


--
-- Name: indicators; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.indicators (
    id integer NOT NULL,
    no_urut bigint,
    jenis_indikator character varying,
    nama_indikator character varying,
    keterangan character varying,
    nama_dokumen_pendukung character varying,
    bobot bigint,
    jenis_file character varying,
    parent character varying,
    mandatory character varying,
    created_by character varying,
    updated_by character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: indicators_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.indicators_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: indicators_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.indicators_id_seq OWNED BY public.indicators.id;


--
-- Name: inovasi_indikator; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.inovasi_indikator (
    id bigint NOT NULL,
    inovasi_id bigint,
    indikator_id bigint,
    informasi character varying,
    nilai numeric,
    nilai_sebelum numeric,
    nilai_sesudah numeric,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: inovasi_indikator_file; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.inovasi_indikator_file (
    id bigint NOT NULL,
    inovasi_indikator_id bigint,
    file_id bigint,
    nomor_surat character varying,
    tanggal_surat date,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    tentang character varying,
    inovasi_id bigint,
    indikator_id bigint
);



--
-- Name: inovasi_indikator_file_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.inovasi_indikator_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: inovasi_indikator_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.inovasi_indikator_file_id_seq OWNED BY public.inovasi_indikator_file.id;


--
-- Name: inovasi_indikator_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.inovasi_indikator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: inovasi_indikator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.inovasi_indikator_id_seq OWNED BY public.inovasi_indikator.id;


--
-- Name: review_inovasi_daerah; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.review_inovasi_daerah (
    id integer NOT NULL,
    random_number bigint,
    skor double precision DEFAULT 0 NOT NULL,
    status character varying,
    inovasi_id bigint,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: laporan_bentuk_inovasi; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.laporan_bentuk_inovasi AS
 SELECT gi.pemda_id,
    gi.innovation_form AS bentuk_inovasi,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_form)::text) = lower((gi.innovation_form)::text)) AND (lower((rid.status)::text) = 'accept'::text) AND (gi2.pemda_id = gi.pemda_id))) AS total_disetujui,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_form)::text) = lower((gi.innovation_form)::text)) AND ((lower((rid.status)::text) = 'pending'::text) OR (lower((rid.status)::text) = 'rejected'::text)) AND (gi2.pemda_id = gi.pemda_id))) AS total_ditolak,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_form)::text) = lower((gi.innovation_form)::text)) AND (gi2.pemda_id = gi.pemda_id))) AS total_keseluruhan
   FROM (public.profil_pemda pp
     RIGHT JOIN public.government_innovations gi ON ((gi.pemda_id = pp.id)))
  GROUP BY gi.innovation_form, gi.pemda_id;



--
-- Name: laporan_indeks; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.laporan_indeks AS
SELECT
    NULL::bigint AS pemda_id,
    NULL::character varying AS nama_daerah,
    NULL::character varying AS opd_yang_menangani,
    NULL::bigint AS jumlah_inovasi,
    NULL::bigint AS total_skor_mandiri,
    NULL::double precision AS nilai_indeks,
    NULL::bigint AS total_file,
    NULL::text AS predikat,
    NULL::bigint AS indeks,
    NULL::character varying AS nominator,
    NULL::character varying AS created_by;



--
-- Name: laporan_inisiator_inovasi; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.laporan_inisiator_inovasi AS
 SELECT gi.pemda_id,
    gi.innovation_initiator AS inisiator_inovasi,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_initiator)::text) = lower((gi.innovation_initiator)::text)) AND (lower((rid.status)::text) = 'accept'::text) AND (gi2.pemda_id = gi.pemda_id))) AS total_disetujui,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_initiator)::text) = lower((gi.innovation_initiator)::text)) AND ((lower((rid.status)::text) = 'pending'::text) OR (lower((rid.status)::text) = 'rejected'::text)) AND (gi2.pemda_id = gi.pemda_id))) AS total_ditolak,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_initiator)::text) = lower((gi.innovation_initiator)::text)) AND (gi2.pemda_id = gi.pemda_id))) AS total_keseluruhan
   FROM (public.profil_pemda pp
     RIGHT JOIN public.government_innovations gi ON ((gi.pemda_id = pp.id)))
  GROUP BY gi.innovation_initiator, gi.pemda_id;



--
-- Name: laporan_jenis_inovasi; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.laporan_jenis_inovasi AS
 SELECT gi.pemda_id,
    gi.innovation_type AS jenis_inovasi,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_type)::text) = lower((gi.innovation_type)::text)) AND (lower((rid.status)::text) = 'accept'::text) AND (gi2.pemda_id = gi.pemda_id))) AS total_disetujui,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_type)::text) = lower((gi.innovation_type)::text)) AND ((lower((rid.status)::text) = 'pending'::text) OR (lower((rid.status)::text) = 'rejected'::text)) AND (gi2.pemda_id = gi.pemda_id))) AS total_ditolak,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((lower((gi2.innovation_type)::text) = lower((gi.innovation_type)::text)) AND (gi2.pemda_id = gi.pemda_id))) AS total_keseluruhan
   FROM (public.profil_pemda pp
     RIGHT JOIN public.government_innovations gi ON ((gi.pemda_id = pp.id)))
  GROUP BY gi.innovation_type, gi.pemda_id;



--
-- Name: laporan_urusan_inovasi; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.laporan_urusan_inovasi AS
 SELECT gi.pemda_id,
    gs.name AS urusan_pemerintahan,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((gi2.government_sector_id = gi.government_sector_id) AND (lower((rid.status)::text) = 'accept'::text) AND (gi2.pemda_id = gi.pemda_id))) AS total_disetujui,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((gi2.government_sector_id = gi.government_sector_id) AND ((lower((rid.status)::text) = 'pending'::text) OR (lower((rid.status)::text) = 'rejected'::text)) AND (gi2.pemda_id = gi.pemda_id))) AS total_ditolak,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((gi2.government_sector_id = gi.government_sector_id) AND (gi2.pemda_id = gi.pemda_id))) AS total_keseluruhan
   FROM (public.government_sectors gs
     LEFT JOIN public.government_innovations gi ON ((gi.government_sector_id = gs.id)))
  GROUP BY gi.pemda_id, gs.name, gi.government_sector_id;



--
-- Name: laporan_urusan_inovasi_all; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.laporan_urusan_inovasi_all AS
 SELECT gs.name AS urusan_pemerintahan,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((gi2.government_sector_id = gi.government_sector_id) AND (lower((rid.status)::text) = 'accept'::text))) AS total_disetujui,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE ((gi2.government_sector_id = gi.government_sector_id) AND ((lower((rid.status)::text) = 'pending'::text) OR (lower((rid.status)::text) = 'rejected'::text)))) AS total_ditolak,
    ( SELECT count(rid.id) AS count
           FROM (public.review_inovasi_daerah rid
             LEFT JOIN public.government_innovations gi2 ON ((gi2.id = rid.inovasi_id)))
          WHERE (gi2.government_sector_id = gi.government_sector_id)) AS total_keseluruhan
   FROM (public.government_sectors gs
     LEFT JOIN public.government_innovations gi ON ((gi.government_sector_id = gs.id)))
  GROUP BY gs.name, gi.government_sector_id;



--
-- Name: pakta_integritas; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.pakta_integritas (
    id bigint NOT NULL,
    user_id bigint,
    upload_id bigint,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: pakta_integritas_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.pakta_integritas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: pakta_integritas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.pakta_integritas_id_seq OWNED BY public.pakta_integritas.id;


--
-- Name: pemda_indikator; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.pemda_indikator (
    id bigint NOT NULL,
    pemda_id bigint,
    indikator_id bigint,
    informasi character varying,
    skor numeric,
    nilai_sebelum numeric,
    nilai_sesudah numeric,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    keterangan text,
    catatan text,
    skor_verifikasi numeric,
    data_saat_ini character varying
);



--
-- Name: pemda_indikator_file; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.pemda_indikator_file (
    id bigint NOT NULL,
    pemda_indikator_id bigint,
    file_id bigint,
    nomor_surat character varying,
    tanggal_surat date,
    nama_surat character varying,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    pemda_id bigint,
    indikator_id bigint
);



--
-- Name: pemda_indikator_file_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.pemda_indikator_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: pemda_indikator_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.pemda_indikator_file_id_seq OWNED BY public.pemda_indikator_file.id;


--
-- Name: pemda_indikator_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.pemda_indikator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: pemda_indikator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.pemda_indikator_id_seq OWNED BY public.pemda_indikator.id;


--
-- Name: peringkat_hasil_review; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.peringkat_hasil_review AS
SELECT
    NULL::bigint AS id,
    NULL::character varying AS nama_daerah,
    NULL::bigint AS jumlah_inovasi,
    NULL::text AS isp,
    NULL::bigint AS total_pemda,
    NULL::numeric AS total_skor_verifikasi,
    NULL::text AS predikat,
    NULL::character varying AS nominator,
    NULL::numeric AS skor,
    NULL::numeric AS skor_evaluasi,
    NULL::bigint AS total_skor_mandiri,
    NULL::character varying AS created_by;



--
-- Name: preview_review_inovasi_daerah; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.preview_review_inovasi_daerah (
    id bigint NOT NULL,
    komentar character varying,
    review_inovasi_daerah_id bigint,
    created_by character varying NOT NULL,
    updated_by character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: preview_review_inovasi_daerah_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.preview_review_inovasi_daerah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: preview_review_inovasi_daerah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.preview_review_inovasi_daerah_id_seq OWNED BY public.preview_review_inovasi_daerah.id;


--
-- Name: profil_pemda_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.profil_pemda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: profil_pemda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.profil_pemda_id_seq OWNED BY public.profil_pemda.id;


--
-- Name: rangking_index; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.rangking_index AS
SELECT
    NULL::bigint AS id,
    NULL::character varying AS nama_daerah,
    NULL::bigint AS jumlah_inovasi,
    NULL::bigint AS total_skor_mandiri,
    NULL::double precision AS nilai_indeks,
    NULL::bigint AS total_file,
    NULL::text AS predikat,
    NULL::bigint AS indeks,
    NULL::character varying AS nominator,
    NULL::character varying AS created_by;



--
-- Name: regional_apparatus; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.regional_apparatus (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    created_by character varying(255),
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(255)
);



--
-- Name: regional_apparatus_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.regional_apparatus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: regional_apparatus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.regional_apparatus_id_seq OWNED BY public.regional_apparatus.id;


--
-- Name: regions; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.regions (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: regions_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.regions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: regions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.regions_id_seq OWNED BY public.regions.id;


--
-- Name: rekap_indeks_akhir; Type: VIEW; Schema: public; Owner: koneksik_siagas.api
--

CREATE VIEW public.rekap_indeks_akhir AS
SELECT
    NULL::bigint AS id,
    NULL::character varying AS nama_daerah,
    NULL::bigint AS jumlah_inovasi,
    NULL::bigint AS total_skor_mandiri,
    NULL::double precision AS nilai_indeks,
    NULL::bigint AS total_file,
    NULL::integer AS nilai_indeks_verifikasi,
    NULL::text AS predikat,
    NULL::bigint AS indeks,
    NULL::character varying AS nominator,
    NULL::character varying AS created_by;



--
-- Name: review_inovasi_daerah_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.review_inovasi_daerah_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: review_inovasi_daerah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.review_inovasi_daerah_id_seq OWNED BY public.review_inovasi_daerah.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    is_creator bpchar DEFAULT 't'::bpchar NOT NULL,
    is_super_admin bpchar DEFAULT 't'::bpchar NOT NULL,
    label character varying
);



--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.settings (
    id bigint NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    helper character varying
);



--
-- Name: COLUMN settings.helper; Type: COMMENT; Schema: public; Owner: koneksik_siagas.api
--

COMMENT ON COLUMN public.settings.helper IS 'Helper digunakan untuk menambahkan text bantuan ketika mengisi value';


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: siagas_migrations; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.siagas_migrations (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    name character varying NOT NULL
);



--
-- Name: siagas_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.siagas_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: siagas_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.siagas_migrations_id_seq OWNED BY public.siagas_migrations.id;


--
-- Name: tim_penilaian; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.tim_penilaian (
    id bigint NOT NULL,
    asn_username character varying,
    nama character varying,
    instansi character varying,
    created_by character varying,
    updated_by character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: tim_penilaian_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.tim_penilaian_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: tim_penilaian_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.tim_penilaian_id_seq OWNED BY public.tim_penilaian.id;


--
-- Name: tuxedos; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.tuxedos (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    section character varying(255) NOT NULL,
    content text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);



--
-- Name: tuxedos_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.tuxedos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: tuxedos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.tuxedos_id_seq OWNED BY public.tuxedos.id;


--
-- Name: typeorm_metadata; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.typeorm_metadata (
    type character varying NOT NULL,
    database character varying,
    schema character varying,
    "table" character varying,
    name character varying,
    value text
);



--
-- Name: uptd; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.uptd (
    id bigint NOT NULL,
    regional_apparatus_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    created_by character varying(255),
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_by character varying(255)
);



--
-- Name: uptd_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.uptd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: uptd_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.uptd_id_seq OWNED BY public.uptd.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: koneksik_siagas.api
--

CREATE TABLE public.users (
    id integer NOT NULL,
    role_id integer,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    email character varying(255) NOT NULL,
    full_name character varying(255),
    nickname character varying(255),
    nama_pemda character varying,
    created_by bigint
);



--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: koneksik_siagas.api
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: koneksik_siagas.api
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: announcements id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.announcements ALTER COLUMN id SET DEFAULT nextval('public.announcements_id_seq'::regclass);


--
-- Name: areas id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.areas ALTER COLUMN id SET DEFAULT nextval('public.areas_id_seq'::regclass);


--
-- Name: clusters id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.clusters ALTER COLUMN id SET DEFAULT nextval('public.clusters_id_seq'::regclass);


--
-- Name: distrik id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.distrik ALTER COLUMN id SET DEFAULT nextval('public.distrik_id_seq'::regclass);


--
-- Name: document_categories id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.document_categories ALTER COLUMN id SET DEFAULT nextval('public.document_categories_id_seq'::regclass);


--
-- Name: documents id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.documents ALTER COLUMN id SET DEFAULT nextval('public.documents_id_seq'::regclass);


--
-- Name: evaluasi_inovasi_daerah id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_inovasi_daerah ALTER COLUMN id SET DEFAULT nextval('public.evaluasi_inovasi_daerah_id_seq'::regclass);


--
-- Name: evaluasi_review_inovasi_daerah id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_review_inovasi_daerah ALTER COLUMN id SET DEFAULT nextval('public.evaluasi_review_inovasi_daerah_id_seq'::regclass);


--
-- Name: faq id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.faq ALTER COLUMN id SET DEFAULT nextval('public.faq_id_seq'::regclass);


--
-- Name: files id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.files ALTER COLUMN id SET DEFAULT nextval('public.files_id_seq'::regclass);


--
-- Name: government_indicators id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_indicators ALTER COLUMN id SET DEFAULT nextval('public.government_indicators_id_seq'::regclass);


--
-- Name: government_innovations id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations ALTER COLUMN id SET DEFAULT nextval('public.government_innovations_id_seq'::regclass);


--
-- Name: government_profiles id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_profiles ALTER COLUMN id SET DEFAULT nextval('public.government_profiles_id_seq'::regclass);


--
-- Name: government_sectors id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_sectors ALTER COLUMN id SET DEFAULT nextval('public.government_sectors_id_seq'::regclass);


--
-- Name: indicator_scales id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.indicator_scales ALTER COLUMN id SET DEFAULT nextval('public.indicator_scales_id_seq'::regclass);


--
-- Name: indicators id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.indicators ALTER COLUMN id SET DEFAULT nextval('public.indicators_id_seq'::regclass);


--
-- Name: inovasi_indikator id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator ALTER COLUMN id SET DEFAULT nextval('public.inovasi_indikator_id_seq'::regclass);


--
-- Name: inovasi_indikator_file id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file ALTER COLUMN id SET DEFAULT nextval('public.inovasi_indikator_file_id_seq'::regclass);


--
-- Name: pakta_integritas id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pakta_integritas ALTER COLUMN id SET DEFAULT nextval('public.pakta_integritas_id_seq'::regclass);


--
-- Name: pemda_indikator id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator ALTER COLUMN id SET DEFAULT nextval('public.pemda_indikator_id_seq'::regclass);


--
-- Name: pemda_indikator_file id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file ALTER COLUMN id SET DEFAULT nextval('public.pemda_indikator_file_id_seq'::regclass);


--
-- Name: preview_review_inovasi_daerah id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.preview_review_inovasi_daerah ALTER COLUMN id SET DEFAULT nextval('public.preview_review_inovasi_daerah_id_seq'::regclass);


--
-- Name: profil_pemda id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.profil_pemda ALTER COLUMN id SET DEFAULT nextval('public.profil_pemda_id_seq'::regclass);


--
-- Name: regional_apparatus id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regional_apparatus ALTER COLUMN id SET DEFAULT nextval('public.regional_apparatus_id_seq'::regclass);


--
-- Name: regions id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regions ALTER COLUMN id SET DEFAULT nextval('public.regions_id_seq'::regclass);


--
-- Name: review_inovasi_daerah id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.review_inovasi_daerah ALTER COLUMN id SET DEFAULT nextval('public.review_inovasi_daerah_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: siagas_migrations id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.siagas_migrations ALTER COLUMN id SET DEFAULT nextval('public.siagas_migrations_id_seq'::regclass);


--
-- Name: tim_penilaian id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.tim_penilaian ALTER COLUMN id SET DEFAULT nextval('public.tim_penilaian_id_seq'::regclass);


--
-- Name: tuxedos id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.tuxedos ALTER COLUMN id SET DEFAULT nextval('public.tuxedos_id_seq'::regclass);


--
-- Name: uptd id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.uptd ALTER COLUMN id SET DEFAULT nextval('public.uptd_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: announcements; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.announcements VALUES (13, 'Test Loading', 'Test Laoding Edit', 80, '<p>Test Lopading edit</p>', '2023-06-27 20:29:04.189382', '2023-09-14 02:24:45.289931');
INSERT INTO public.announcements VALUES (19, 'penguguman baru', 'pengumumanbaru', 86, '<p>isi</p>', '2023-09-15 00:06:52.890276', '2023-09-15 00:06:52.890276');
INSERT INTO public.announcements VALUES (17, 'Judul Pengumuman Edit', 'Slug Pengumuman', 79, '<p>akan tampil isi pengumuman</p>', '2023-09-14 02:10:51.319605', '2023-09-27 17:50:16.757075');


--
-- Data for Name: areas; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: cluster_details; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: clusters; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: distrik; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.distrik VALUES (3, 'Distrik 1 Edit', '2023-10-23 23:38:58.261749', '2023-10-23 23:40:35.963476');


--
-- Data for Name: document_categories; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.document_categories VALUES (1, 'Kategori Baru', 'kategori-baru', '2023-03-29 22:43:58.884992', '2023-09-13 01:37:53.985084');
INSERT INTO public.document_categories VALUES (3, 'Kategori Tambahan Edit', 'kategori-tambahan', '2023-09-13 01:46:52.460692', '2023-09-13 01:46:58.672559');
INSERT INTO public.document_categories VALUES (5, 'pendidikan', 'pendidkan-2', '2023-09-15 00:03:19.623299', '2023-09-15 00:03:19.623299');


--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.documents VALUES (6, 3, 'Ini mau nambah data Edit', '<p>Tambah data UYEEE Edit</p>', NULL, '2023-09-14 01:49:37.236553', '2023-09-14 01:49:54.387517');
INSERT INTO public.documents VALUES (3, 1, 'Test edit lagi', '<p>Halo</p>', 22, '2023-04-28 01:34:28.068592', '2023-09-14 13:27:32.487079');
INSERT INTO public.documents VALUES (7, 5, 'pendidikan', '<p>isi	</p>', 85, '2023-09-15 00:04:51.26606', '2023-09-15 00:04:51.26606');


--
-- Data for Name: evaluasi_inovasi_daerah; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.evaluasi_inovasi_daerah VALUES (7, 26, 'Peraturan Kepala Daerah / Peraturan Daerah', NULL, 'string', NULL, 'superadmin', 'superadmin', '2023-10-08 22:18:30.827685', '2023-10-08 22:18:30.827685', 'string');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (8, 66, 'SK Kepala Perangkat Daerah', NULL, 'Keterangan', NULL, 'superadmin', 'superadmin', '2023-10-08 22:26:42.31116', '2023-10-08 22:26:42.31116', 'Sesuai');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (9, 40, 'SK Kepala Perangkat Daerah', NULL, '<p><span style="color: rgb(0, 0, 0);">Diedit</span></p>', NULL, 'superadmin', 'superadmin', '2023-10-09 01:37:16.5755', '2023-10-27 21:51:13.423987', 'Sesuai');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (10, 36, 'SK Kepala Daerah', NULL, '<p>Sgvsgw</p>', NULL, 'superadmin', 'superadmin', '2023-11-08 17:34:22.901414', '2023-11-08 17:34:22.901414', 'Sesuai');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (11, 39, 'SK Kepala Perangkat Daerah', NULL, '<p>SEMBARANG</p>', NULL, 'superadmin', 'superadmin', '2023-11-14 21:11:44.206668', '2023-11-14 21:11:44.206668', 'Sesuai');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (12, 150, 'SK Kepala Perangkat Daerah', NULL, '<p>Sembarang</p>', NULL, 'superadmin', 'superadmin', '2023-11-14 21:19:25.766182', '2023-11-14 21:19:25.766182', 'Sesuai');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (15, 145, 'string', NULL, 'string', NULL, 'superadmin', 'superadmin', '2023-11-15 03:31:02.779909', '2023-11-15 03:31:02.779909', 'string');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (16, 141, 'string', NULL, 'string', NULL, 'superadmin', 'superadmin', '2023-11-15 04:26:57.387399', '2023-11-15 04:26:57.387399', 'string');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (17, 142, 'Peraturan Kepala Daerah / Peraturan Daerah', NULL, '<p>pp</p>', NULL, 'superadmin', 'superadmin', '2023-11-15 13:44:32.160062', '2023-11-15 13:45:18.735593', 'Tidak Sesuai');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (13, 150, 'SK Kepala Daerah', NULL, '<p>String</p>', NULL, 'superadmin', 'superadmin', '2023-11-15 03:28:22.967852', '2023-11-17 22:45:17.480487', 'Sesuai');
INSERT INTO public.evaluasi_inovasi_daerah VALUES (14, 151, 'Tidak dapat diukur', NULL, '<p>string</p>', NULL, 'superadmin', 'superadmin', '2023-11-15 03:29:00.884233', '2023-11-18 00:21:55.41446', 'Tidak Sesuai');


--
-- Data for Name: evaluasi_review_inovasi_daerah; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: faq; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.faq VALUES (12, 'HTML', '<p>Apa saja urutan nomor?</p><ol><li><span class="ql-cursor">﻿</span>Nomor Satu</li><li>Nomor Dua</li><li>Nomor Tiga</li></ol>', '2023-09-14 23:01:55.461708', '2023-09-14 23:39:11.383247');
INSERT INTO public.faq VALUES (13, 'Apa perbedaan Menu Database Inovasi Daerah dengan Menu Lomba Inovasi Daerah', '<p>Perbedaan antara Menu Database Inovasi Daerah dengan Menu Lomba Inovasi Daerah sebagai berikut :</p><ol><li>Menu Database Inovasi Daerah terdiri atas submenu Profil Pemda dan Inovasi Daerah :</li><li class="ql-indent-1">Profil Pemda merupakan submenu yang berfungsi sebagai pengelolaan semua data indikator aspek Satuan Pemerintah Daerah, pengeditan alamat Pemda, alamat email, nomor telephone dan nama admin pengelola</li><li class="ql-indent-1">Inovasi Daerah merupakan submenu yang berfungsi untuk mengupload data Inovasi Daerah baik oleh Akun Pemda maupun Akun OPD, mengelola seluruh data Inovasi dan Indikator Satuan Inovasi serta melihat statistik data terkait dengan jumlah Inovasi Daerah pertahapan.</li><li>Lomba Inovasi Daerah terdiri atas submenu Inovasi Pemerintah Daerah dan Inovasi Masyarakat :</li><li class="ql-indent-1">Profil Pemda merupakan submenu yang berfungsi sebagai pengelolaan semua data indikator aspek Satuan Pemerintah Daerah, pengeditan alamat Pemda, alamat email, nomor telephone dan nama admin pengelola</li><li class="ql-indent-1">Inovasi Daerah merupakan submenu yang berfungsi untuk mengupload data Inovasi Daerah baik oleh Akun Pemda maupun Akun OPD, mengelola seluruh data Inovasi dan Indikator Satuan Inovasi serta melihat statistik data terkait dengan jumlah Inovasi Daerah pertahapan.</li></ol><p><br></p>', '2023-09-14 23:53:07.075104', '2023-09-14 23:53:07.075104');
INSERT INTO public.faq VALUES (14, 'cara login', '<p>masukkan username</p><p>masukkan password</p>', '2023-09-15 00:07:59.685762', '2023-09-15 00:07:59.685762');


--
-- Data for Name: files; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.files VALUES (2, '1679631283370_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', 'upload/inovasi_pemda/1679631283370_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', '.png', '324478', 'superadmin', '2023-03-24 11:14:43.275369', '2023-03-24 11:14:43.275369');
INSERT INTO public.files VALUES (3, '1679631338801_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', 'upload/inovasi_pemda/1679631338801_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', '.png', '324478', 'superadmin', '2023-03-24 11:15:38.703386', '2023-03-24 11:15:38.703386');
INSERT INTO public.files VALUES (4, '1679631684251_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', 'upload/inovasi_pemda/1679631684251_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', '.png', '324478', 'superadmin', '2023-03-24 11:21:24.384704', '2023-03-24 11:21:24.384704');
INSERT INTO public.files VALUES (5, '1679631755541_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', 'upload/inovasi_pemda/1679631755541_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', '.png', '324478', 'superadmin', '2023-03-24 11:22:35.509766', '2023-03-24 11:22:35.509766');
INSERT INTO public.files VALUES (6, '1679631813760_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', 'upload/inovasi_pemda/1679631813760_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', '.png', '324478', 'superadmin', '2023-03-24 11:23:33.655579', '2023-03-24 11:23:33.655579');
INSERT INTO public.files VALUES (7, '1679631915808_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', 'upload/inovasi_pemda/1679631915808_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', '.png', '324478', 'superadmin', '2023-03-24 11:25:16.642346', '2023-03-24 11:25:16.642346');
INSERT INTO public.files VALUES (8, '1679631916744_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_32_27.png', 'upload/inovasi_pemda/1679631916744_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_32_27.png', '.png', '283890', 'superadmin', '2023-03-24 11:25:16.712373', '2023-03-24 11:25:16.712373');
INSERT INTO public.files VALUES (9, '1679639822265_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', 'upload/inovasi_pemda/1679639822265_screencapture-siagas-api-sorong-koneksiku-my-id-docs-2023-03-23-14_44_03.png', '.png', '324478', 'superadmin', '2023-03-24 13:37:02.23782', '2023-03-24 13:37:02.23782');
INSERT INTO public.files VALUES (10, '1679692368992_Kota Surabaya', 'upload/profil_pemda/1679692368992_Kota Surabaya', '.pdf', '81612', 'user', '2023-03-25 04:12:48.830696', '2023-03-25 04:12:48.830696');
INSERT INTO public.files VALUES (11, '1679693307349_Kota Surabaya..pdf', 'upload/profil_pemda/1679693307349_Kota Surabaya..pdf', '.pdf', '81612', 'user', '2023-03-25 04:28:27.188145', '2023-03-25 04:28:27.188145');
INSERT INTO public.files VALUES (12, '1679693403691_Kota Surabaya.pdf', 'upload/profil_pemda/1679693403691_Kota Surabaya.pdf', '.pdf', '81612', 'user', '2023-03-25 04:30:03.51522', '2023-03-25 04:30:03.51522');
INSERT INTO public.files VALUES (13, '1681146138349_after_3 month.png', 'upload/pengumuman/1681146138349_after_3 month.png', '.png', '158814', 'superadmin', '2023-04-11 00:02:18.366811', '2023-04-11 00:02:18.366811');
INSERT INTO public.files VALUES (14, '1681265097594_group_66.pdf', 'upload/pengumuman/1681265097594_group_66.pdf', '.pdf', '399959', 'superadmin', '2023-04-12 09:04:57.722341', '2023-04-12 09:04:57.722341');
INSERT INTO public.files VALUES (15, '1681332644869_5e3855e136db7.jpeg', 'upload/pengumuman/1681332644869_5e3855e136db7.jpeg', '.jpeg', '38617', 'superadmin', '2023-04-13 03:50:44.886004', '2023-04-13 03:50:44.886004');
INSERT INTO public.files VALUES (16, '1681332709711_5e3855e136db7.jpeg', 'upload/pengumuman/1681332709711_5e3855e136db7.jpeg', '.jpeg', '38617', 'superadmin', '2023-04-13 03:51:49.715035', '2023-04-13 03:51:49.715035');
INSERT INTO public.files VALUES (17, '1681409770568_group_66.pdf', 'upload/pengumuman/1681409770568_group_66.pdf', '.pdf', '399959', 'superadmin', '2023-04-14 01:16:10.622326', '2023-04-14 01:16:10.622326');
INSERT INTO public.files VALUES (18, '1681410049165_group_66.pdf', 'upload/pengumuman/1681410049165_group_66.pdf', '.pdf', '399959', 'superadmin', '2023-04-14 01:20:49.200621', '2023-04-14 01:20:49.200621');
INSERT INTO public.files VALUES (19, '1681410143074_group_66.pdf', 'upload/pengumuman/1681410143074_group_66.pdf', '.pdf', '399959', 'superadmin', '2023-04-14 01:22:23.090581', '2023-04-14 01:22:23.090581');
INSERT INTO public.files VALUES (20, '1681413361981_group_66.pdf', 'upload/pengumuman/1681413361981_group_66.pdf', '.pdf', '399959', 'superadmin', '2023-04-14 02:16:02.011814', '2023-04-14 02:16:02.011814');
INSERT INTO public.files VALUES (21, '1682619891505_after_3 month.png', 'upload/dokumen/1682619891505_after_3 month.png', '.png', '158814', 'superadmin', '2023-04-28 01:24:51.524038', '2023-04-28 01:24:51.524038');
INSERT INTO public.files VALUES (22, '1682620468054_after_copywrite.png', 'upload/dokumen/1682620468054_after_copywrite.png', '.png', '163581', 'superadmin', '2023-04-28 01:34:28.060429', '2023-04-28 01:34:28.060429');
INSERT INTO public.files VALUES (23, '1682620816077_after_default 1 month.png', 'upload/dokumen/1682620816077_after_default 1 month.png', '.png', '137822', 'superadmin', '2023-04-28 01:40:16.116355', '2023-04-28 01:40:16.116355');
INSERT INTO public.files VALUES (24, '1683902104625_test.jpeg', 'upload/profil_pemda/1683902104625_test.jpeg', '.jpeg', '156470', 'user', '2023-05-12 21:35:04.830865', '2023-05-12 21:35:04.830865');
INSERT INTO public.files VALUES (25, '1684794615077_pp_blurred background.jpg', 'upload/inovasi_pemda/1684794615077_pp_blurred background.jpg', '.jpg', '1760159', 'superadmin', '2023-05-23 05:30:15.008033', '2023-05-23 05:30:15.008033');
INSERT INTO public.files VALUES (26, '1687872461785_kucing.png', 'upload/pengumuman/1687872461785_kucing.png', '.png', '17208', 'superadmin', '2023-06-27 20:27:41.798706', '2023-06-27 20:27:41.798706');
INSERT INTO public.files VALUES (27, '1687872536831_rectangle.png', 'upload/pengumuman/1687872536831_rectangle.png', '.png', '8332532', 'superadmin', '2023-06-27 20:28:57.204091', '2023-06-27 20:28:57.204091');
INSERT INTO public.files VALUES (28, '1687872544178_kucing.png', 'upload/pengumuman/1687872544178_kucing.png', '.png', '17208', 'superadmin', '2023-06-27 20:29:04.183262', '2023-06-27 20:29:04.183262');
INSERT INTO public.files VALUES (29, '1687872576181_alfatihah.jpeg', 'upload/pengumuman/1687872576181_alfatihah.jpeg', '.jpeg', '156470', 'superadmin', '2023-06-27 20:29:36.189604', '2023-06-27 20:29:36.189604');
INSERT INTO public.files VALUES (30, '1688059852223_grocycode.png', 'upload/profil_pemda/1688059852223_grocycode.png', '.png', '464', 'creator', '2023-06-30 00:30:52.642749', '2023-06-30 00:30:52.642749');
INSERT INTO public.files VALUES (31, '1688831808810_pp_blurred background 1.png', 'upload/pakta_integritas/1688831808810_pp_blurred background 1.png', '.png', '6876344', 'creator', '2023-07-08 22:56:49.335691', '2023-07-08 22:56:49.335691');
INSERT INTO public.files VALUES (32, 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'upload/pemda/dokumen/1690215629917_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:20:29.959238', '2023-07-24 23:20:29.959238');
INSERT INTO public.files VALUES (33, 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'upload/pemda/dokumen/1690215692058_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:21:31.822896', '2023-07-24 23:21:31.822896');
INSERT INTO public.files VALUES (34, 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'upload/pemda/dokumen/1690215724616_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:22:04.364135', '2023-07-24 23:22:04.364135');
INSERT INTO public.files VALUES (35, 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'upload/pemda/dokumen/1690215725829_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:22:05.57546', '2023-07-24 23:22:05.57546');
INSERT INTO public.files VALUES (36, 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'upload/pemda/dokumen/1690215727003_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:22:06.791244', '2023-07-24 23:22:06.791244');
INSERT INTO public.files VALUES (37, 'PENETAPAN INOVASI TERAPI (PETERNAKAN SAPI) SEBAGAI INOVASI BENTUK LAINNYA YANG MENJADI KEWENANGAN PADA KECAMATAN TELUK BELENGKONG KABUPATEN INDRAGIRI HILIR', 'upload/inovasi/dokumen/1690217464812_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:51:04.590633', '2023-07-24 23:51:04.590633');
INSERT INTO public.files VALUES (38, 'PENETAPAN INOVASI TERAPI (PETERNAKAN SAPI) SEBAGAI INOVASI BENTUK LAINNYA YANG MENJADI KEWENANGAN PADA KECAMATAN TELUK BELENGKONG KABUPATEN INDRAGIRI HILIR', 'upload/inovasi/dokumen/1690217469389_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:51:09.151467', '2023-07-24 23:51:09.151467');
INSERT INTO public.files VALUES (39, 'PENETAPAN INOVASI TERAPI (PETERNAKAN SAPI) SEBAGAI INOVASI BENTUK LAINNYA YANG MENJADI KEWENANGAN PADA KECAMATAN TELUK BELENGKONG KABUPATEN INDRAGIRI HILIR', 'upload/inovasi/dokumen/1690217470908_pp_png cool-min.png', '.png', '700923', 'creator', '2023-07-24 23:51:10.667316', '2023-07-24 23:51:10.667316');
INSERT INTO public.files VALUES (40, '1690560918016_419.jpeg', 'upload/profil_pemda/1690560918016_419.jpeg', '.jpeg', '205978', 'superadmin', '2023-07-28 23:15:18.067056', '2023-07-28 23:15:18.067056');
INSERT INTO public.files VALUES (41, '1691336886498_tes_mb.pdf', 'upload/pemda/dokumen/1691336886498_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-06 22:48:06.524911', '2023-08-06 22:48:06.524911');
INSERT INTO public.files VALUES (42, '1691336896331_tes_mb.pdf', 'upload/pemda/dokumen/1691336896331_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-06 22:48:16.335628', '2023-08-06 22:48:16.335628');
INSERT INTO public.files VALUES (43, '1691336902202_tes_mb.pdf', 'upload/pemda/dokumen/1691336902202_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-06 22:48:22.205846', '2023-08-06 22:48:22.205846');
INSERT INTO public.files VALUES (44, '1691336914481_tes_mb.pdf', 'upload/pemda/dokumen/1691336914481_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-06 22:48:34.490671', '2023-08-06 22:48:34.490671');
INSERT INTO public.files VALUES (45, '1691412669352_tes_mb.pdf', 'upload/pemda/dokumen/1691412669352_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-07 19:51:09.372555', '2023-08-07 19:51:09.372555');
INSERT INTO public.files VALUES (46, '1691414095297_tes_mb.pdf', 'upload/pemda/dokumen/1691414095297_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-07 20:14:55.312971', '2023-08-07 20:14:55.312971');
INSERT INTO public.files VALUES (47, '1691416973493_tes_mb.pdf', 'upload/pemda/dokumen/1691416973493_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-07 21:02:53.511323', '2023-08-07 21:02:53.511323');
INSERT INTO public.files VALUES (48, '1691417908648_tes_mb.pdf', 'upload/pemda/dokumen/1691417908648_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-07 21:18:28.655099', '2023-08-07 21:18:28.655099');
INSERT INTO public.files VALUES (49, '1691417950116_tes_mb.pdf', 'upload/pemda/dokumen/1691417950116_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-07 21:19:10.12025', '2023-08-07 21:19:10.12025');
INSERT INTO public.files VALUES (50, '1691423621816_tes_mb.pdf', 'upload/profil_pemda/1691423621816_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-08-07 22:53:41.83069', '2023-08-07 22:53:41.83069');
INSERT INTO public.files VALUES (51, '1692978548443_screen_shot 2023-08-24 at 12.44.28.png', 'upload/inovasi_pemda/1692978548443_screen_shot 2023-08-24 at 12.44.28.png', '.png', '397354', 'superadmin', '2023-08-25 22:49:08.586334', '2023-08-25 22:49:08.586334');
INSERT INTO public.files VALUES (52, '1692978548467_benjamin-voros-phifdc6la4e-unsplash.jpeg', 'upload/inovasi_pemda/1692978548467_benjamin-voros-phifdc6la4e-unsplash.jpeg', '.jpeg', '2737470', 'superadmin', '2023-08-25 22:49:08.605336', '2023-08-25 22:49:08.605336');
INSERT INTO public.files VALUES (53, '1692978635636_screen_shot 2023-08-24 at 12.44.28.png', 'upload/inovasi_pemda/1692978635636_screen_shot 2023-08-24 at 12.44.28.png', '.png', '397354', 'superadmin', '2023-08-25 22:50:36.141784', '2023-08-25 22:50:36.141784');
INSERT INTO public.files VALUES (54, '1692978635663_benjamin-voros-phifdc6la4e-unsplash.jpeg', 'upload/inovasi_pemda/1692978635663_benjamin-voros-phifdc6la4e-unsplash.jpeg', '.jpeg', '2737470', 'superadmin', '2023-08-25 22:50:36.149576', '2023-08-25 22:50:36.149576');
INSERT INTO public.files VALUES (55, '1692978674345_screen_shot 2023-08-24 at 12.44.28.png', 'upload/inovasi_pemda/1692978674345_screen_shot 2023-08-24 at 12.44.28.png', '.png', '397354', 'superadmin', '2023-08-25 22:51:14.573177', '2023-08-25 22:51:14.573177');
INSERT INTO public.files VALUES (56, '1692978674383_benjamin-voros-phifdc6la4e-unsplash.jpeg', 'upload/inovasi_pemda/1692978674383_benjamin-voros-phifdc6la4e-unsplash.jpeg', '.jpeg', '2737470', 'superadmin', '2023-08-25 22:51:14.584302', '2023-08-25 22:51:14.584302');
INSERT INTO public.files VALUES (57, '1693227734210_carbon_(1).png', 'upload/inovasi_pemda/1693227734210_carbon_(1).png', '.png', '127787', 'superadmin', '2023-08-28 20:02:14.283916', '2023-08-28 20:02:14.283916');
INSERT INTO public.files VALUES (58, '1693227734224_carbon_(2).png', 'upload/inovasi_pemda/1693227734224_carbon_(2).png', '.png', '213065', 'superadmin', '2023-08-28 20:02:14.30147', '2023-08-28 20:02:14.30147');
INSERT INTO public.files VALUES (59, '1693227743488_carbon_(1).png', 'upload/inovasi_pemda/1693227743488_carbon_(1).png', '.png', '127787', 'superadmin', '2023-08-28 20:02:23.611237', '2023-08-28 20:02:23.611237');
INSERT INTO public.files VALUES (60, '1693227743499_carbon_(2).png', 'upload/inovasi_pemda/1693227743499_carbon_(2).png', '.png', '213065', 'superadmin', '2023-08-28 20:02:23.618726', '2023-08-28 20:02:23.618726');
INSERT INTO public.files VALUES (61, '1693227787081_carbon_(1).png', 'upload/inovasi_pemda/1693227787081_carbon_(1).png', '.png', '0', 'superadmin', '2023-08-28 20:03:07.130957', '2023-08-28 20:03:07.130957');
INSERT INTO public.files VALUES (62, '1693227787091_carbon_(2).png', 'upload/inovasi_pemda/1693227787091_carbon_(2).png', '.png', '0', 'superadmin', '2023-08-28 20:03:07.14249', '2023-08-28 20:03:07.14249');
INSERT INTO public.files VALUES (63, '1693234746334_carbon_(1).png', 'upload/inovasi_pemda/1693234746334_carbon_(1).png', '.png', '127787', 'superadmin', '2023-08-28 21:59:06.387116', '2023-08-28 21:59:06.387116');
INSERT INTO public.files VALUES (64, '1693234746345_carbon_(1).png', 'upload/inovasi_pemda/1693234746345_carbon_(1).png', '.png', '127787', 'superadmin', '2023-08-28 21:59:06.395614', '2023-08-28 21:59:06.395614');
INSERT INTO public.files VALUES (65, '1693726767577_carbon_(1).png', 'upload/inovasi_pemda/1693726767577_carbon_(1).png', '.png', '127787', 'superadmin', '2023-09-03 14:39:27.643148', '2023-09-03 14:39:27.643148');
INSERT INTO public.files VALUES (66, '1693726767589_carbon_(11).png', 'upload/inovasi_pemda/1693726767589_carbon_(11).png', '.png', '79243', 'superadmin', '2023-09-03 14:39:27.667187', '2023-09-03 14:39:27.667187');
INSERT INTO public.files VALUES (67, '1693726841424_3483794198b80fc3746a76760cde6966.jpeg', 'upload/inovasi_pemda/1693726841424_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-03 14:40:41.493676', '2023-09-03 14:40:41.493676');
INSERT INTO public.files VALUES (68, '1693726841436_3483794198b80fc3746a76760cde6966.jpeg', 'upload/inovasi_pemda/1693726841436_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-03 14:40:41.502536', '2023-09-03 14:40:41.502536');
INSERT INTO public.files VALUES (69, '1693726995425_3483794198b80fc3746a76760cde6966.jpeg', 'upload/inovasi_pemda/1693726995425_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-03 14:43:15.464987', '2023-09-03 14:43:15.464987');
INSERT INTO public.files VALUES (70, '1693726995434_3483794198b80fc3746a76760cde6966.jpeg', 'upload/inovasi_pemda/1693726995434_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-03 14:43:15.471049', '2023-09-03 14:43:15.471049');
INSERT INTO public.files VALUES (71, '1693727545608_3483794198b80fc3746a76760cde6966.jpeg', 'upload/inovasi_pemda/1693727545608_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-03 14:52:25.64732', '2023-09-03 14:52:25.64732');
INSERT INTO public.files VALUES (72, '1693727545616_3483794198b80fc3746a76760cde6966.jpeg', 'upload/inovasi_pemda/1693727545616_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-03 14:52:25.652808', '2023-09-03 14:52:25.652808');
INSERT INTO public.files VALUES (73, '1694089712516_albert-einstein-man-physicist-scientist.jpeg', 'upload/profil_pemda/1694089712516_albert-einstein-man-physicist-scientist.jpeg', '.jpeg', '108906', 'superadmin', '2023-09-07 19:28:32.548696', '2023-09-07 19:28:32.548696');
INSERT INTO public.files VALUES (74, '1694089785111_bbeeb137-907b-400b-ae22-084bdcafa64a-removebg-preview.png', 'upload/inovasi_pemda/1694089785111_bbeeb137-907b-400b-ae22-084bdcafa64a-removebg-preview.png', '.png', '257980', 'superadmin', '2023-09-07 19:29:45.171173', '2023-09-07 19:29:45.171173');
INSERT INTO public.files VALUES (75, '1694089785122_albert-einstein-man-physicist-scientist.jpeg', 'upload/inovasi_pemda/1694089785122_albert-einstein-man-physicist-scientist.jpeg', '.jpeg', '108906', 'superadmin', '2023-09-07 19:29:45.179632', '2023-09-07 19:29:45.179632');
INSERT INTO public.files VALUES (76, '1694629819819_3483794198b80fc3746a76760cde6966.jpeg', 'upload/dokumen/1694629819819_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-14 01:30:19.836755', '2023-09-14 01:30:19.836755');
INSERT INTO public.files VALUES (77, '1694630488621_3483794198b80fc3746a76760cde6966.jpeg', 'upload/dokumen/1694630488621_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-14 01:41:28.63408', '2023-09-14 01:41:28.63408');
INSERT INTO public.files VALUES (78, '1694630977222_albert-einstein-man-physicist-scientist.jpeg', 'upload/dokumen/1694630977222_albert-einstein-man-physicist-scientist.jpeg', '.jpeg', '108906', 'superadmin', '2023-09-14 01:49:37.229961', '2023-09-14 01:49:37.229961');
INSERT INTO public.files VALUES (79, '1694632251293_bbeeb137-907b-400b-ae22-084bdcafa64a-removebg-preview.png', 'upload/pengumuman/1694632251293_bbeeb137-907b-400b-ae22-084bdcafa64a-removebg-preview.png', '.png', '257980', 'superadmin', '2023-09-14 02:10:51.31281', '2023-09-14 02:10:51.31281');
INSERT INTO public.files VALUES (80, '1694633085268_3483794198b80fc3746a76760cde6966.jpeg', 'upload/pengumuman/1694633085268_3483794198b80fc3746a76760cde6966.jpeg', '.jpeg', '23655', 'superadmin', '2023-09-14 02:24:45.282147', '2023-09-14 02:24:45.282147');
INSERT INTO public.files VALUES (81, '1694709111950_template_cover depan.pdf', 'upload/profil_pemda/1694709111950_template_cover depan.pdf', '.pdf', '102518', 'superadmin', '2023-09-14 23:31:51.993355', '2023-09-14 23:31:51.993355');
INSERT INTO public.files VALUES (82, '1694709433538_template_cover depan.pdf', 'upload/profil_pemda/1694709433538_template_cover depan.pdf', '.pdf', '102518', 'superadmin', '2023-09-14 23:37:13.565497', '2023-09-14 23:37:13.565497');
INSERT INTO public.files VALUES (83, '1694709840224_template_cover depan (2).pdf', 'upload/inovasi_pemda/1694709840224_template_cover depan (2).pdf', '.pdf', '102518', 'superadmin', '2023-09-14 23:44:00.278694', '2023-09-14 23:44:00.278694');
INSERT INTO public.files VALUES (84, '1694709840230_template_cover depan (2).pdf', 'upload/inovasi_pemda/1694709840230_template_cover depan (2).pdf', '.pdf', '102518', 'superadmin', '2023-09-14 23:44:00.299229', '2023-09-14 23:44:00.299229');
INSERT INTO public.files VALUES (85, '1694711091248_template_cover depan (2).pdf', 'upload/dokumen/1694711091248_template_cover depan (2).pdf', '.pdf', '102518', 'superadmin', '2023-09-15 00:04:51.259993', '2023-09-15 00:04:51.259993');
INSERT INTO public.files VALUES (86, '1694711212873_template_cover depan (2).pdf', 'upload/pengumuman/1694711212873_template_cover depan (2).pdf', '.pdf', '102518', 'superadmin', '2023-09-15 00:06:52.881609', '2023-09-15 00:06:52.881609');
INSERT INTO public.files VALUES (87, '1695366735585_whatsapp_image 2023-09-17 at 21.33.29.jpeg', 'upload/profil_pemda/1695366735585_whatsapp_image 2023-09-17 at 21.33.29.jpeg', '.jpeg', '173789', 'user', '2023-09-22 14:12:15.620416', '2023-09-22 14:12:15.620416');
INSERT INTO public.files VALUES (88, '1696418687015_4b2cf09b-4cfd-44ee-8931-f3f1e5147584.png', 'upload/pemda/dokumen/1696418687015_4b2cf09b-4cfd-44ee-8931-f3f1e5147584.png', '.png', '854289', 'user', '2023-10-04 18:24:47.11963', '2023-10-04 18:24:47.11963');
INSERT INTO public.files VALUES (89, '1696578312645_img-20230927-wa0009-removebg-preview.png', 'upload/pemda/dokumen/1696578312645_img-20230927-wa0009-removebg-preview.png', '.png', '237603', 'superadmin', '2023-10-06 14:45:12.683254', '2023-10-06 14:45:12.683254');
INSERT INTO public.files VALUES (90, '1696578379508_img-20230927-wa0009-removebg-preview.png', 'upload/pemda/dokumen/1696578379508_img-20230927-wa0009-removebg-preview.png', '.png', '237603', 'superadmin', '2023-10-06 14:46:19.517086', '2023-10-06 14:46:19.517086');
INSERT INTO public.files VALUES (91, '1696591447524_cv_okt23.pdf.pdf', 'upload/pemda/dokumen/1696591447524_cv_okt23.pdf.pdf', '.pdf', '587862', 'superadmin', '2023-10-06 18:24:07.567531', '2023-10-06 18:24:07.567531');
INSERT INTO public.files VALUES (92, '1696662993300_1638853983.jpg', 'upload/inovasi_pemda/1696662993300_1638853983.jpg', '.jpg', '148464', 'superadmin', '2023-10-07 14:16:33.370968', '2023-10-07 14:16:33.370968');
INSERT INTO public.files VALUES (93, '1696662993319_1639396874.jpg', 'upload/inovasi_pemda/1696662993319_1639396874.jpg', '.jpg', '147938', 'superadmin', '2023-10-07 14:16:33.387843', '2023-10-07 14:16:33.387843');
INSERT INTO public.files VALUES (94, '1696691428481_5614cdd2-4d02-4e36-b62e-42b649e4b439.jpeg', 'upload/pemda/dokumen/1696691428481_5614cdd2-4d02-4e36-b62e-42b649e4b439.jpeg', '.jpeg', '35209', 'user', '2023-10-07 22:10:28.506508', '2023-10-07 22:10:28.506508');
INSERT INTO public.files VALUES (95, '1696780162766_manual%20book%20&%20petunjuk%20teknis%20siagas%202023.pdf.pdf', 'upload/pakta_integritas/1696780162766_manual%20book%20&%20petunjuk%20teknis%20siagas%202023.pdf.pdf', '.pdf', '5895358', 'user', '2023-10-08 22:49:23.156709', '2023-10-08 22:49:23.156709');
INSERT INTO public.files VALUES (96, '1696780346220_juara_2 lomba pubgm.pdf', 'upload/pakta_integritas/1696780346220_juara_2 lomba pubgm.pdf', '.pdf', '1038404', 'user', '2023-10-08 22:52:24.425089', '2023-10-08 22:52:24.425089');
INSERT INTO public.files VALUES (97, '1696780719724_juara_2 lomba pubgm.pdf', 'upload/pakta_integritas/1696780719724_juara_2 lomba pubgm.pdf', '.pdf', '1038404', 'user', '2023-10-08 22:58:39.796644', '2023-10-08 22:58:39.796644');
INSERT INTO public.files VALUES (98, '1696781555055_1691423621816_tes_mb.pdf', 'upload/pakta_integritas/1696781555055_1691423621816_tes_mb.pdf', '.pdf', '24767', 'user', '2023-10-08 23:12:35.075336', '2023-10-08 23:12:35.075336');
INSERT INTO public.files VALUES (99, '1696783444833_juara_2 lomba pubgm.pdf', 'upload/pemda/dokumen/1696783444833_juara_2 lomba pubgm.pdf', '.pdf', '1038404', 'creator', '2023-10-08 23:44:03.026897', '2023-10-08 23:44:03.026897');
INSERT INTO public.files VALUES (100, '1696783461710_juara_2 lomba pubgm.pdf', 'upload/pemda/dokumen/1696783461710_juara_2 lomba pubgm.pdf', '.pdf', '1038404', 'creator', '2023-10-08 23:44:19.619053', '2023-10-08 23:44:19.619053');
INSERT INTO public.files VALUES (101, '1696784290168_d_o n a t.pdf', 'upload/pemda/dokumen/1696784290168_d_o n a t.pdf', '.pdf', '53638', 'user', '2023-10-08 23:58:10.192505', '2023-10-08 23:58:10.192505');
INSERT INTO public.files VALUES (102, '1696785097806_juara_2 lomba pubgm.pdf', 'upload/inovasi/dokumen/1696785097806_juara_2 lomba pubgm.pdf', '.pdf', '1038404', 'creator', '2023-10-09 00:11:35.979161', '2023-10-09 00:11:35.979161');
INSERT INTO public.files VALUES (103, '1696785892348_1691423621816_tes_mb.pdf', 'upload/pemda/dokumen/1696785892348_1691423621816_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-10-09 00:24:52.397213', '2023-10-09 00:24:52.397213');
INSERT INTO public.files VALUES (104, '1696785999965_1691423621816_tes_mb.pdf', 'upload/inovasi/dokumen/1696785999965_1691423621816_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-10-09 00:26:39.974776', '2023-10-09 00:26:39.974776');
INSERT INTO public.files VALUES (105, '1696786101046_tes_mb.pdf', 'upload/inovasi/dokumen/1696786101046_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-10-09 00:28:21.051636', '2023-10-09 00:28:21.051636');
INSERT INTO public.files VALUES (106, '1696786516744_petunjuk-teknis.pdf', 'upload/inovasi/dokumen/1696786516744_petunjuk-teknis.pdf', '.pdf', '5895358', 'superadmin', '2023-10-09 00:35:17.565389', '2023-10-09 00:35:17.565389');
INSERT INTO public.files VALUES (107, '1697007224963_9d4d403e-1234-4ff4-9de1-08b700589452.jpeg', 'upload/pemda/dokumen/1697007224963_9d4d403e-1234-4ff4-9de1-08b700589452.jpeg', '.jpeg', '35209', 'superadmin', '2023-10-11 13:53:44.997036', '2023-10-11 13:53:44.997036');
INSERT INTO public.files VALUES (108, '1697388048967_screenshot_2023-10-14 105812.png', 'upload/inovasi_pemda/1697388048967_screenshot_2023-10-14 105812.png', '.png', '16905', 'superadmin', '2023-10-15 23:40:47.887426', '2023-10-15 23:40:47.887426');
INSERT INTO public.files VALUES (109, '1697644075416_albert-einstein-man-physicist-scientist.jpeg', 'upload/inovasi_pemda/1697644075416_albert-einstein-man-physicist-scientist.jpeg', '.jpeg', '108906', 'superadmin', '2023-10-18 22:47:55.486958', '2023-10-18 22:47:55.486958');
INSERT INTO public.files VALUES (110, '1697644922857_albert-einstein-man-physicist-scientist.jpeg', 'upload/profil_pemda/1697644922857_albert-einstein-man-physicist-scientist.jpeg', '.jpeg', '108906', 'user22', '2023-10-18 23:02:02.961538', '2023-10-18 23:02:02.961538');
INSERT INTO public.files VALUES (111, '1697645146942_albert-einstein-man-physicist-scientist.jpeg', 'upload/inovasi_pemda/1697645146942_albert-einstein-man-physicist-scientist.jpeg', '.jpeg', '108906', 'user22', '2023-10-18 23:05:47.007863', '2023-10-18 23:05:47.007863');
INSERT INTO public.files VALUES (112, '1698418123069_1697644922857_albert-einstein-man-physicist-scientist.jpeg', 'upload/inovasi/dokumen/1698418123069_1697644922857_albert-einstein-man-physicist-scientist.jpeg', '.jpeg', '108906', 'superadmin', '2023-10-27 21:48:43.089113', '2023-10-27 21:48:43.089113');
INSERT INTO public.files VALUES (113, '1698418451275_1691423621816_tes_mb.pdf', 'upload/inovasi/dokumen/1698418451275_1691423621816_tes_mb.pdf', '.pdf', '24767', 'superadmin', '2023-10-27 21:54:11.284388', '2023-10-27 21:54:11.284388');
INSERT INTO public.files VALUES (114, '1699439541395_whatsapp_image 2023-09-24 at 16.48.28.jpeg', 'upload/pemda/dokumen/1699439541395_whatsapp_image 2023-09-24 at 16.48.28.jpeg', '.jpeg', '177865', 'superadmin', '2023-11-08 17:32:21.413977', '2023-11-08 17:32:21.413977');
INSERT INTO public.files VALUES (115, '1699439636499_whatsapp_image 2023-09-24 at 16.48.28.jpeg', 'upload/inovasi/dokumen/1699439636499_whatsapp_image 2023-09-24 at 16.48.28.jpeg', '.jpeg', '177865', 'superadmin', '2023-11-08 17:33:56.507488', '2023-11-08 17:33:56.507488');
INSERT INTO public.files VALUES (116, '1699971061194_test_insurance.pdf', 'upload/inovasi/dokumen/1699971061194_test_insurance.pdf', '.pdf', '12431', 'superadmin', '2023-11-14 21:11:01.22883', '2023-11-14 21:11:01.22883');
INSERT INTO public.files VALUES (117, '1700030647947_5a262a70-4996-4b5b-bf25-41ae0854beab.png', 'upload/inovasi/dokumen/1700030647947_5a262a70-4996-4b5b-bf25-41ae0854beab.png', '.png', '854289', 'superadmin', '2023-11-15 13:44:07.992307', '2023-11-15 13:44:07.992307');


--
-- Data for Name: government_indicators; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: government_innovations; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.government_innovations VALUES (21, 'superadmin', 'superadmin', NULL, 'penerapan', 'ASN', 'Digital', 'Inovasi Pelayanan Publik', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, '2023-09-05 15:23:29.693367', '2023-09-05 15:23:29.693367', NULL, 1, NULL, 1, NULL);
INSERT INTO public.government_innovations VALUES (22, 'superadmin', 'superadmin', NULL, 'uji coba', 'OPD', 'Digital', 'Inovasi daerah lainnya dengan urusan pemerintahan yang menjadi kewenangan daerah', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, '2023-09-05 15:27:45.378459', '2023-09-05 15:27:45.378459', NULL, 1, NULL, 9, NULL);
INSERT INTO public.government_innovations VALUES (23, 'superadmin', 'superadmin', NULL, 'inisiatif', 'ASN', 'Digital', 'Inovasi Pelayanan Publik', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, '2023-09-05 15:31:49.047498', '2023-09-05 15:31:49.047498', NULL, 3, NULL, 9, NULL);
INSERT INTO public.government_innovations VALUES (24, 'superadmin', 'superadmin', NULL, 'inisiatif', 'Anggota DPRD', 'Digital', 'Inovasi daerah lainnya dengan urusan pemerintahan yang menjadi kewenangan daerah', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, '2023-09-05 15:33:44.299924', '2023-09-05 15:33:44.299924', NULL, 1, NULL, 9, NULL);
INSERT INTO public.government_innovations VALUES (25, 'superadmin', 'superadmin', NULL, 'inisiatif', 'Masyarakat', 'Digital', 'Inovasi daerah lainnya dengan urusan pemerintahan yang menjadi kewenangan daerah', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, '2023-09-05 15:36:14.947035', '2023-09-05 15:36:14.947035', NULL, 1, NULL, 9, NULL);
INSERT INTO public.government_innovations VALUES (32, 'superadmin', 'superadmin', 'ULP Aimas', 'inisiatif', 'ASN', 'Digital', 'Inovasi daerah lainnya dengan urusan pemerintahan yang menjadi kewenangan daerah', 'Digitalisasi layanan pemerintahan', NULL, NULL, '2023-10-04', '2023-10-31', '<p>Rancang Bangun</p>', '<p>Tujuan</p>', '<p>Manfaat</p>', '<p>Hasil inovasi</p>', NULL, NULL, '2023-10-08 21:53:16.423216', '2023-10-08 21:53:16.423216', NULL, 10, NULL, 23, NULL);
INSERT INTO public.government_innovations VALUES (33, 'user55', 'user55', 'JANDU', 'inisiatif', 'ASN', 'Digital', 'Inovasi pelayanan publik', 'Penanggulangan kemiskinan', NULL, NULL, '2023-10-10', '2024-01-10', NULL, NULL, NULL, NULL, NULL, NULL, '2023-10-10 19:29:32.253063', '2023-10-10 19:29:32.253063', NULL, 10, NULL, 28, NULL);
INSERT INTO public.government_innovations VALUES (34, 'user55', 'user55', 'Jandu', 'inisiatif', 'ASN', 'Digital', 'Inovasi pelayanan publik', 'Penanggulangan kemiskinan', NULL, NULL, '2023-10-10', '2023-10-02', NULL, NULL, NULL, NULL, NULL, NULL, '2023-10-10 19:29:33.479596', '2023-10-10 19:29:33.479596', NULL, 10, NULL, 28, NULL);
INSERT INTO public.government_innovations VALUES (31, 'superadmin', 'superadmin', 'Geometeorolog dan Geofisika Sorong', 'uji coba', 'OPD', 'Digital', 'Inovasi daerah lainnya dengan urusan pemerintahan yang menjadi kewenangan daerah', 'Digitalisasi layanan pemerintahan', NULL, NULL, '2023-10-04', '2023-10-31', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut turpis at urna facilisis ullamcorper. In metus dolor, consequat sed auctor vel, venenatis eget lectus. Curabitur convallis ut tellus in interdum. Donec vulputate dui eget posuere aliquam. Nunc egestas augue eget ligula fringilla suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam malesuada lectus ex, eu dictum eros pulvinar ut.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut turpis at urna facilisis ullamcorper. In metus dolor, consequat sed auctor vel, venenatis eget lectus. Curabitur convallis ut tellus in interdum. Donec vulputate dui eget posuere aliquam. Nunc egestas augue eget ligula fringilla suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam malesuada lectus ex, eu dictum eros pulvinar ut.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut turpis at urna facilisis ullamcorper. In metus dolor, consequat sed auctor vel, venenatis eget lectus. Curabitur convallis ut tellus in interdum. Donec vulputate dui eget posuere aliquam. Nunc egestas augue eget ligula fringilla suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam malesuada lectus ex, eu dictum eros pulvinar ut.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ut turpis at urna facilisis ullamcorper. In metus dolor, consequat sed auctor vel, venenatis eget lectus. Curabitur convallis ut tellus in interdum. Donec vulputate dui eget posuere aliquam. Nunc egestas augue eget ligula fringilla suscipit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam malesuada lectus ex, eu dictum eros pulvinar ut.</p>', NULL, NULL, '2023-10-08 21:26:22.237473', '2023-10-08 21:26:22.237473', NULL, 11, NULL, 25, NULL);
INSERT INTO public.government_innovations VALUES (30, 'user', 'superadmin', 'Ddcc', 'inisiatif', 'Kepala Daerah', NULL, NULL, 'Digitalisasi layanan pemerintahan', NULL, NULL, '2023-10-07', '2023-10-07', '<p>Xxc</p>', '<p>Ccc</p>', '<p>Ccc</p>', '<p>  C</p>', 92, 93, '2023-10-07 14:16:33.396692', '2023-10-07 14:16:33.446429', NULL, 15, NULL, 13, NULL);
INSERT INTO public.government_innovations VALUES (37, 'User22', 'user22', 'Test Inovasi Daearh User 22', 'inisiatif', 'Kepala Daerah', 'Digital', 'Inovasi daerah lainnya dengan urusan pemerintahan yang menjadi kewenangan daerah', 'Digitalisasi layanan pemerintahan', NULL, NULL, '2023-10-04', '2023-10-31', '<p>Rancang Bangun</p>', '<p>Tujuan Inovasi Daerah</p>', '<p>Manfaat yang diperoleh</p>', '<p>Hasil Inovasi</p>', NULL, NULL, '2023-10-18 23:05:47.022085', '2023-10-18 23:12:34.41747', NULL, 10, NULL, 30, 111);
INSERT INTO public.government_innovations VALUES (36, 'User22', 'user22', 'Test Foto', 'inisiatif', 'Kepala Daerah', 'Digital', 'Inovasi daerah lainnya dengan urusan pemerintahan yang menjadi kewenangan daerah', 'Digitalisasi layanan pemerintahan', NULL, NULL, '2023-10-19', '2023-10-31', '<p>Rancang Bangun&nbsp;(Minimal 300 kata)</p>', '<p>Tujuan Inovasi Daerah</p>', '<p>Manfaat yang diperoleh</p>', '<p>Hasil Inovasi</p>', NULL, 109, '2023-10-18 22:46:16.434473', '2023-10-18 23:14:29.063729', NULL, 10, NULL, 28, NULL);


--
-- Data for Name: government_profiles; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.government_profiles VALUES (2, 1, 'ini daerah test', 'test', 'test', 'test@test.com', 'test', 'test', NULL, '2023-05-23 05:15:46.111553', '2023-05-23 05:15:46.111553');
INSERT INTO public.government_profiles VALUES (3, 1, 'ini daerah test 2', 'test', 'test', 'test@test.com', 'test', 'test', NULL, '2023-05-23 05:15:55.031887', '2023-05-23 05:15:55.031887');


--
-- Data for Name: government_sectors; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.government_sectors VALUES (1, 'Test Deadline', '2023-04-05 05:14:58.171235', '2023-04-05 05:14:58.171235', '2023-04-07');
INSERT INTO public.government_sectors VALUES (2, 'Test authorisasi', '2023-04-05 05:46:59.023152', '2023-04-05 05:46:59.023152', '2023-04-10');
INSERT INTO public.government_sectors VALUES (3, 'Urusan lain', '2023-06-29 23:24:59.060926', '2023-06-29 23:24:59.060926', '2023-07-10');
INSERT INTO public.government_sectors VALUES (4, 'Urusan Gue nih', '2023-09-13 00:51:40.746994', '2023-09-13 00:51:40.746994', '2023-11-13');
INSERT INTO public.government_sectors VALUES (5, 'Lebih dari 16', '2023-09-13 00:52:12.953584', '2023-09-13 00:52:12.953584', '2023-09-30');
INSERT INTO public.government_sectors VALUES (6, 'Testing Warna Merah', '2023-09-13 00:53:17.76564', '2023-09-13 00:53:17.76564', '2023-09-17');
INSERT INTO public.government_sectors VALUES (7, 'Testing Warna Kuning', '2023-09-13 00:53:42.332885', '2023-09-13 00:53:42.332885', '2023-09-19');
INSERT INTO public.government_sectors VALUES (8, 'Testing Warna Kuning 2', '2023-09-13 01:05:05.68819', '2023-09-13 01:06:25.042217', '2023-09-14');
INSERT INTO public.government_sectors VALUES (9, 'Test Staging Edit', '2023-09-13 02:03:14.044018', '2023-09-13 02:03:28.982908', '2023-09-15');
INSERT INTO public.government_sectors VALUES (10, 'pendidikan online', '2023-09-14 23:57:08.926783', '2023-09-14 23:57:08.926783', '2023-10-28');
INSERT INTO public.government_sectors VALUES (11, 'pendidikan 2', '2023-09-15 00:01:13.925309', '2023-09-15 00:01:13.925309', '2023-11-16');
INSERT INTO public.government_sectors VALUES (15, 'undefined', '2023-10-07 14:16:33.396692', '2023-10-07 14:16:33.446429', '2023-10-07');
INSERT INTO public.government_sectors VALUES (16, '', '2023-10-15 23:40:47.923949', '2023-10-15 23:40:48.188931', '2023-10-15');


--
-- Data for Name: indicator_scales; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: indicators; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.indicators VALUES (6, 2, 'spd', 'Visi Dan Misi Pemda', '<p>VISI DAN MISI PEMDA</p>', 'INSTITUISI Visi dan misi pemda', 10, 'Application/PDF', 'Visi dan Misi', 'Mandatory', NULL, NULL, '2023-06-30 16:19:37.92963', '2023-10-08 16:30:59.721901');
INSERT INTO public.indicators VALUES (7, 2, 'spd', 'PENERAPAN SIPD', '<p>PENERAPAN SIPD</p>', 'INSTITUISI PENERAPAN SIPD', 10, 'Application/PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Mandatory', NULL, NULL, '2023-06-30 16:19:37.92963', '2023-10-08 16:32:02.803397');
INSERT INTO public.indicators VALUES (5, 2, 'spd', 'APBD TEPAT WAKTU', '<p>APBD TEPAT WAKTU</p>', 'INSTITUISI', 10, 'Application/PDF', 'APBD Tepat Waktu', 'Tidak Mandatory', NULL, NULL, '2023-06-30 16:19:37.92963', '2023-10-08 16:33:15.865666');
INSERT INTO public.indicators VALUES (8, 2, 'spd', 'KUALITAS PENINGKATAN PERIZINAN', '<p>KUALITAS PENINGKATAN PERIZINAN</p>', 'INSTITUISI KUALITAS PENINGKATAN PERIZINAN', 13, 'Application/PDF', 'Kualitas Peningkatan Perizinan', 'Mandatory', NULL, NULL, '2023-06-30 16:19:37.92963', '2023-10-08 16:34:24.102213');
INSERT INTO public.indicators VALUES (9, 123, 'spd', 'JUMLAH PENDAPATAN PERKAPITA', '<p>JUMLAH PENDAPATAN PERKAPITA</p>', 'INSTITUISI JUMLAH PENDAPATAN PERKAPITA', 10, 'Application/PDF', 'Visi dan Misi', 'Mandatory', NULL, NULL, '2023-09-12 23:13:00.393676', '2023-10-08 16:35:16.486043');
INSERT INTO public.indicators VALUES (10, 4, 'spd', 'TINGKAT PENGGANGURAN TERBUKA', '<p>TINGKAT PENGGANGURAN TERBUKA</p>', 'INSTITUISI TINGKAT PENGGANGURAN TERBUKA', 20, 'Application/PDF', 'Penurunan Tingkat Pengangguran Terbuka', 'Mandatory', NULL, NULL, '2023-09-12 23:33:19.555764', '2023-10-08 16:36:17.316145');
INSERT INTO public.indicators VALUES (11, 5, 'spd', 'JUMLAH PENINGKATAN INVESTASI', '<p>TINGKAT PENGGANGURAN TERBUKA</p>', 'TINGKAT PENGGANGURAN TERBUKA INSTITUISI', 10, 'Application/PDF', 'Jumlah Peningkatan Investasi', 'Mandatory', NULL, NULL, '2023-09-12 23:38:36.951992', '2023-10-08 16:37:10.552658');
INSERT INTO public.indicators VALUES (12, 45, 'spd', 'JUMLAH PENINGKATAN PAD', '<p>JUMLAH PENINGKATAN PAD</p>', 'INSTITUISI JUMLAH PENINGKATAN PAD', 20, 'Application/PDF', 'Visi dan Misi', 'Mandatory', NULL, NULL, '2023-09-13 02:02:42.935376', '2023-10-08 16:38:14.311728');
INSERT INTO public.indicators VALUES (19, 13, 'spd', 'ROADMAP SIDA', '<p>ROADMAP SIDA</p>', 'ROADMAP SIDA EKOSISTEM INOVASI', 15, 'PDF', 'APBD Tepat Waktu', 'Mandatory', NULL, NULL, '2023-10-08 16:50:32.589908', '2023-10-08 16:50:32.589908');
INSERT INTO public.indicators VALUES (20, 14, 'si', 'REGULASI INOVASI DAERAH', '<p>	REGULASI INOVASI DAERAH</p>', 'INFRASTRUKTUR', 10, '16', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Mandatory', NULL, NULL, '2023-10-08 16:52:26.656613', '2023-10-08 16:52:26.656613');
INSERT INTO public.indicators VALUES (14, 123123, 'spd', 'NILAI CAPAIAN LAKIP', '<p>NILAI CAPAIAN LAKIP</p>', 'NILAI CAPAIAN LAKIP INSTITUISI', 1, '123123', 'Kualitas Peningkatan Perizinan', 'Mandatory', NULL, NULL, '2023-10-05 22:43:02.863602', '2023-10-08 16:40:26.135256');
INSERT INTO public.indicators VALUES (13, 73, 'spd', 'OPINI BPK', '<p>OPINI BPK</p>', 'OPINI BPK INSTITUISI', 1, 'pdf', 'Opini BPK', 'Tidak Mandatory', NULL, NULL, '2023-09-15 00:09:43.505827', '2023-10-08 16:41:57.071565');
INSERT INTO public.indicators VALUES (15, 333, 'spd', 'PENURUNAN ANGKA KEMISKINAN', '<p>PENURUNAN ANGKA KEMISKINAN</p>', 'PENURUNAN ANGKA KEMISKINAN INSTITUISI', 12, 'PDF', 'APBD Tepat Waktu', 'Tidak Mandatory', NULL, NULL, '2023-10-07 18:15:36.456785', '2023-10-08 16:43:11.938065');
INSERT INTO public.indicators VALUES (16, 43, 'spd', 'NILAI IPM', '<p>NILAI IPM</p>', 'INSTITUISI NILAI IPM', 1, 'PDF', 'Jumlah Pendapatan Perkapita', 'Mandatory', NULL, NULL, '2023-10-07 18:19:37.063185', '2023-10-08 16:43:57.937367');
INSERT INTO public.indicators VALUES (17, 44, 'spd', 'PENGHARGAAN INOVATOR', '<p>PENGHARGAAN INOVATOR</p>', 'PENGHARGAAN INOVATOR INSTITUISI', 1, 'PDF', 'APBD Tepat Waktu', 'Tidak Mandatory', NULL, NULL, '2023-10-07 18:35:37.99461', '2023-10-08 16:44:40.871722');
INSERT INTO public.indicators VALUES (18, 12, 'spd', 'JUMLAH PENELITIAN/KAJIAN INOVASI', '<p>JUMLAH PENELITIAN/KAJIAN INOVASI</p>', 'JUMLAH PENELITIAN/KAJIAN INOVASI INSTITUISI', 12, 'PDF', 'Kualitas Peningkatan Perizinan', 'Mandatory', NULL, NULL, '2023-10-08 16:46:58.960826', '2023-10-08 16:46:58.960826');
INSERT INTO public.indicators VALUES (21, 21, 'si', 'KETERSEDIAAN SDM INOVASI DAERAH', '<p>KETERSEDIAAN SDM INOVASI DAERAH</p>', 'KETERSEDIAAN SDM INOVASI DAERAH INFRASTRUKTUR', 20, 'PDF', 'Jumlah Peningkatan Investasi', 'Mandatory', NULL, NULL, '2023-10-08 16:53:44.391667', '2023-10-08 16:53:44.391667');
INSERT INTO public.indicators VALUES (22, 22, 'si', 'DUKUNGAN ANGGARAN', '<p>DUKUNGAN ANGGARAN</p>', 'DUKUNGAN ANGGARAN INFRASTRUKTUR', 15, 'PDF', 'Jumlah Peningkatan PAD', 'Mandatory', NULL, NULL, '2023-10-08 16:55:51.837573', '2023-10-08 16:55:51.837573');
INSERT INTO public.indicators VALUES (23, 24, 'si', 'BIMTEK INOVASI', '<p>BIMTEK INOVASI</p>', 'BIMTEK INOVASI INFRASTUKTUR', 10, 'PDF', 'Jumlah Pendapatan Perkapita', 'Mandatory', NULL, NULL, '2023-10-08 16:56:51.320571', '2023-10-08 16:56:51.320571');
INSERT INTO public.indicators VALUES (24, 26, 'si', 'INTEGRASI PROGRAM DALAM RKPF', '<p>INTEGRASI PROGRAM DALAM RKPF</p>', 'INTEGRASI PROGRAM DALAM RKPF INFRASTURKTUR', 10, 'PDF', 'Visi dan Misi', 'Mandatory', NULL, NULL, '2023-10-08 16:57:50.76171', '2023-10-08 16:57:50.76171');
INSERT INTO public.indicators VALUES (25, 29, 'si', 'KETERLIBATAN AKTOR INOVASI', '<p>KETERLIBATAN AKTOR INOVASI</p>', 'KETERLIBATAN AKTOR INOVASI OUTPUT', 10, 'PDF', 'Kualitas Peningkatan Perizinan', 'Mandatory', NULL, NULL, '2023-10-08 16:59:57.814065', '2023-10-08 16:59:57.814065');
INSERT INTO public.indicators VALUES (26, 30, 'si', 'PELAKSANA INOVASI DAERAH', '<p>PELAKSANA INOVASI DAERAH</p>', 'PELAKSANA INOVASI DAERAH', 10, 'PDF', 'Jumlah Pendapatan Perkapita', 'Mandatory', NULL, NULL, '2023-10-08 17:00:40.635665', '2023-10-08 17:00:40.635665');
INSERT INTO public.indicators VALUES (27, 31, 'si', 'JEJARING INOVASI', '<p>JEJARING INOVASI</p>', 'JEJARING INOVASI', 10, 'PDF', 'Kualitas Peningkatan Perizinan', 'Mandatory', NULL, NULL, '2023-10-08 17:01:38.270436', '2023-10-08 17:01:38.270436');
INSERT INTO public.indicators VALUES (28, 32, 'si', 'SOSIALISASI INOVASI DAERAH', '<p>SOSIALISASI INOVASI DAERAH</p>', 'SOSIALISASI INOVASI DAERAH', 10, 'PDF', 'Jumlah Peningkatan Investasi', 'Mandatory', NULL, NULL, '2023-10-08 17:02:39.725445', '2023-10-08 17:02:39.725445');
INSERT INTO public.indicators VALUES (29, 33, 'si', 'PEDOMAN TEKNIS INOVASI', '<p>PEDOMAN TEKNIS INOVASI</p>', 'PEDOMAN TEKNIS INOVASI', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Tidak Mandatory', NULL, NULL, '2023-10-08 17:03:16.270739', '2023-10-08 17:03:16.270739');
INSERT INTO public.indicators VALUES (30, 34, 'si', 'KEMUDAHAN INFORMASI LAYANAN', '<p>KEMUDAHAN INFORMASI LAYANAN</p>', 'KEMUDAHAN INFORMASI LAYANAN', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Mandatory', NULL, NULL, '2023-10-08 17:04:12.141037', '2023-10-08 17:04:12.141037');
INSERT INTO public.indicators VALUES (31, 35, 'si', 'KECEPATAN PENCIPTAAN INOVASI', '<p>KECEPATAN PENCIPTAAN INOVASI</p>', 'KECEPATAN PENCIPTAAN INOVASI', 10, 'PDF', 'Visi dan Misi', 'Mandatory', NULL, NULL, '2023-10-08 17:05:13.501215', '2023-10-08 17:05:13.501215');
INSERT INTO public.indicators VALUES (32, 34, 'si', 'KEMUDAHAN PROSES INOVASI', '<p>KEMUDAHAN PROSES INOVASI</p>', 'KEMUDAHAN PROSES INOVASI', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Mandatory', NULL, NULL, '2023-10-08 17:16:53.879176', '2023-10-08 17:16:53.879176');
INSERT INTO public.indicators VALUES (33, 12, 'si', 'PENYELESAIAN PENGADUAN', '<p>PENYELESAIAN PENGADUAN</p>', 'PENYELESAIAN PENGADUAN', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Mandatory', NULL, NULL, '2023-10-08 17:17:28.376423', '2023-10-08 17:17:28.376423');
INSERT INTO public.indicators VALUES (34, 35, 'si', 'ONLINE SISTEM', '<p>ONLINE SISTEM</p>', 'ONLINE SISTEM', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Mandatory', NULL, NULL, '2023-10-08 17:17:53.764423', '2023-10-08 17:17:53.764423');
INSERT INTO public.indicators VALUES (35, 46, 'si', 'REPLIKASI', '<p>REPLIKASI</p>', 'REPLIKASI', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Mandatory', NULL, NULL, '2023-10-08 17:18:17.050367', '2023-10-08 17:18:17.050367');
INSERT INTO public.indicators VALUES (36, 35, 'si', 'PENGGUNAN IT', '<p>PENGGUNAN IT</p>', 'PENGGUNAN IT', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Tidak Mandatory', NULL, NULL, '2023-10-08 17:18:56.782172', '2023-10-08 17:18:56.782172');
INSERT INTO public.indicators VALUES (37, 10, 'si', 'KEMANFAATAN INOVASI', '<p>KEMANFAATAN INOVASI</p>', 'KEMANFAATAN INOVASI', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Tidak Mandatory', NULL, NULL, '2023-10-08 17:19:28.267143', '2023-10-08 17:19:28.267143');
INSERT INTO public.indicators VALUES (38, 37, 'si', 'MONITORING DAN EVALUASI', '<p>MONITORING DAN EVALUASI</p>', 'MONITORING DAN EVALUASI', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Tidak Mandatory', NULL, NULL, '2023-10-08 17:19:53.764295', '2023-10-08 17:19:53.764295');
INSERT INTO public.indicators VALUES (39, 33, 'si', 'KUALITAS INOVASI DAERAH', '<p>KUALITAS INOVASI DAERAH</p>', 'KUALITAS INOVASI DAERAH', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Tidak Mandatory', NULL, NULL, '2023-10-08 17:20:20.139917', '2023-10-08 17:20:20.139917');
INSERT INTO public.indicators VALUES (40, 39, 'si', 'JULAH INOVASI DAERAH', '<p>JULAH INOVASI DAERAH</p>', 'JULAH INOVASI DAERAH', 10, 'PDF', 'Penerapan SIPD (Sistem Informasi Pemerintah)', 'Tidak Mandatory', NULL, NULL, '2023-10-08 17:20:55.529877', '2023-10-08 17:20:55.529877');


--
-- Data for Name: inovasi_indikator; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.inovasi_indikator VALUES (13, 21, 5, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-05 15:23:30.451395', '2023-09-05 15:23:30.451395');
INSERT INTO public.inovasi_indikator VALUES (14, 22, 5, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-05 15:27:45.824785', '2023-09-05 15:27:45.824785');
INSERT INTO public.inovasi_indikator VALUES (15, 23, 5, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-05 15:31:49.624653', '2023-09-05 15:31:49.624653');
INSERT INTO public.inovasi_indikator VALUES (16, 24, 5, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-05 15:33:45.107733', '2023-09-05 15:33:45.107733');
INSERT INTO public.inovasi_indikator VALUES (17, 25, 5, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-05 15:36:15.836096', '2023-09-05 15:36:15.836096');
INSERT INTO public.inovasi_indikator VALUES (23, 30, 5, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 14:16:33.446429', '2023-10-07 14:16:33.446429');
INSERT INTO public.inovasi_indikator VALUES (24, 30, 13, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 14:16:33.446429', '2023-10-07 14:16:33.446429');
INSERT INTO public.inovasi_indikator VALUES (25, 30, 9, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 14:16:33.446429', '2023-10-07 14:16:33.446429');
INSERT INTO public.inovasi_indikator VALUES (26, 31, 20, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (27, 31, 21, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (28, 31, 22, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (29, 31, 23, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (30, 31, 24, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (31, 31, 25, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (32, 31, 26, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (33, 31, 27, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (34, 31, 28, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (35, 31, 29, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (36, 31, 30, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (37, 31, 31, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (38, 31, 32, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (39, 31, 33, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (40, 31, 34, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (41, 31, 35, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (42, 31, 36, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (43, 31, 37, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (44, 31, 38, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (45, 31, 39, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (46, 31, 40, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:26:22.314045', '2023-10-08 21:26:22.314045');
INSERT INTO public.inovasi_indikator VALUES (47, 32, 20, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (48, 32, 21, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (49, 32, 22, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (50, 32, 23, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (51, 32, 24, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (52, 32, 25, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (53, 32, 26, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (54, 32, 27, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (55, 32, 28, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (56, 32, 29, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (57, 32, 30, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (58, 32, 31, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (59, 32, 32, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (60, 32, 33, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (61, 32, 34, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (62, 32, 35, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (63, 32, 36, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (64, 32, 37, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (65, 32, 38, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (66, 32, 39, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (67, 32, 40, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-08 21:53:16.489674', '2023-10-08 21:53:16.489674');
INSERT INTO public.inovasi_indikator VALUES (68, 33, 20, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (69, 33, 21, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (70, 33, 22, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (71, 33, 23, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (72, 33, 24, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (73, 33, 25, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (74, 33, 26, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (75, 33, 27, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (76, 33, 28, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (77, 33, 29, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (78, 33, 30, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (79, 33, 31, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (80, 33, 32, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (81, 33, 33, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (82, 33, 34, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (83, 33, 35, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (84, 33, 36, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (85, 33, 37, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (86, 33, 38, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (87, 33, 39, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (88, 33, 40, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:32.4814', '2023-10-10 19:29:32.4814');
INSERT INTO public.inovasi_indikator VALUES (89, 34, 20, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (90, 34, 21, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (91, 34, 22, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (92, 34, 23, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (93, 34, 24, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (94, 34, 25, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (95, 34, 26, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (96, 34, 27, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (97, 34, 28, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (98, 34, 29, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (99, 34, 30, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (100, 34, 31, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (101, 34, 32, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (102, 34, 33, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (103, 34, 34, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (104, 34, 35, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (105, 34, 36, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (106, 34, 37, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (107, 34, 38, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (108, 34, 39, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (109, 34, 40, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:29:33.551605', '2023-10-10 19:29:33.551605');
INSERT INTO public.inovasi_indikator VALUES (131, 36, 20, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (132, 36, 21, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (133, 36, 22, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (134, 36, 23, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (135, 36, 24, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (136, 36, 25, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (137, 36, 26, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (138, 36, 27, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (139, 36, 28, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (140, 36, 29, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (141, 36, 30, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (142, 36, 31, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (143, 36, 32, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (144, 36, 33, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (145, 36, 34, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (146, 36, 35, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (147, 36, 36, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (148, 36, 37, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (149, 36, 38, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (150, 36, 39, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (151, 36, 40, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-18 22:46:16.619685', '2023-10-18 22:46:16.619685');
INSERT INTO public.inovasi_indikator VALUES (152, 37, 20, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (153, 37, 21, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (154, 37, 22, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (155, 37, 23, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (156, 37, 24, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (157, 37, 25, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (158, 37, 26, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (159, 37, 27, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (160, 37, 28, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (161, 37, 29, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (162, 37, 30, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (163, 37, 31, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (164, 37, 32, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (165, 37, 33, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (166, 37, 34, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (167, 37, 35, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (168, 37, 36, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (169, 37, 37, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (170, 37, 38, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (171, 37, 39, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');
INSERT INTO public.inovasi_indikator VALUES (172, 37, 40, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:05:47.098674', '2023-10-18 23:05:47.098674');


--
-- Data for Name: inovasi_indikator_file; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.inovasi_indikator_file VALUES (5, NULL, 102, 'casdasd', '2023-10-10', 'creator', 'creator', '2023-10-09 00:11:36.046764', '2023-10-09 00:11:36.046764', 'asdasdc', 21, 5);
INSERT INTO public.inovasi_indikator_file VALUES (6, NULL, 104, 'INOVASI-DAERAH/09/10/TEST', '2023-10-09', 'superadmin', 'superadmin', '2023-10-09 00:26:39.989468', '2023-10-09 00:26:39.989468', 'COBA COBA', 32, 40);
INSERT INTO public.inovasi_indikator_file VALUES (7, NULL, 105, 'MONITORING-DAN-EVALUASI/0-9', '2023-10-04', 'superadmin', 'superadmin', '2023-10-09 00:28:21.059624', '2023-10-09 00:28:21.059624', 'MONITORING DAN EVALUASI FILE PENTING BANGET PAKE SEKALI', 31, 38);
INSERT INTO public.inovasi_indikator_file VALUES (8, NULL, 106, 'INOVASI-DAERAH-2/09/10', '2023-10-10', 'superadmin', 'superadmin', '2023-10-09 00:35:17.593249', '2023-10-09 00:35:17.593249', 'COBA KEDUA', 32, 40);
INSERT INTO public.inovasi_indikator_file VALUES (9, NULL, 112, 'Skor Evaluasi', '2023-10-27', 'superadmin', 'superadmin', '2023-10-27 21:48:43.104681', '2023-10-27 21:48:43.104681', 'Skor Evaluasi dan Estimasi', 30, 40);
INSERT INTO public.inovasi_indikator_file VALUES (10, NULL, 113, 'Test Foto Evaluasi', '2023-10-27', 'superadmin', 'superadmin', '2023-10-27 21:54:11.292292', '2023-10-27 21:54:11.292292', 'Test Foto', 36, 40);
INSERT INTO public.inovasi_indikator_file VALUES (11, NULL, 115, 'wefweg', '2023-11-08', 'superadmin', 'superadmin', '2023-11-08 17:33:56.511573', '2023-11-08 17:33:56.511573', 'wegwg', 36, 36);
INSERT INTO public.inovasi_indikator_file VALUES (12, NULL, 116, '2023/11/14-BEN-APIK', '2023-11-14', 'superadmin', 'superadmin', '2023-11-14 21:11:01.249168', '2023-11-14 21:11:01.249168', 'BEN APIK', 36, 39);
INSERT INTO public.inovasi_indikator_file VALUES (13, NULL, 117, 'Sss', '2023-11-15', 'superadmin', 'superadmin', '2023-11-15 13:44:08.005592', '2023-11-15 13:44:08.005592', 'Gsggbw', 36, 31);


--
-- Data for Name: pakta_integritas; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.pakta_integritas VALUES (1, 1, 31, 'creator', 'creator', '2023-07-08 22:56:49.378317', '2023-07-08 22:56:49.378317');
INSERT INTO public.pakta_integritas VALUES (2, 4, 95, 'user', 'user', '2023-10-08 22:49:23.170759', '2023-10-08 22:49:23.170759');
INSERT INTO public.pakta_integritas VALUES (4, 4, 97, 'user', 'user', '2023-10-08 22:58:39.805522', '2023-10-08 22:58:39.805522');
INSERT INTO public.pakta_integritas VALUES (5, 4, 98, 'user', 'user', '2023-10-08 23:12:35.088596', '2023-10-08 23:12:35.088596');


--
-- Data for Name: pemda_indikator; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.pemda_indikator VALUES (76, 26, 6, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (77, 26, 7, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (1, 8, 5, NULL, NULL, NULL, NULL, 'creator', 'creator', '2023-07-11 15:53:32.752579', '2023-07-11 15:53:32.752579', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (2, 9, 6, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-07-28 23:15:18.547325', '2023-07-28 23:15:18.547325', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (6, 10, 6, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-07 19:28:32.617912', '2023-09-07 19:28:32.617912', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (7, 11, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-14 23:31:52.059715', '2023-09-14 23:31:52.059715', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (8, 11, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-14 23:31:52.059715', '2023-09-14 23:31:52.059715', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (9, 11, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-14 23:31:52.059715', '2023-09-14 23:31:52.059715', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (10, 12, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-14 23:37:13.612766', '2023-09-14 23:37:13.612766', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (11, 12, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-14 23:37:13.612766', '2023-09-14 23:37:13.612766', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (12, 12, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-09-14 23:37:13.612766', '2023-09-14 23:37:13.612766', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (13, 13, 12, NULL, NULL, NULL, NULL, 'user', 'user', '2023-09-22 14:12:15.676535', '2023-09-22 14:12:15.676535', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (14, 13, 11, NULL, NULL, NULL, NULL, 'user', 'user', '2023-09-22 14:12:15.676535', '2023-09-22 14:12:15.676535', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (15, 13, 10, NULL, NULL, NULL, NULL, 'user', 'user', '2023-09-22 14:12:15.676535', '2023-09-22 14:12:15.676535', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (16, 14, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:19:53.220375', '2023-10-07 23:19:53.220375', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (17, 14, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:19:53.220375', '2023-10-07 23:19:53.220375', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (18, 14, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:19:53.220375', '2023-10-07 23:19:53.220375', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (19, 14, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:19:53.220375', '2023-10-07 23:19:53.220375', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (20, 14, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:19:53.220375', '2023-10-07 23:19:53.220375', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (21, 15, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:20:35.582334', '2023-10-07 23:20:35.582334', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (22, 15, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:20:35.582334', '2023-10-07 23:20:35.582334', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (23, 15, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:20:35.582334', '2023-10-07 23:20:35.582334', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (24, 15, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:20:35.582334', '2023-10-07 23:20:35.582334', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (25, 15, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:20:35.582334', '2023-10-07 23:20:35.582334', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (26, 16, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:21:56.457228', '2023-10-07 23:21:56.457228', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (27, 16, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:21:56.457228', '2023-10-07 23:21:56.457228', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (28, 16, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:21:56.457228', '2023-10-07 23:21:56.457228', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (29, 16, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:21:56.457228', '2023-10-07 23:21:56.457228', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (30, 16, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:21:56.457228', '2023-10-07 23:21:56.457228', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (31, 17, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:21.468559', '2023-10-07 23:22:21.468559', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (32, 17, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:21.468559', '2023-10-07 23:22:21.468559', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (33, 17, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:21.468559', '2023-10-07 23:22:21.468559', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (34, 17, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:21.468559', '2023-10-07 23:22:21.468559', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (35, 17, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:21.468559', '2023-10-07 23:22:21.468559', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (36, 18, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:53.703302', '2023-10-07 23:22:53.703302', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (37, 18, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:53.703302', '2023-10-07 23:22:53.703302', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (38, 18, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:53.703302', '2023-10-07 23:22:53.703302', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (39, 18, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:53.703302', '2023-10-07 23:22:53.703302', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (40, 18, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:22:53.703302', '2023-10-07 23:22:53.703302', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (41, 19, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:10.748699', '2023-10-07 23:23:10.748699', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (42, 19, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:10.748699', '2023-10-07 23:23:10.748699', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (43, 19, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:10.748699', '2023-10-07 23:23:10.748699', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (44, 19, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:10.748699', '2023-10-07 23:23:10.748699', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (45, 19, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:10.748699', '2023-10-07 23:23:10.748699', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (46, 20, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:26.647723', '2023-10-07 23:23:26.647723', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (47, 20, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:26.647723', '2023-10-07 23:23:26.647723', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (48, 20, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:26.647723', '2023-10-07 23:23:26.647723', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (49, 20, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:26.647723', '2023-10-07 23:23:26.647723', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (50, 20, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:26.647723', '2023-10-07 23:23:26.647723', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (51, 21, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:46.838412', '2023-10-07 23:23:46.838412', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (52, 21, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:46.838412', '2023-10-07 23:23:46.838412', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (53, 21, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:46.838412', '2023-10-07 23:23:46.838412', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (54, 21, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:46.838412', '2023-10-07 23:23:46.838412', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (55, 21, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:23:46.838412', '2023-10-07 23:23:46.838412', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (56, 22, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:24:10.281383', '2023-10-07 23:24:10.281383', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (57, 22, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:24:10.281383', '2023-10-07 23:24:10.281383', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (58, 22, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:24:10.281383', '2023-10-07 23:24:10.281383', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (59, 22, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:24:10.281383', '2023-10-07 23:24:10.281383', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (60, 22, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:24:10.281383', '2023-10-07 23:24:10.281383', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (61, 23, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:01.497347', '2023-10-07 23:36:01.497347', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (62, 23, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:01.497347', '2023-10-07 23:36:01.497347', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (63, 23, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:01.497347', '2023-10-07 23:36:01.497347', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (64, 23, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:01.497347', '2023-10-07 23:36:01.497347', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (65, 23, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:01.497347', '2023-10-07 23:36:01.497347', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (66, 24, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:25.758007', '2023-10-07 23:36:25.758007', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (67, 24, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:25.758007', '2023-10-07 23:36:25.758007', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (68, 24, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:25.758007', '2023-10-07 23:36:25.758007', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (69, 24, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:25.758007', '2023-10-07 23:36:25.758007', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (70, 24, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:36:25.758007', '2023-10-07 23:36:25.758007', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (71, 25, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:37:08.036232', '2023-10-07 23:37:08.036232', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (72, 25, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:37:08.036232', '2023-10-07 23:37:08.036232', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (73, 25, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:37:08.036232', '2023-10-07 23:37:08.036232', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (74, 25, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:37:08.036232', '2023-10-07 23:37:08.036232', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (75, 25, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-07 23:37:08.036232', '2023-10-07 23:37:08.036232', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (78, 26, 5, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (79, 26, 8, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (80, 26, 9, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (81, 26, 10, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (82, 26, 11, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (83, 26, 12, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (84, 26, 19, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (85, 26, 14, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (86, 26, 13, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (87, 26, 15, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (88, 26, 16, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (89, 26, 17, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (90, 26, 18, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 08:52:53.355999', '2023-10-09 08:52:53.355999', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (91, 27, 6, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (92, 27, 7, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (93, 27, 5, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (94, 27, 8, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (95, 27, 9, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (96, 27, 10, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (97, 27, 11, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (98, 27, 12, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (99, 27, 19, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (100, 27, 14, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (101, 27, 13, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (102, 27, 15, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (103, 27, 16, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (104, 27, 17, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (105, 27, 18, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-09 09:00:58.512301', '2023-10-09 09:00:58.512301', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (106, 28, 6, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (107, 28, 7, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (108, 28, 5, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (109, 28, 8, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (110, 28, 9, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (111, 28, 10, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (112, 28, 11, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (113, 28, 12, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (114, 28, 19, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (115, 28, 14, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (116, 28, 13, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (117, 28, 15, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (118, 28, 16, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (119, 28, 17, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (120, 28, 18, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.182938', '2023-10-10 19:12:28.182938', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (121, 29, 6, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (122, 29, 7, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (123, 29, 5, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (124, 29, 8, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (125, 29, 9, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (126, 29, 10, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (127, 29, 11, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (128, 29, 12, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (129, 29, 19, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (130, 29, 14, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (131, 29, 13, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (132, 29, 15, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (133, 29, 16, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (134, 29, 17, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (135, 29, 18, NULL, NULL, NULL, NULL, 'user55', 'user55', '2023-10-10 19:12:28.254005', '2023-10-10 19:12:28.254005', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (136, 30, 6, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (137, 30, 7, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (138, 30, 5, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (139, 30, 8, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (140, 30, 9, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (141, 30, 10, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (142, 30, 11, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (143, 30, 12, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (144, 30, 19, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (145, 30, 14, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (146, 30, 13, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (147, 30, 15, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (148, 30, 16, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (149, 30, 17, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (150, 30, 18, NULL, NULL, NULL, NULL, 'user22', 'user22', '2023-10-18 23:02:03.050889', '2023-10-18 23:02:03.050889', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (151, 31, 6, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (152, 31, 7, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (153, 31, 5, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (154, 31, 8, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (155, 31, 9, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (156, 31, 10, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (157, 31, 11, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (158, 31, 12, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (159, 31, 19, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (160, 31, 14, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (161, 31, 13, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (162, 31, 15, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (163, 31, 16, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (164, 31, 17, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);
INSERT INTO public.pemda_indikator VALUES (165, 31, 18, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.755261', '2023-10-22 16:52:04.755261', NULL, NULL, NULL, NULL);


--
-- Data for Name: pemda_indikator_file; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.pemda_indikator_file VALUES (15, 12, 94, 'yhygy', '2023-10-07', 'Wsws', 'user', 'user', '2023-10-07 22:10:28.51555', '2023-10-07 22:10:28.51555', 12, 10);
INSERT INTO public.pemda_indikator_file VALUES (13, 12, 90, 'Halo/01/024/23', '2023-10-06', 'Halo', 'superadmin', 'superadmin', '2023-10-06 14:46:19.527395', '2023-10-06 14:46:19.527395', 12, 10);
INSERT INTO public.pemda_indikator_file VALUES (14, 15, 91, 'Nnn', '2023-10-06', 'Ggjj', 'superadmin', 'superadmin', '2023-10-06 18:24:07.576774', '2023-10-06 18:24:07.576774', 13, 10);
INSERT INTO public.pemda_indikator_file VALUES (11, 2, 47, 'Perbup-18-Tahun-2020-Rencana-Aksi-Daerah-Penyediaan-Air-Minum-Penyehatan-Lingkungan-Kabupaten-Asahan-Tahun-2017-2021', '2023-08-08', 'Perbup-18-Tahun-2020-Rencana-Aksi-Daerah-Penyediaan-Air-Minum-Penyehatan-Lingkungan-Kabupaten-Asahan-Tahun-2017-2021', 'superadmin', 'superadmin', '2023-08-07 21:02:53.519516', '2023-08-07 21:02:53.519516', 9, 6);
INSERT INTO public.pemda_indikator_file VALUES (1, 1, 33, 'Qanun Kabupaten Aceh barat Nomor 5 Tahun 2020', '2020-12-28', 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'creator', 'creator', '2023-07-24 23:21:31.852614', '2023-07-24 23:21:31.852614', 8, 5);
INSERT INTO public.pemda_indikator_file VALUES (2, 1, 34, 'Qanun Kabupaten Aceh barat Nomor 5 Tahun 2020', '2020-12-28', 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'creator', 'creator', '2023-07-24 23:22:04.387104', '2023-07-24 23:22:04.387104', 8, 5);
INSERT INTO public.pemda_indikator_file VALUES (3, 1, 35, 'Qanun Kabupaten Aceh barat Nomor 5 Tahun 2020', '2020-12-28', 'Perubahan Atas Qanun Kabupaten Aceh Barat Nomor 1 Tahun 2018 tentang Rencana Pembangunan Jangka Menengah Kabupaten Aceh Barat Tahun 2017-2022', 'creator', 'creator', '2023-07-24 23:22:05.599435', '2023-07-24 23:22:05.599435', 8, 5);
INSERT INTO public.pemda_indikator_file VALUES (16, NULL, 100, 'asdas', '2023-10-10', 'dasd', 'creator', 'creator', '2023-10-08 23:44:19.648262', '2023-10-08 23:44:19.648262', 13, 10);
INSERT INTO public.pemda_indikator_file VALUES (17, NULL, 101, '08/10/2023/TESRT', '2023-10-08', 'TEST COBA COBA BERHADIAH', 'user', 'user', '2023-10-08 23:58:10.203499', '2023-10-08 23:58:10.203499', 13, 19);
INSERT INTO public.pemda_indikator_file VALUES (19, NULL, 107, 'Tes', '2023-10-11', 'Testes', 'superadmin', 'superadmin', '2023-10-11 13:53:45.011977', '2023-10-11 13:53:45.011977', 29, 19);
INSERT INTO public.pemda_indikator_file VALUES (20, NULL, 114, 'qwfqwf', '2023-11-11', 'Qwqwq', 'superadmin', 'superadmin', '2023-11-08 17:32:21.426996', '2023-11-08 17:32:21.426996', 30, 16);


--
-- Data for Name: preview_review_inovasi_daerah; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: profil_pemda; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.profil_pemda VALUES (8, 1, NULL, 'Dinas Tenaga Kerja dan Transmigrasi Kabupaten Sorong', 'Bappeda Litbang', NULL, NULL, NULL, NULL, 'creator', 'superadmin', '2023-07-11 15:53:32.247723', '2023-10-07 23:13:23.78096', NULL, 'Ya', NULL);
INSERT INTO public.profil_pemda VALUES (2, 1, 30, 'Dinas Peternakan dan Kesehatan hewan Kabupaten Sorong', 'diluar', 'Jl. Surabaya', 'surabaya@gmail.com', '0838416513', 'adminsurabaya', 'creator', 'superadmin', '2023-06-30 00:30:52.684642', '2023-10-12 22:25:46.708', NULL, 'Ya', NULL);
INSERT INTO public.profil_pemda VALUES (12, 2, 82, 'Sekretariat DPRD Kabupaten Sorong', 'Badan Litbang', 'alamat', 'superadmin@dummy.com', '812343', 'Super Admin', 'superadmin', 'superadmin', '2023-09-14 23:37:13.577834', '2023-10-07 22:30:00.114724', NULL, 'Ya', NULL);
INSERT INTO public.profil_pemda VALUES (11, 2, 81, 'Badan Kepegawaian, Pendidikan, dan Pelatihan Daerah Kabupaten Sorong', 'Badan Litbang', 'bb', 'superadmin@dummy.com', '87', 'Super Admin', 'superadmin', 'superadmin', '2023-09-14 23:31:52.023455', '2023-10-07 22:31:04.544436', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (10, 2, 73, 'Badan Pengelola Keuangan dan Aset DAerah Kabupaten Sorong', 'Badan Litbang', 'Test', 'superadmin@dummy.com', '087857868345', 'Super Admin', 'superadmin', 'superadmin', '2023-09-07 19:28:32.566763', '2023-10-07 22:31:46.172641', NULL, 'Ya', NULL);
INSERT INTO public.profil_pemda VALUES (9, 2, 50, 'Dinas Pemberdayaan Masyarakat KAmpung Kabupaten Sorong', 'Bappeda Litbang', 'Jl. Tegal', 'superadmin@dummy.com', '98739923832', 'Super Admin', 'superadmin', 'superadmin', '2023-07-28 23:15:18.142371', '2023-10-07 22:33:25.124818', NULL, 'Ya', 'Tidak');
INSERT INTO public.profil_pemda VALUES (7, 1, NULL, 'Dinas Perpustakaan Kabupaten Sorong', 'Bappeda Litbang', NULL, NULL, NULL, NULL, 'creator', 'superadmin', '2023-07-11 15:52:35.518053', '2023-10-07 23:13:55.102461', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (6, 1, NULL, 'Dinas Kesehatan Kabupaten Sorong', 'diluar', NULL, NULL, NULL, NULL, 'creator', 'superadmin', '2023-07-11 15:48:19.130683', '2023-10-07 23:14:12.351089', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (4, 1, NULL, 'Dinas Sosial Kabupaten Sorong', 'diluar', NULL, NULL, NULL, NULL, 'creator', 'superadmin', '2023-07-11 15:43:07.107336', '2023-10-07 23:16:07.064164', NULL, 'Tidak', 'Ya');
INSERT INTO public.profil_pemda VALUES (3, 1, NULL, 'Dinas PErtanian, Tanaman Pangan, Hortikultura, dan Perkebunan, Kabupaten Sorong', 'diluar', NULL, NULL, NULL, NULL, 'creator', 'superadmin', '2023-07-11 15:41:36.74034', '2023-10-07 23:17:01.426094', NULL, 'Ya', 'Ya');
INSERT INTO public.profil_pemda VALUES (18, 2, NULL, 'Dinas Komunikasi, Informatika, Staistik, dan persandian Kabupaten Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:22:53.675368', '2023-10-07 23:22:53.675368', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (20, 2, NULL, 'Kantor Pertanahan Kabupaten Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:23:26.62545', '2023-11-08 17:44:37.001119', NULL, 'Tidak', 'Ya');
INSERT INTO public.profil_pemda VALUES (5, 1, NULL, 'Dinas pengedalian Penduduk, Keluarga Berencana, Pemberdayaan, Perempuan, dan Perlindungan Anak Kabupaten Sorong', 'Bappeda Litbang', NULL, NULL, NULL, NULL, 'creator', 'superadmin', '2023-07-11 15:45:46.975766', '2023-11-17 21:06:26.340056', NULL, 'Ya', NULL);
INSERT INTO public.profil_pemda VALUES (13, 4, 87, 'User', 'diluar', 'User', 'user@dummy.com', '123', 'User', 'user', 'user', '2023-09-22 14:12:15.638341', '2023-09-22 14:12:15.676535', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (14, 2, NULL, 'Dinas Parwisata, Pemuda, dan Olahraga Kabupaten Sorong', 'Badan Litbang', 'Sorong', 'superadmin@dummy.com', '081', 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:19:53.184777', '2023-10-07 23:19:53.184777', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (28, 50, NULL, 'Kabupaten Sorong', 'Bappeda Litbang', 'Jln. Sorong Klomono Aimas II Km.24', 'user55@gmail.com', NULL, 'user55', 'user55', 'user55', '2023-10-10 19:12:28.109535', '2023-10-11 13:35:19.376357', NULL, 'Ya', NULL);
INSERT INTO public.profil_pemda VALUES (29, 50, NULL, 'Kabupaten Sorong', 'diluar', 'Jl. Sorong-Klamono KM.24 ', 'user55@gmail.com', NULL, 'user55', 'user55', 'superadmin', '2023-10-10 19:12:28.228032', '2023-10-11 13:55:41.416566', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (19, 2, NULL, 'Samsat Kabupaten Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:23:10.730673', '2023-10-07 23:23:10.730673', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (25, 2, NULL, 'Stasiun Geometeorologi dan Geofisika Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:37:08.000589', '2023-11-23 22:00:26.50171', NULL, 'Ya', 'Ya');
INSERT INTO public.profil_pemda VALUES (21, 2, NULL, 'Kantor Kementrian Agama Kabupaten Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:23:46.800242', '2023-10-07 23:23:46.800242', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (30, 46, 110, 'Test Pemda user 22', 'Badan Litbang', 'Jl. Pemda OPD User 22', 'User22@gmail.com', '08573746342', 'user22', 'user22', 'user22', '2023-10-18 23:02:02.972169', '2023-10-18 23:02:03.050889', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (24, 2, NULL, 'PT Andriyani Jaya Abadi', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:36:25.733377', '2023-10-07 23:36:25.733377', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (15, 2, NULL, 'Dinas Pekerjaan Umum dan penataan Ruang Kabupaten Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:20:35.545604', '2023-10-07 23:20:35.545604', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (16, 2, NULL, 'Dinas Perdagangan, Koperasi, Usaha kecil dan menengah Kabupaten Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:21:56.432986', '2023-10-07 23:21:56.432986', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (17, 2, NULL, 'Dinas Penanaman Modal Pelayanan Terpadu Satu Pintu', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:22:21.448334', '2023-10-07 23:22:21.448334', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (31, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'superadmin', 'superadmin', '2023-10-22 16:52:04.578628', '2023-10-22 16:52:04.578628', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (26, 46, NULL, NULL, NULL, NULL, 'User22@gmail.com', NULL, 'user22', 'user22', 'user22', '2023-10-09 08:52:53.308341', '2023-10-09 08:52:53.308341', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (27, 46, NULL, NULL, NULL, NULL, 'User22@gmail.com', NULL, 'user22', 'user22', 'user22', '2023-10-09 09:00:58.480217', '2023-10-09 09:00:58.480217', NULL, NULL, NULL);
INSERT INTO public.profil_pemda VALUES (22, 2, NULL, 'Kepolisian Resor Kabupaten Sorong', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:24:10.2613', '2023-11-08 17:47:29.895204', NULL, 'Ya', 'Tidak');
INSERT INTO public.profil_pemda VALUES (23, 2, NULL, 'PLN ULP Aimas', NULL, NULL, 'superadmin@dummy.com', NULL, 'Super Admin', 'superadmin', 'superadmin', '2023-10-07 23:36:01.447986', '2023-11-17 22:36:54.722554', NULL, 'Tidak', 'Ya');
INSERT INTO public.profil_pemda VALUES (1, 1, NULL, 'Dinas Perikanan Kabupaten Sorong', 'diluar', 'Jl. Surabaya', 'surabaya@gmail.com', '0838416513', 'adminsurabaya', 'creator', 'superadmin', '2023-06-30 00:30:21.110847', '2023-11-20 14:26:55.706789', NULL, 'Ya', 'Tidak');


--
-- Data for Name: regional_apparatus; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.regional_apparatus VALUES (1, 'Kabupaten Sorong', '2023-06-29 21:56:41.197228', 'creator', '2023-10-07 23:37:45.493224', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (10, '	Stasiun Geometeorologi dan Geofisika Sorong', '2023-10-09 01:13:33.948991', 'superadmin', '2023-10-09 01:13:33.948991', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (11, '	PT Andriyani Jaya Abadi', '2023-10-09 01:13:49.828421', 'superadmin', '2023-10-09 01:13:49.828421', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (12, '	PLN ULP Aimas', '2023-10-09 01:14:10.625132', 'superadmin', '2023-10-09 01:14:10.625132', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (13, 'Kepolisian Resor Kabupaten Sorong', '2023-10-09 01:14:40.232129', 'superadmin', '2023-10-09 01:14:40.232129', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (14, 'Kantor Kementrian Agama Kabupaten Sorong', '2023-10-09 01:14:56.834181', 'superadmin', '2023-10-09 01:14:56.834181', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (15, 'Kantor Pertanahan Kabupaten Sorong', '2023-10-09 01:15:20.814997', 'superadmin', '2023-10-09 01:15:20.814997', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (16, 'Samsat Kabupaten Sorong', '2023-10-09 01:15:37.656485', 'superadmin', '2023-10-09 01:15:37.656485', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (17, 'Dinas Komunikasi, Informatika, Staistik, dan persandian Kabupaten Sorong', '2023-10-09 01:15:55.232254', 'superadmin', '2023-10-09 01:15:55.232254', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (18, 'Dinas Penanaman Modal Pelayanan Terpadu Satu Pintu', '2023-10-09 01:16:10.352312', 'superadmin', '2023-10-09 01:16:10.352312', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (19, 'Dinas Perdagangan, Koperasi, Usaha kecil dan menengah Kabupaten Sorong', '2023-10-09 01:16:49.7073', 'superadmin', '2023-10-09 01:16:49.7073', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (20, 'Dinas Pekerjaan Umum dan penataan Ruang Kabupaten Sorong', '2023-10-09 01:17:08.635489', 'superadmin', '2023-10-09 01:17:08.635489', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (21, 'Dinas Parwisata, Pemuda, dan Olahraga Kabupaten Sorong', '2023-10-09 01:17:24.355094', 'superadmin', '2023-10-09 01:17:24.355094', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (22, 'Sekretariat DPRD Kabupaten Sorong', '2023-10-09 01:17:52.187556', 'superadmin', '2023-10-09 01:17:52.187556', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (23, 'Badan Kepegawaian, Pendidikan, dan Pelatihan Daerah Kabupaten Sorong', '2023-10-09 01:18:10.746182', 'superadmin', '2023-10-09 01:18:10.746182', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (24, 'Badan Pengelola Keuangan dan Aset DAerah Kabupaten Sorong', '2023-10-09 01:18:28.646824', 'superadmin', '2023-10-09 01:18:28.646824', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (25, 'Dinas Pemberdayaan Masyarakat KAmpung Kabupaten Sorong', '2023-10-09 01:21:48.498577', 'superadmin', '2023-10-09 01:21:48.498577', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (26, 'Dinas Tenaga Kerja dan Transmigrasi Kabupaten Sorong', '2023-10-09 01:22:09.183993', 'superadmin', '2023-10-09 01:22:09.183993', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (27, 'Dinas Perpustakaan Kabupaten Sorong', '2023-10-09 01:22:24.545312', 'superadmin', '2023-10-09 01:22:24.545312', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (28, 'Dinas Kesehatan Kabupaten Sorong', '2023-10-09 01:22:43.645352', 'superadmin', '2023-10-09 01:22:43.645352', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (29, 'Dinas pengedalian Penduduk, Keluarga Berencana, Pemberdayaan, Perempuan, dan Perlindungan Anak Kabupaten Sorong', '2023-10-09 01:22:59.766192', 'superadmin', '2023-10-09 01:22:59.766192', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (30, 'Dinas Sosial Kabupaten Sorong', '2023-10-09 01:23:17.898104', 'superadmin', '2023-10-09 01:23:17.898104', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (32, 'Dinas Peternakan dan Kesehatan hewan Kabupaten Sorong', '2023-10-09 01:24:09.863141', 'superadmin', '2023-10-09 01:24:09.863141', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (33, 'Dinas Perikanan Kabupaten Sorong', '2023-10-09 01:24:23.958015', 'superadmin', '2023-10-09 01:24:23.958015', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (31, 'Dinas Pertanian, Tanaman Pangan, Hortikultura, dan Perkebunan, Kabupaten Sorong', '2023-10-09 01:23:39.647067', 'superadmin', '2023-10-09 01:25:25.034979', 'user');
INSERT INTO public.regional_apparatus VALUES (35, 'Badan Pengelola Pajak dan Retribusi Daerah Kabupaten Sorong', '2023-10-09 08:57:10.20973', 'user22', '2023-10-09 08:57:10.20973', 'user22');
INSERT INTO public.regional_apparatus VALUES (36, 'DINAS PERHUBUNGAN KABUPATEN SORONG', '2023-10-09 08:57:13.963176', 'user22', '2023-10-09 08:57:13.963176', 'user22');
INSERT INTO public.regional_apparatus VALUES (38, 'Dinas Ketahanan Pangan  Kabupaten Sorong', '2023-10-09 08:57:51.52133', 'user22', '2023-10-09 08:57:51.52133', 'user22');
INSERT INTO public.regional_apparatus VALUES (37, 'Badan Kesatuan Bangsa Dan Politik', '2023-10-09 08:57:28.952193', 'user22', '2023-10-09 08:58:53.442093', 'user');
INSERT INTO public.regional_apparatus VALUES (39, 'Sekretariat Daerah Kabupaten Sorong', '2023-10-09 18:24:19.227322', 'superadmin', '2023-10-09 18:24:19.227322', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (40, 'Inspektorat Daerah Kabupaten Sorong', '2023-10-09 18:24:55.725472', 'superadmin', '2023-10-09 18:24:55.725472', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (41, 'Dinas pendidikan dan Kebudayaan', '2023-10-09 18:25:32.527778', 'superadmin', '2023-10-09 18:25:32.527778', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (42, 'Dinas perumahan dan kawasan pemukiman', '2023-10-09 18:27:09.084068', 'superadmin', '2023-10-09 18:27:25.473651', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (43, 'Satuan polisi pamong praja', '2023-10-09 18:27:55.403178', 'superadmin', '2023-10-09 18:28:30.517093', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (44, 'Dinas lingkungan hidup', '2023-10-09 18:30:00.253874', 'superadmin', '2023-10-09 18:30:00.253874', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (45, 'Dinas kependudukan dan pencacatan sipil', '2023-10-09 18:30:42.310614', 'superadmin', '2023-10-09 18:30:42.310614', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (46, 'Dinas Pariwisata, pemuda dan olahraga', '2023-10-09 18:32:29.57126', 'superadmin', '2023-10-09 18:34:47.32071', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (47, 'Dinas perpustakaan dan kearsipan', '2023-10-09 18:35:49.099541', 'superadmin', '2023-10-09 18:35:49.099541', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (48, 'Dinas perindustrian dan perdagangan', '2023-10-09 18:36:34.314998', 'superadmin', '2023-10-09 18:36:34.314998', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (49, 'Badan perencanaan, penelitian dan pengembangan  ', '2023-10-09 18:37:58.519197', 'superadmin', '2023-10-09 18:37:58.519197', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (50, 'Badan pengelola keuangan dan aset daerah', '2023-10-09 18:38:25.653345', 'superadmin', '2023-10-09 18:38:25.653345', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (51, 'Badan pengelola pajak dan restribusi daerah', '2023-10-09 18:38:53.466894', 'superadmin', '2023-10-09 18:38:53.466894', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (52, 'Badan kepegawaian, pendidikan, dan pelatihan daerah', '2023-10-09 18:39:38.120312', 'superadmin', '2023-10-09 18:39:38.120312', 'superadmin');
INSERT INTO public.regional_apparatus VALUES (53, 'Badan penanggulangan bencana daerah kabupaten sorong', '2023-10-09 18:40:06.647606', 'superadmin', '2023-10-09 18:40:06.647606', 'superadmin');


--
-- Data for Name: regions; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.regions VALUES (1, 'Surabaya', '2023-03-24 15:53:51.210695', '2023-03-24 15:53:51.210695');
INSERT INTO public.regions VALUES (2, 'malang', '2023-03-29 22:19:50.334482', '2023-03-29 22:19:50.334482');
INSERT INTO public.regions VALUES (3, 'paupa barat', '2023-06-26 13:47:44.902423', '2023-06-26 13:47:44.902423');
INSERT INTO public.regions VALUES (4, 'halo', '2023-06-27 20:25:41.063531', '2023-06-27 20:25:47.38908');
INSERT INTO public.regions VALUES (5, 'test', '2023-06-27 20:42:36.264347', '2023-06-27 20:42:36.264347');
INSERT INTO public.regions VALUES (6, 'kabupaten', '2023-07-07 14:27:44.591751', '2023-07-07 14:27:44.591751');
INSERT INTO public.regions VALUES (7, 'pp', '2023-07-23 20:05:14.968144', '2023-07-23 20:05:14.968144');
INSERT INTO public.regions VALUES (8, 'papapa', '2023-09-07 15:35:28.329142', '2023-09-07 15:35:28.329142');
INSERT INTO public.regions VALUES (9, '', '2023-09-15 07:04:02.079862', '2023-09-15 07:04:02.079862');


--
-- Data for Name: review_inovasi_daerah; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.review_inovasi_daerah VALUES (48, 667344328205, 50, 'Accept', 30, 'superadmin', 'superadmin', '2023-10-07 14:16:33.468346', '2023-10-27 21:49:06.469056');
INSERT INTO public.review_inovasi_daerah VALUES (22, 4, 102, 'Accept', 22, 'superadmin', 'superadmin', '2023-09-08 08:41:58.20612', '2023-11-08 17:40:19.169711');
INSERT INTO public.review_inovasi_daerah VALUES (49, 197318824321, 102, 'Accept', 31, 'superadmin', 'superadmin', '2023-10-08 21:26:22.335256', '2023-11-08 17:40:41.974181');
INSERT INTO public.review_inovasi_daerah VALUES (51, 711606765706, 50, 'Rejected', 33, 'user55', 'superadmin', '2023-10-10 19:29:32.585092', '2023-11-18 00:10:36.306176');
INSERT INTO public.review_inovasi_daerah VALUES (52, 990037905717, 50, 'Accept', 34, 'user55', 'superadmin', '2023-10-10 19:29:33.563973', '2023-10-12 23:04:17.952981');
INSERT INTO public.review_inovasi_daerah VALUES (27, 1, 50, 'Accept', 24, 'superadmin', 'superadmin', '2023-09-08 08:41:58.20612', '2023-09-08 08:41:58.20612');
INSERT INTO public.review_inovasi_daerah VALUES (28, 2, 50, 'Pending', 25, 'superadmin', 'superadmin', '2023-09-08 08:41:58.20612', '2023-09-08 08:41:58.20612');
INSERT INTO public.review_inovasi_daerah VALUES (26, 4, 50, 'Accept', 23, 'superadmin', 'superadmin', '2023-09-08 08:41:58.20612', '2023-09-08 08:41:58.20612');
INSERT INTO public.review_inovasi_daerah VALUES (25, 3, 102, 'Rejected', 22, 'superadmin', 'superadmin', '2023-09-08 08:41:58.20612', '2023-09-08 08:41:58.20612');
INSERT INTO public.review_inovasi_daerah VALUES (24, 2, 105, 'Pending', 21, 'superadmin', 'superadmin', '2023-09-08 08:41:58.20612', '2023-09-08 08:41:58.20612');
INSERT INTO public.review_inovasi_daerah VALUES (54, 480037086443, 50, 'Pending', 36, 'superadmin', 'superadmin', '2023-10-18 22:46:16.646164', '2023-10-18 22:46:16.646164');
INSERT INTO public.review_inovasi_daerah VALUES (55, 384384208086, 50, 'Pending', 37, 'user22', 'user22', '2023-10-18 23:05:47.123582', '2023-10-18 23:05:47.123582');
INSERT INTO public.review_inovasi_daerah VALUES (50, 837188348329, 50, 'Accept', 32, 'superadmin', 'superadmin', '2023-10-08 21:53:16.509015', '2023-10-20 16:20:50.990345');


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.roles VALUES (1, 'super creator', '2023-03-23 12:27:51.051357', '2023-03-23 12:27:51.051357', 'y', 'y', 'super creator');
INSERT INTO public.roles VALUES (2, 'Super Admin', '2023-03-23 12:27:51.051357', '2023-03-23 12:27:51.051357', 't', 'y', 'Super Admin');
INSERT INTO public.roles VALUES (11, 'OPD', '2023-03-24 08:43:15.557066', '2023-03-24 08:43:15.557066', 't', 't', 'OPD');
INSERT INTO public.roles VALUES (12, 'Viewer', '2023-03-24 08:43:15.557066', '2023-03-24 08:43:15.557066', 't', 't', 'Viewer');
INSERT INTO public.roles VALUES (13, 'Verifikator ', '2023-03-24 08:43:15.557066', '2023-03-24 08:43:15.557066', 't', 't', 'Verifikator ');
INSERT INTO public.roles VALUES (8, 'User', '2023-03-23 21:04:59.468471', '2023-03-23 21:04:59.468471', 't', 't', 'OPD user/admin');
INSERT INTO public.roles VALUES (9, 'Admin', '2023-03-24 08:43:15.557066', '2023-03-24 08:43:15.557066', 't', 't', 'Admin distrik/kecamatan');
INSERT INTO public.roles VALUES (10, 'Pemda', '2023-03-24 08:43:15.557066', '2023-03-24 08:43:15.557066', 't', 't', 'Admin desa/kampung');


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.settings VALUES (1, 'whitelisted_file', '.pdf,.xlsx,.docx,.png,.jpg,.jpeg', '2023-03-23 12:27:51.051357', '2023-03-23 12:27:51.051357', 'Tambahkan simbol ''.'' diawal tipe file dan pisahkan menggunakan simbol '',''. Contoh: ''.png,.pdf,.jpeg''');
INSERT INTO public.settings VALUES (2, 'file_max_size', '3', '2023-03-23 12:27:51.051357', '2023-03-23 12:27:51.051357', 'Masukkan angka dalam satuan MB');
INSERT INTO public.settings VALUES (4, 'konfigurasi', 'nilai', '2023-09-14 22:02:40.894208', '2023-09-14 22:05:22.215521', 'helper');


--
-- Data for Name: siagas_migrations; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.siagas_migrations VALUES (1, 1678180735650, 'CreateRoleTable1678180735650');
INSERT INTO public.siagas_migrations VALUES (2, 1678225831756, 'CreateUserTable1678225831756');
INSERT INTO public.siagas_migrations VALUES (3, 1678226927035, 'CreateGovernmentSectorTable1678226927035');
INSERT INTO public.siagas_migrations VALUES (4, 1678331371930, 'CreateRegionTable1678331371930');
INSERT INTO public.siagas_migrations VALUES (5, 1678344294279, 'CreateIndicatorTable1678344294279');
INSERT INTO public.siagas_migrations VALUES (6, 1678345361293, 'CreateIndicatorScaleTable1678345361293');
INSERT INTO public.siagas_migrations VALUES (7, 1678519684438, 'AddIsCreatorColumnTable1678519684438');
INSERT INTO public.siagas_migrations VALUES (8, 1678653781946, 'CreateRegionalApparatusTable1678653781946');
INSERT INTO public.siagas_migrations VALUES (9, 1678658467059, 'AddEmailColumnToUserTable1678658467059');
INSERT INTO public.siagas_migrations VALUES (10, 1678691020809, 'SeedingInitialData1678691020809');
INSERT INTO public.siagas_migrations VALUES (11, 1678697193536, 'CreateAreaTable1678697193536');
INSERT INTO public.siagas_migrations VALUES (12, 1678757268739, 'CreateUPTDTable1678757268739');
INSERT INTO public.siagas_migrations VALUES (13, 1678762165098, 'CreateClusterTable1678762165098');
INSERT INTO public.siagas_migrations VALUES (14, 1678763517930, 'CreateClusterDetailTable1678763517930');
INSERT INTO public.siagas_migrations VALUES (15, 1678778288144, 'CreateSettingTable1678778288144');
INSERT INTO public.siagas_migrations VALUES (16, 1678783021467, 'CreateFileTable1678783021467');
INSERT INTO public.siagas_migrations VALUES (17, 1678783581974, 'AddHelperColumnToSettingTable1678783581974');
INSERT INTO public.siagas_migrations VALUES (18, 1678783791941, 'AddDataToSettingTable1678783791941');
INSERT INTO public.siagas_migrations VALUES (19, 1678830821842, 'CreateTuxedoTable1678830821842');
INSERT INTO public.siagas_migrations VALUES (20, 1678868516337, 'CreateAnnouncementTable1678868516337');
INSERT INTO public.siagas_migrations VALUES (21, 1678958051471, 'CreateFaqTable1678958051471');
INSERT INTO public.siagas_migrations VALUES (22, 1679016247399, 'CreateDocumentCategoryTable1679016247399');
INSERT INTO public.siagas_migrations VALUES (23, 1679019262604, 'CreateDocumentTable1679019262604');
INSERT INTO public.siagas_migrations VALUES (24, 1679578372406, 'AddIsSuperAdminColumnToRoleTable1679578372406');
INSERT INTO public.siagas_migrations VALUES (25, 1679604402590, 'CreateGovernmentInnovationTable1679604402590');
INSERT INTO public.siagas_migrations VALUES (26, 1679642508013, 'CreateGovernmentProfileTable1679642508013');
INSERT INTO public.siagas_migrations VALUES (27, 1680645415390, 'AddDeadlineColumnToGovernmentSectorTable1680645415390');
INSERT INTO public.siagas_migrations VALUES (29, 1682917269807, 'UpdateIndicatorTable1682917269807');
INSERT INTO public.siagas_migrations VALUES (30, 1682925034301, 'CreateGovernmentIndicatorTable1682925034301');
INSERT INTO public.siagas_migrations VALUES (31, 1688052478794, 'CreateTimPenilaianTable1688052478794');
INSERT INTO public.siagas_migrations VALUES (32, 1688056396617, 'CreateProfilePemdaTable1688056396617');
INSERT INTO public.siagas_migrations VALUES (33, 1688058282677, 'AddNamaPemdaToUserTable1688058282677');
INSERT INTO public.siagas_migrations VALUES (34, 1688115174377, 'ChangeIndicatorTable1688115174377');
INSERT INTO public.siagas_migrations VALUES (35, 1688786606530, 'CreatePaktaIntegritasTable1688786606530');
INSERT INTO public.siagas_migrations VALUES (36, 1688833567130, 'UpdateGovernmentInnovationTable1688833567130');
INSERT INTO public.siagas_migrations VALUES (37, 1689061205806, 'CreatePemdaIndikatorTable1689061205806');
INSERT INTO public.siagas_migrations VALUES (38, 1689062368585, 'CreatePemdaIndikatorFileTable1689062368585');
INSERT INTO public.siagas_migrations VALUES (39, 1689087963096, 'CreateInovasiIndikatorTable1689087963096');
INSERT INTO public.siagas_migrations VALUES (40, 1689088128340, 'CreateInovasiIndikatorFileTable1689088128340');
INSERT INTO public.siagas_migrations VALUES (41, 1689089341041, 'AddCreatedByColumnToGovernmentInnovationsTable1689089341041');
INSERT INTO public.siagas_migrations VALUES (43, 1691075486821, 'CreateReviewInovasiDaerahTable1691075486821');
INSERT INTO public.siagas_migrations VALUES (44, 1691077843241, 'CreatePreviewReviewInovasiDaerahTable1691077843241');
INSERT INTO public.siagas_migrations VALUES (47, 1691078558362, 'CreateEvaluasiTable1691078558362');
INSERT INTO public.siagas_migrations VALUES (48, 1691464585896, 'UpdatePemdaIndikatorTable1691464585896');
INSERT INTO public.siagas_migrations VALUES (50, 1691567938146, 'CreateEvaluasiInovasiDaerahTable1691567938146');
INSERT INTO public.siagas_migrations VALUES (51, 1691572448206, 'AddDocumentCategoryToEvaluasiInovasi1691572448206');
INSERT INTO public.siagas_migrations VALUES (52, 1692015237330, 'AddPemdaIdToGovernmentInnovationTable1692015237330');
INSERT INTO public.siagas_migrations VALUES (53, 1692020072236, 'AddColumnToProfilPemdaTable1692020072236');
INSERT INTO public.siagas_migrations VALUES (55, 1692022444142, 'CreateViewRangkingIndex1692022444142');
INSERT INTO public.siagas_migrations VALUES (57, 1692027349102, 'CreateViewRekapIndeksAkhir1692027349102');
INSERT INTO public.siagas_migrations VALUES (60, 1692545438422, 'CreateViewArsip1692545438422');
INSERT INTO public.siagas_migrations VALUES (61, 1693024738812, 'CreateViewDashboard1693024738812');
INSERT INTO public.siagas_migrations VALUES (66, 1693037725871, 'UpdatePemdaIndikatorTable1693037725871');
INSERT INTO public.siagas_migrations VALUES (70, 1693212250713, 'CreatePeringkatHasilReviewView1693212250713');
INSERT INTO public.siagas_migrations VALUES (72, 1694232964339, 'CreateLaporanIndexksView1694232964339');
INSERT INTO public.siagas_migrations VALUES (73, 1694252198602, 'CreateLaporanJenisInovasiView1694252198602');
INSERT INTO public.siagas_migrations VALUES (74, 1694300098672, 'CreateLaporanBentukInovasiView1694300098672');
INSERT INTO public.siagas_migrations VALUES (75, 1694301097748, 'CreateLaporanInisiatorInovasiView1694301097748');
INSERT INTO public.siagas_migrations VALUES (76, 1694301935977, 'CreateLaporanUrusanInovasiView1694301935977');
INSERT INTO public.siagas_migrations VALUES (77, 1696776584663, 'UpdateEvaluasiInovasiDaerahTable1696776584663');
INSERT INTO public.siagas_migrations VALUES (78, 1696777532075, 'AddTentangToInovasiIndikatorFile1696777532075');
INSERT INTO public.siagas_migrations VALUES (79, 1696782503905, 'AddColumToPemdaIndikatorFile1696782503905');
INSERT INTO public.siagas_migrations VALUES (80, 1696784487557, 'AddColumnToInovasiIndikatorFile1696784487557');
INSERT INTO public.siagas_migrations VALUES (81, 1697387399831, 'AddFotoColumnInGovernmentInnovationTable1697387399831');
INSERT INTO public.siagas_migrations VALUES (82, 1697789648894, 'CreateDistrikTable1697789648894');
INSERT INTO public.siagas_migrations VALUES (83, 1699073022930, 'AddUpdateByInUserEntity1699073022930');
INSERT INTO public.siagas_migrations VALUES (84, 1699079012362, 'AddLabelInRole1699079012362');


--
-- Data for Name: tim_penilaian; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.tim_penilaian VALUES (1, 'Test Update', 'Test Update', 'Test Update', 'creator', 'creator', '2023-06-29 23:10:55.312503', '2023-06-29 23:10:55.312503');
INSERT INTO public.tim_penilaian VALUES (3, 'string', 'string', 'string', 'superadmin', 'superadmin', '2023-09-12 19:38:16.896866', '2023-09-12 19:38:16.896866');
INSERT INTO public.tim_penilaian VALUES (4, 'User ASN', 'Nama ASN', 'Ini Instansi', 'superadmin', 'superadmin', '2023-09-12 19:45:34.628825', '2023-09-12 19:45:34.628825');
INSERT INTO public.tim_penilaian VALUES (7, '123123123123', '123123123', '123123123123', 'superadmin', 'superadmin', '2023-09-12 20:14:32.001436', '2023-09-12 20:14:32.001436');
INSERT INTO public.tim_penilaian VALUES (8, 'Testing Validasi', 'Testing Validasi', 'Instansi Validasi', 'superadmin', 'superadmin', '2023-09-13 01:09:48.970408', '2023-09-13 01:09:48.970408');
INSERT INTO public.tim_penilaian VALUES (9, 'Test Staging', 'Test Staging', 'Test Staging', 'superadmin', 'superadmin', '2023-09-13 02:02:20.70977', '2023-09-13 02:02:20.70977');
INSERT INTO public.tim_penilaian VALUES (10, 'admin 2', 'admin', 'dinas pendidikan', 'superadmin', 'superadmin', '2023-09-14 23:55:41.623599', '2023-09-14 23:55:41.623599');


--
-- Data for Name: tuxedos; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.tuxedos VALUES (5, 'tes', 'tes', 'faq', '<p>tes</p>', '2023-09-15 12:47:39.781938', '2023-09-15 12:47:39.781938');


--
-- Data for Name: typeorm_metadata; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--



--
-- Data for Name: uptd; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.uptd VALUES (10, 1, 'Klaso', '2023-10-07 23:38:17.693991', 'superadmin', '2023-10-07 23:38:17.693991', 'superadmin');
INSERT INTO public.uptd VALUES (11, 1, 'Saengkeduk', '2023-10-07 23:38:42.044138', 'superadmin', '2023-10-07 23:38:42.044138', 'superadmin');
INSERT INTO public.uptd VALUES (12, 1, 'Makbon', '2023-10-07 23:38:51.821877', 'superadmin', '2023-10-07 23:38:51.821877', 'superadmin');
INSERT INTO public.uptd VALUES (13, 1, 'Klayili', '2023-10-07 23:39:06.456584', 'superadmin', '2023-10-07 23:39:06.456584', 'superadmin');
INSERT INTO public.uptd VALUES (15, 1, 'Beraur', '2023-10-07 23:39:30.471431', 'superadmin', '2023-10-07 23:39:30.471431', 'superadmin');
INSERT INTO public.uptd VALUES (16, 1, 'Klamono', '2023-10-07 23:39:42.654545', 'superadmin', '2023-10-07 23:39:42.654545', 'superadmin');
INSERT INTO public.uptd VALUES (17, 1, 'Klabot', '2023-10-07 23:39:51.360399', 'superadmin', '2023-10-07 23:39:51.360399', 'superadmin');
INSERT INTO public.uptd VALUES (18, 1, 'Klawak', '2023-10-07 23:40:00.719055', 'superadmin', '2023-10-07 23:40:00.719055', 'superadmin');
INSERT INTO public.uptd VALUES (19, 1, 'Bagun', '2023-10-07 23:40:08.243257', 'superadmin', '2023-10-07 23:40:08.243257', 'superadmin');
INSERT INTO public.uptd VALUES (20, 1, 'Klasafet', '2023-10-07 23:40:19.545765', 'superadmin', '2023-10-07 23:40:19.545765', 'superadmin');
INSERT INTO public.uptd VALUES (21, 1, 'Malabotom', '2023-10-07 23:40:33.149204', 'superadmin', '2023-10-07 23:40:33.149204', 'superadmin');
INSERT INTO public.uptd VALUES (22, 1, 'Botain', '2023-10-07 23:40:41.729949', 'superadmin', '2023-10-07 23:40:41.729949', 'superadmin');
INSERT INTO public.uptd VALUES (23, 1, 'Konhir', '2023-10-07 23:40:52.092767', 'superadmin', '2023-10-07 23:40:52.092767', 'superadmin');
INSERT INTO public.uptd VALUES (24, 1, 'Salawati', '2023-10-07 23:41:03.687953', 'superadmin', '2023-10-07 23:41:03.687953', 'superadmin');
INSERT INTO public.uptd VALUES (25, 1, 'Mayamuk', '2023-10-07 23:41:16.869707', 'superadmin', '2023-10-07 23:41:16.869707', 'superadmin');
INSERT INTO public.uptd VALUES (26, 1, 'Moisigin', '2023-10-07 23:41:36.085328', 'superadmin', '2023-10-07 23:41:36.085328', 'superadmin');
INSERT INTO public.uptd VALUES (27, 1, 'Hobard', '2023-10-07 23:41:45.269306', 'superadmin', '2023-10-07 23:41:45.269306', 'superadmin');
INSERT INTO public.uptd VALUES (28, 1, 'Buk', '2023-10-07 23:41:54.468331', 'superadmin', '2023-10-07 23:41:54.468331', 'superadmin');
INSERT INTO public.uptd VALUES (29, 1, 'Seget', '2023-10-07 23:42:02.762171', 'superadmin', '2023-10-07 23:42:02.762171', 'superadmin');
INSERT INTO public.uptd VALUES (30, 1, 'Segun', '2023-10-07 23:42:10.47379', 'superadmin', '2023-10-07 23:42:10.47379', 'superadmin');
INSERT INTO public.uptd VALUES (31, 1, 'Salawati Selatan', '2023-10-07 23:42:24.940214', 'superadmin', '2023-10-07 23:42:24.940214', 'superadmin');
INSERT INTO public.uptd VALUES (32, 1, 'Salawati TEngah', '2023-10-07 23:42:37.829317', 'superadmin', '2023-10-07 23:42:37.829317', 'superadmin');
INSERT INTO public.uptd VALUES (33, 1, 'Aimas', '2023-10-07 23:42:47.172143', 'superadmin', '2023-10-07 23:42:47.172143', 'superadmin');
INSERT INTO public.uptd VALUES (34, 1, 'Mariat', '2023-10-07 23:42:55.438385', 'superadmin', '2023-10-07 23:42:55.438385', 'superadmin');
INSERT INTO public.uptd VALUES (35, 1, 'Sorong', '2023-10-07 23:43:05.111893', 'superadmin', '2023-10-07 23:43:05.111893', 'superadmin');
INSERT INTO public.uptd VALUES (36, 1, 'Sayosa', '2023-10-07 23:43:12.064724', 'superadmin', '2023-10-07 23:43:12.064724', 'superadmin');
INSERT INTO public.uptd VALUES (37, 1, 'Maudus', '2023-10-07 23:43:23.863156', 'superadmin', '2023-10-07 23:43:23.863156', 'superadmin');
INSERT INTO public.uptd VALUES (38, 1, 'Wemak', '2023-10-07 23:43:34.460443', 'superadmin', '2023-10-07 23:43:34.460443', 'superadmin');
INSERT INTO public.uptd VALUES (39, 1, 'Sayosa Timur', '2023-10-07 23:43:44.237373', 'superadmin', '2023-10-07 23:43:44.237373', 'superadmin');
INSERT INTO public.uptd VALUES (40, 1, 'Sunook', '2023-10-07 23:43:53.660838', 'superadmin', '2023-10-07 23:43:53.660838', 'superadmin');
INSERT INTO public.uptd VALUES (41, 30, 'Selemkai', '2023-10-09 18:21:51.821975', 'superadmin', '2023-10-09 18:21:51.821975', 'superadmin');
INSERT INTO public.uptd VALUES (42, 30, 'Mega', '2023-10-09 18:22:14.712151', 'superadmin', '2023-10-09 18:22:14.712151', 'superadmin');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: koneksik_siagas.api
--

INSERT INTO public.users VALUES (60, 8, 'dpmptsp', '$2b$10$YOB.hTi19seh/8kOp5NshO.fFDpqn1AGGPwCS0XtF80.na4J2otEO', '2024-01-31 08:21:23.210441', '2024-01-31 08:21:23.210441', '  ', 'Ronald Sekerony', 'Ronald', 'DPMPTSP', NULL);
INSERT INTO public.users VALUES (61, 9, 'admin', '$2b$10$pC.25LOKuYCDIABjiNrG7ucDYu47tBLzeu.XDampPfuVuxbEH6BHK', '2024-01-31 08:43:19.651242', '2024-01-31 08:43:19.651242', 'admin@dummy.com', 'Admin', 'Admin', NULL, NULL);
INSERT INTO public.users VALUES (62, 8, 'baperlitbangkabsorong', '$2b$10$TxVVw5RilsFHI8vXhRVRD.BJ2CRjN0ds2EInB5OAJMxPoLc6nMlfG', '2024-02-08 20:59:04.546669', '2024-02-08 20:59:04.546669', 'iii', 'Christi Irjasari Timban', 'Sari', 'Baperlitbang', NULL);
INSERT INTO public.users VALUES (4, 8, 'user', '$2b$10$Q3obFHaKCTAXGsMiUrC3suzW4ykCigRInM82pNcLFE1FLIZRUXX.W', '2023-03-24 13:53:17.92093', '2023-03-24 13:53:17.92093', 'user@dummy.com', 'User', 'User', 'user', 2);
INSERT INTO public.users VALUES (43, 8, 'user3', '$2b$10$4MiN/7cijl3.0WDndluzUePbjUaT6WyhqTKlmSvv3/TCF7w2JRM96', '2023-09-15 12:45:42.95558', '2023-09-15 12:45:42.95558', 'user3Agmail.com', 'user3', 'user3', 'user3', 2);
INSERT INTO public.users VALUES (46, 8, 'user22', '$2b$10$HEe/BHHM34mxKi.AXgqMA.OroZdtQ8T.yLzmFHUgIUKyuRbnSd6D.', '2023-10-08 16:23:07.955254', '2023-10-08 16:23:07.955254', 'User22@gmail.com', 'user22', 'User22', 'User22', 2);
INSERT INTO public.users VALUES (47, 8, 'user111', '$2b$10$icGGh8cCQRYDRAy06NQJpuuUrEMoL6fqAOQIYq46msQ.44fp8UWli', '2023-10-09 08:43:23.148212', '2023-10-09 08:43:23.148212', 'User111@gmailcom', 'User111', 'User', 'User111', 2);
INSERT INTO public.users VALUES (48, 8, 'user33', '$2b$10$CCy9GX5lViUroJne1BQIp.0eNOPbz58dHQbtwGU5oNBlA19LSfeAK', '2023-10-09 08:44:53.096432', '2023-10-09 08:44:53.096432', 'user33@gmail.com', 'user33', 'user33', 'user33', 2);
INSERT INTO public.users VALUES (49, 8, 'user44', '$2b$10$m3oNCR2Rdq7MZxEr1A4VIe000Q671341FWXkOldP7V9q1aQ7VFMqS', '2023-10-09 08:45:32.642096', '2023-10-09 08:45:32.642096', 'user44@gmil.com', 'user44', 'user44', 'user44', 2);
INSERT INTO public.users VALUES (50, 8, 'user55', '$2b$10$i2Ptp9fbvNHGTHXocmBSkuwBooI8yD8ZNrRXH5xSQSUqbgtJBwo/.', '2023-10-09 08:45:56.857621', '2023-10-09 08:45:56.857621', 'user55@gmail.com', 'user55', 'user55', 'user55', 2);
INSERT INTO public.users VALUES (65, 8, 'dtphbunkabsorong', '$2b$10$yq0BL5NhOfJ1xpL3O4fypeQW8s2IzBBhh8hoqAwUcbeKdtr/r3yuC', '2024-02-08 21:05:22.529424', '2024-02-08 21:05:22.529424', 'iii@gmail.com', 'Maya Nurlita Asmi', 'Maya', 'Dinas Tanaman Pangan Holtikultura dan Perkebunan', NULL);
INSERT INTO public.users VALUES (1, 1, 'creator', '$2b$10$xQWsHTrhmW3YhUHMwKvIX.BSABo71J9C260SYDDYQsE3bVavOSkSW', '2023-03-23 12:27:51.051357', '2023-09-13 20:02:04.298725', 'creator@gmail.com', 'Creator', 'Creator', 'Creator', NULL);
INSERT INTO public.users VALUES (2, 2, 'superadmin', '$2b$10$AiL02MyHTOR74NWTs0zJT.6tR5UAvCguwlBguovh65FWz2fGOtm2y', '2023-03-23 12:27:51.051357', '2023-03-23 12:27:51.051357', 'superadmin@dummy.com', 'Super Admin', 'Super Admin', 'superadmin', 1);
INSERT INTO public.users VALUES (67, 8, 'lingkunganhidupkabsorong', '$2b$10$uuOc0w0OQrruy7/2K/XT3.AeszAhgUF1pWb.4upoWeiVP1XJRmypu', '2024-02-08 21:08:24.589299', '2024-02-08 21:08:24.589299', 'qq@gmail.com', 'Surya Arifin', 'Surya', 'Dinas Lingkungan Hidup', NULL);
INSERT INTO public.users VALUES (68, 8, 'perpustakaankabsorong', '$2b$10$sdlxg9ip1joY4uJ/mNhpn.w0HIW3tTFIhp0N8ABqbOfsxquJaxk8e', '2024-02-08 21:09:54.718397', '2024-02-08 21:09:54.718397', 'sarah@gmail.com', 'Wehelmina Sara Ubwarin', 'Sarah', 'Dinas Perpustakaan dan Kearsipan', NULL);
INSERT INTO public.users VALUES (69, 8, 'perumahankabsorong', '$2b$10$9qp186sntgvlt2ot/6DS5.u1pp5EDFYrggYLJPwc8J4GR5mAaDx46', '2024-02-08 21:11:39.220139', '2024-02-08 21:11:39.220139', 'fany@gmail.com', 'Fany Irawan Wailissa', 'Fany', 'Dinas Perumahan dan Kawasan Pemukiman', NULL);
INSERT INTO public.users VALUES (70, 8, 'perindakopkabsorong', '$2b$10$8E/G9MCmXlNkNPpU74av4.H9ew/nvQ7ejycPsYTH9InaGcdPNaA6S', '2024-02-08 21:13:24.504907', '2024-02-08 21:13:24.504907', 'achmad@gmail.com', 'Achamd Rondya', 'Achmad', 'Dinas Perindustrian, Perdagangan, Koperasi Usaha Kecil dan Menengah', NULL);
INSERT INTO public.users VALUES (71, 8, 'satpolppkabsorong', '$2b$10$Ny31rt7EnNREC46Bape/Ye3fKsFS7KE6uZ0CSUqxXbV/CTwOyw.ve', '2024-02-08 21:15:00.436731', '2024-02-08 21:15:00.436731', 'muh@gmail.com', 'Muhamad Bumburo', 'Muhamad', 'Satuan Polisi Pamong Praja', NULL);
INSERT INTO public.users VALUES (72, 8, 'disnakerkabsorong', '$2b$10$eIQAHF89iR4Hoia/fE9QaedHIOPRkKmt5I7Pl0u5oQJUnogVLkziu', '2024-02-08 21:16:23.987431', '2024-02-08 21:16:23.987431', 'sri@gmail.com', 'Sri Mumpuni', 'Sri', 'Dinas Tenaga Kerja dan Transmigrasi', NULL);
INSERT INTO public.users VALUES (73, 8, 'pendidikankabsorong', '$2b$10$BuUHOeWa1vnHn68G2adPXufSxCfR7RJeXj8vA3TX5jKrmzaZRbBvi', '2024-02-08 21:18:58.280041', '2024-02-08 21:18:58.280041', 'setyo@gmail.com', 'Setyo Untoro', 'Setyo', 'Dinas Pendidikan dan Kebudayaan ', NULL);
INSERT INTO public.users VALUES (74, 8, 'bkppdkabsorong', '$2b$10$iYWIzFReZOHfVkX0VSs7wuJRnJK4z3Z1w5Jw2Wp1LH90FTBxhmxqe', '2024-02-08 21:31:12.100197', '2024-02-08 21:31:12.100197', 'kristanto@gmail.com', 'Kristanto Mnaohonim', 'Kristanto', 'Badan Kepegawaian, Pendidikan dan Pelatihan Daerah', NULL);
INSERT INTO public.users VALUES (75, 8, 'peternakankabsorong', '$2b$10$6HvjCpufInRgMj9.rYuHKuAHMhjxfbjWZJZRuMAYl4u3Kc8pkH.mS', '2024-02-08 21:32:49.195108', '2024-02-08 21:32:49.195108', 'suralin@gmail.com', 'Suralin Rantelinggi', 'Suralin', 'Dinas Peternakan dan Kesehatan Hewan', NULL);
INSERT INTO public.users VALUES (76, 8, 'sekwankabsorong', '$2b$10$j6HiGXIFkUE7MoJ1AzylpumSJN7c9CT0Mna5SBQQSto/KuVPfjt36', '2024-02-08 21:35:35.284496', '2024-02-08 21:35:35.284496', 'alex@gmail.com', 'Alex Sorsery', 'Alex', 'Sekretariat DPRD', NULL);
INSERT INTO public.users VALUES (77, 8, 'pariwisatakabsorong', '$2b$10$PhUB7MGPyn/EEJseD/bqbOgkyALNCq8xJRWeqfc9FCkGWHK1G73rC', '2024-02-08 21:37:11.716802', '2024-02-08 21:37:11.716802', 'mieke@gmail.com', 'Mieke Tuhumury', 'Mieke', 'Dinas Pariwisata, Pemuda dan Olahraga', NULL);
INSERT INTO public.users VALUES (78, 8, 'dinsoskabsorong', '$2b$10$vK3N6yw09COhQJ.nTi4ogu/e9xKrKC3vvAWtVMssNtNXXi.mHGshe', '2024-02-08 21:38:44.810987', '2024-02-08 21:38:44.810987', 'nixon@gmail.com', 'Nixon Raymon Howay', 'Nixon', 'Dinas Sosial', NULL);
INSERT INTO public.users VALUES (79, 8, 'perikanankabsorong', '$2b$10$bPwhLgG0q.aYqS4G5xTkiOQIeysuFemQFu400199RAyVFACrUcYk.', '2024-02-12 13:30:20.505618', '2024-02-12 13:30:20.505618', 'yoelan@gmail.com', 'Yoelan Palemba', 'Yoelan', 'Dinas Perikanan', NULL);
INSERT INTO public.users VALUES (80, 8, 'ketapangkabsorong', '$2b$10$khj8xa.PPnbXaqKidnfenOzJ4NUUIMrc9RNq1wHzPuL.YTvqo0qru', '2024-02-12 13:33:35.264706', '2024-02-12 13:33:35.264706', 'hadiana@gmail.com', 'Hadiana Tandilondong', 'Hadiana', 'Dinas Ketahanan Pangan', NULL);


--
-- Name: announcements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.announcements_id_seq', 19, true);


--
-- Name: areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.areas_id_seq', 11, true);


--
-- Name: clusters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.clusters_id_seq', 1, false);


--
-- Name: distrik_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.distrik_id_seq', 4, true);


--
-- Name: document_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.document_categories_id_seq', 5, true);


--
-- Name: documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.documents_id_seq', 7, true);


--
-- Name: evaluasi_inovasi_daerah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.evaluasi_inovasi_daerah_id_seq', 17, true);


--
-- Name: evaluasi_review_inovasi_daerah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.evaluasi_review_inovasi_daerah_id_seq', 1, false);


--
-- Name: faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.faq_id_seq', 14, true);


--
-- Name: files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.files_id_seq', 117, true);


--
-- Name: government_indicators_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.government_indicators_id_seq', 1, false);


--
-- Name: government_innovations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.government_innovations_id_seq', 37, true);


--
-- Name: government_profiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.government_profiles_id_seq', 3, true);


--
-- Name: government_sectors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.government_sectors_id_seq', 16, true);


--
-- Name: indicator_scales_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.indicator_scales_id_seq', 1, false);


--
-- Name: indicators_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.indicators_id_seq', 40, true);


--
-- Name: inovasi_indikator_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.inovasi_indikator_file_id_seq', 13, true);


--
-- Name: inovasi_indikator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.inovasi_indikator_id_seq', 172, true);


--
-- Name: pakta_integritas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.pakta_integritas_id_seq', 5, true);


--
-- Name: pemda_indikator_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.pemda_indikator_file_id_seq', 20, true);


--
-- Name: pemda_indikator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.pemda_indikator_id_seq', 165, true);


--
-- Name: preview_review_inovasi_daerah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.preview_review_inovasi_daerah_id_seq', 4, true);


--
-- Name: profil_pemda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.profil_pemda_id_seq', 31, true);


--
-- Name: regional_apparatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.regional_apparatus_id_seq', 53, true);


--
-- Name: regions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.regions_id_seq', 9, true);


--
-- Name: review_inovasi_daerah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.review_inovasi_daerah_id_seq', 55, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.roles_id_seq', 13, true);


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.settings_id_seq', 8, true);


--
-- Name: siagas_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.siagas_migrations_id_seq', 84, true);


--
-- Name: tim_penilaian_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.tim_penilaian_id_seq', 10, true);


--
-- Name: tuxedos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.tuxedos_id_seq', 5, true);


--
-- Name: uptd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.uptd_id_seq', 42, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: koneksik_siagas.api
--

SELECT pg_catalog.setval('public.users_id_seq', 80, true);


--
-- Name: inovasi_indikator PK_0201cfe8e1e2f6d96e16f60f1ea; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator
    ADD CONSTRAINT "PK_0201cfe8e1e2f6d96e16f60f1ea" PRIMARY KEY (id);


--
-- Name: pakta_integritas PK_0209e1949bba71218b963a5bc3b; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pakta_integritas
    ADD CONSTRAINT "PK_0209e1949bba71218b963a5bc3b" PRIMARY KEY (id);


--
-- Name: siagas_migrations PK_13dbb087e85cc4afabf49a2e674; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.siagas_migrations
    ADD CONSTRAINT "PK_13dbb087e85cc4afabf49a2e674" PRIMARY KEY (id);


--
-- Name: inovasi_indikator_file PK_26c7ac96c55f7650010096e6e7c; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file
    ADD CONSTRAINT "PK_26c7ac96c55f7650010096e6e7c" PRIMARY KEY (id);


--
-- Name: tim_penilaian PK_34b842a3dd17c0f92ac4d83dd14; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.tim_penilaian
    ADD CONSTRAINT "PK_34b842a3dd17c0f92ac4d83dd14" PRIMARY KEY (id);


--
-- Name: profil_pemda PK_7d79554db0b5e0e57ab08324494; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.profil_pemda
    ADD CONSTRAINT "PK_7d79554db0b5e0e57ab08324494" PRIMARY KEY (id);


--
-- Name: users PK_a3ffb1c0c8416b9fc6f907b7433; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY (id);


--
-- Name: roles PK_c1433d71a4838793a49dcad46ab; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT "PK_c1433d71a4838793a49dcad46ab" PRIMARY KEY (id);


--
-- Name: pemda_indikator PK_c2efa0e99efe4b12e12211cf67e; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator
    ADD CONSTRAINT "PK_c2efa0e99efe4b12e12211cf67e" PRIMARY KEY (id);


--
-- Name: distrik PK_cca032a038e860eba0e95812107; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.distrik
    ADD CONSTRAINT "PK_cca032a038e860eba0e95812107" PRIMARY KEY (id);


--
-- Name: pemda_indikator_file PK_df4d3a10eae94c4b24e8506efe1; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file
    ADD CONSTRAINT "PK_df4d3a10eae94c4b24e8506efe1" PRIMARY KEY (id);


--
-- Name: users UQ_69b55fd0cf5e38e2a2960fb2547; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "UQ_69b55fd0cf5e38e2a2960fb2547" UNIQUE (nama_pemda);


--
-- Name: government_indicators UQ_cd977644c7e8eda88623f4d1ec3; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_indicators
    ADD CONSTRAINT "UQ_cd977644c7e8eda88623f4d1ec3" UNIQUE (gov_profile_id, indicator_id);


--
-- Name: announcements pk_announcement; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT pk_announcement PRIMARY KEY (id);


--
-- Name: areas pk_areas; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.areas
    ADD CONSTRAINT pk_areas PRIMARY KEY (id);


--
-- Name: cluster_details pk_cluster_details; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.cluster_details
    ADD CONSTRAINT pk_cluster_details PRIMARY KEY (cluster_id, region_id);


--
-- Name: clusters pk_clusters; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.clusters
    ADD CONSTRAINT pk_clusters PRIMARY KEY (id);


--
-- Name: documents pk_document; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT pk_document PRIMARY KEY (id);


--
-- Name: document_categories pk_document_categories; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.document_categories
    ADD CONSTRAINT pk_document_categories PRIMARY KEY (id);


--
-- Name: evaluasi_inovasi_daerah pk_evaluasi_inovasi_daerah; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_inovasi_daerah
    ADD CONSTRAINT pk_evaluasi_inovasi_daerah PRIMARY KEY (id);


--
-- Name: evaluasi_review_inovasi_daerah pk_evaluasi_review_inovasi_daerah; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_review_inovasi_daerah
    ADD CONSTRAINT pk_evaluasi_review_inovasi_daerah PRIMARY KEY (id);


--
-- Name: faq pk_faq; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.faq
    ADD CONSTRAINT pk_faq PRIMARY KEY (id);


--
-- Name: files pk_files; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT pk_files PRIMARY KEY (id);


--
-- Name: government_innovations pk_government_innovations; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT pk_government_innovations PRIMARY KEY (id);


--
-- Name: government_profiles pk_government_profiles; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_profiles
    ADD CONSTRAINT pk_government_profiles PRIMARY KEY (id);


--
-- Name: government_sectors pk_government_sectors; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_sectors
    ADD CONSTRAINT pk_government_sectors PRIMARY KEY (id);


--
-- Name: indicator_scales pk_indicator_scales; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.indicator_scales
    ADD CONSTRAINT pk_indicator_scales PRIMARY KEY (id);


--
-- Name: indicators pk_indicators; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.indicators
    ADD CONSTRAINT pk_indicators PRIMARY KEY (id);


--
-- Name: preview_review_inovasi_daerah pk_preview_review_inovasi_daerah; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.preview_review_inovasi_daerah
    ADD CONSTRAINT pk_preview_review_inovasi_daerah PRIMARY KEY (id);


--
-- Name: regional_apparatus pk_regional_apparatus; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regional_apparatus
    ADD CONSTRAINT pk_regional_apparatus PRIMARY KEY (id);


--
-- Name: regions pk_regions; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT pk_regions PRIMARY KEY (id);


--
-- Name: review_inovasi_daerah pk_review_inovasi_daerah; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.review_inovasi_daerah
    ADD CONSTRAINT pk_review_inovasi_daerah PRIMARY KEY (id);


--
-- Name: settings pk_settings; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT pk_settings PRIMARY KEY (id);


--
-- Name: tuxedos pk_tuxedos; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.tuxedos
    ADD CONSTRAINT pk_tuxedos PRIMARY KEY (id);


--
-- Name: uptd pk_uptd; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.uptd
    ADD CONSTRAINT pk_uptd PRIMARY KEY (id);


--
-- Name: settings unique_settings_key; Type: CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT unique_settings_key UNIQUE (key);


--
-- Name: fk_announcement_slug; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX fk_announcement_slug ON public.announcements USING btree (slug);


--
-- Name: fk_announcement_title; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX fk_announcement_title ON public.announcements USING btree (title);


--
-- Name: fk_files_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX fk_files_name ON public.files USING btree (name);


--
-- Name: fk_regional_apparatus_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX fk_regional_apparatus_name ON public.regional_apparatus USING btree (name);


--
-- Name: fk_tuxedos_slug; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX fk_tuxedos_slug ON public.tuxedos USING btree (slug);


--
-- Name: fk_tuxedos_title; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX fk_tuxedos_title ON public.tuxedos USING btree (title);


--
-- Name: idx_areas_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_areas_name ON public.areas USING btree (name);


--
-- Name: idx_government_sectors_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_government_sectors_name ON public.government_sectors USING btree (name);


--
-- Name: idx_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_name ON public.document_categories USING btree (name);


--
-- Name: idx_regions_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_regions_name ON public.regions USING btree (name);


--
-- Name: idx_roles_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_roles_name ON public.roles USING btree (name);


--
-- Name: idx_settings_key; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_settings_key ON public.settings USING btree (key);


--
-- Name: idx_slug; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_slug ON public.document_categories USING btree (slug);


--
-- Name: idx_uptd_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_uptd_name ON public.uptd USING btree (name);


--
-- Name: idx_users_email; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE UNIQUE INDEX idx_users_email ON public.users USING btree (email);


--
-- Name: idx_users_full_name; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_users_full_name ON public.users USING btree (full_name);


--
-- Name: idx_users_nickname; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE INDEX idx_users_nickname ON public.users USING btree (nickname);


--
-- Name: idx_users_username; Type: INDEX; Schema: public; Owner: koneksik_siagas.api
--

CREATE UNIQUE INDEX idx_users_username ON public.users USING btree (username);


--
-- Name: laporan_indeks _RETURN; Type: RULE; Schema: public; Owner: koneksik_siagas.api
--

CREATE OR REPLACE VIEW public.laporan_indeks AS
 SELECT pp.id AS pemda_id,
    pp.nama_daerah,
    pp.opd_yang_menangani,
    ( SELECT count(gi_1.id) AS count
           FROM public.government_innovations gi_1
          WHERE (gi_1.pemda_id = pp.id)) AS jumlah_inovasi,
    sum(
        CASE
            WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
            WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
            WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
            ELSE 0
        END) AS total_skor_mandiri,
        CASE
            WHEN (count(gi.id) > 0) THEN ((sum(
            CASE
                WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
                WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
                WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
                ELSE 0
            END))::double precision / (( SELECT count(gi_1.id) AS count
               FROM public.government_innovations gi_1
              WHERE (gi_1.pemda_id = pp.id)))::double precision)
            ELSE (0)::double precision
        END AS nilai_indeks,
    count(iif.id) AS total_file,
        CASE
            WHEN (count(iif.id) > 10) THEN 'Sangat Inovatif'::text
            WHEN ((count(iif.id) <= 10) AND (count(iif.id) >= 6)) THEN 'Inovatif'::text
            WHEN ((count(iif.id) <= 5) AND (count(iif.id) > 0)) THEN 'Kurang Inovatif'::text
            ELSE 'Tidak Inovatif'::text
        END AS predikat,
    row_number() OVER (ORDER BY
        CASE
            WHEN (count(gi.id) > 0) THEN ((sum(
            CASE
                WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
                WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
                WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
                ELSE 0
            END))::double precision / (( SELECT count(gi_1.id) AS count
               FROM public.government_innovations gi_1
              WHERE (gi_1.pemda_id = pp.id)))::double precision)
            ELSE (0)::double precision
        END DESC) AS indeks,
    pp.nominator,
    pp.created_by
   FROM (((public.profil_pemda pp
     LEFT JOIN public.government_innovations gi ON ((gi.pemda_id = pp.id)))
     LEFT JOIN public.inovasi_indikator ii ON ((ii.inovasi_id = gi.id)))
     LEFT JOIN public.inovasi_indikator_file iif ON ((iif.inovasi_indikator_id = ii.id)))
  GROUP BY pp.nama_daerah, pp.id;


--
-- Name: peringkat_hasil_review _RETURN; Type: RULE; Schema: public; Owner: koneksik_siagas.api
--

CREATE OR REPLACE VIEW public.peringkat_hasil_review AS
 SELECT pp.id,
    pp.nama_daerah,
    count(gi.id) AS jumlah_inovasi,
        CASE
            WHEN (lower((pp.nominator)::text) = 'ya'::text) THEN '1.00'::text
            ELSE '0.00'::text
        END AS isp,
    ( SELECT count(pp2.id) AS count
           FROM public.profil_pemda pp2) AS total_pemda,
        CASE
            WHEN (( SELECT sum(pi.skor_verifikasi) AS sum
               FROM public.pemda_indikator pi
              WHERE (pi.pemda_id = pp.id)) > (0)::numeric) THEN ( SELECT sum(pi.skor_verifikasi) AS sum
               FROM public.pemda_indikator pi
              WHERE (pi.pemda_id = pp.id))
            ELSE (0)::numeric
        END AS total_skor_verifikasi,
    ( SELECT ri.predikat
           FROM public.rangking_index ri
          WHERE (ri.id = pp.id)) AS predikat,
    pp.nominator,
    ( SELECT sum(pi.skor) AS sum
           FROM public.pemda_indikator pi
          WHERE (pi.pemda_id = pp.id)) AS skor,
    ( SELECT sum(pi.skor_verifikasi) AS sum
           FROM public.pemda_indikator pi
          WHERE (pi.pemda_id = pp.id)) AS skor_evaluasi,
    sum(
        CASE
            WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
            WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
            WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
            ELSE 0
        END) AS total_skor_mandiri,
    pp.created_by
   FROM (public.profil_pemda pp
     LEFT JOIN public.government_innovations gi ON ((gi.pemda_id = pp.id)))
  GROUP BY pp.id, pp.nama_daerah;


--
-- Name: rangking_index _RETURN; Type: RULE; Schema: public; Owner: koneksik_siagas.api
--

CREATE OR REPLACE VIEW public.rangking_index AS
 SELECT pp.id,
    pp.nama_daerah,
    ( SELECT count(gi_1.id) AS count
           FROM public.government_innovations gi_1
          WHERE (gi_1.pemda_id = pp.id)) AS jumlah_inovasi,
    sum(
        CASE
            WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
            WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
            WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
            ELSE 0
        END) AS total_skor_mandiri,
        CASE
            WHEN (count(gi.id) > 0) THEN ((sum(
            CASE
                WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
                WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
                WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
                ELSE 0
            END))::double precision / (( SELECT count(gi_1.id) AS count
               FROM public.government_innovations gi_1
              WHERE (gi_1.pemda_id = pp.id)))::double precision)
            ELSE (0)::double precision
        END AS nilai_indeks,
    count(iif.id) AS total_file,
        CASE
            WHEN (count(iif.id) > 10) THEN 'Sangat Inovatif'::text
            WHEN ((count(iif.id) <= 10) AND (count(iif.id) >= 6)) THEN 'Inovatif'::text
            WHEN ((count(iif.id) <= 5) AND (count(iif.id) > 0)) THEN 'Kurang Inovatif'::text
            ELSE 'Tidak Inovatif'::text
        END AS predikat,
    row_number() OVER (ORDER BY
        CASE
            WHEN (count(gi.id) > 0) THEN ((sum(
            CASE
                WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
                WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
                WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
                ELSE 0
            END))::double precision / (( SELECT count(gi_1.id) AS count
               FROM public.government_innovations gi_1
              WHERE (gi_1.pemda_id = pp.id)))::double precision)
            ELSE (0)::double precision
        END DESC) AS indeks,
    pp.nominator,
    pp.created_by
   FROM (((public.profil_pemda pp
     LEFT JOIN public.government_innovations gi ON ((gi.pemda_id = pp.id)))
     LEFT JOIN public.inovasi_indikator ii ON ((ii.inovasi_id = gi.id)))
     LEFT JOIN public.inovasi_indikator_file iif ON ((iif.inovasi_indikator_id = ii.id)))
  GROUP BY pp.nama_daerah, pp.id;


--
-- Name: rekap_indeks_akhir _RETURN; Type: RULE; Schema: public; Owner: koneksik_siagas.api
--

CREATE OR REPLACE VIEW public.rekap_indeks_akhir AS
 SELECT pp.id,
    pp.nama_daerah,
    ( SELECT count(gi_1.id) AS count
           FROM public.government_innovations gi_1
          WHERE (gi_1.pemda_id = pp.id)) AS jumlah_inovasi,
    sum(
        CASE
            WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
            WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
            WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
            ELSE 0
        END) AS total_skor_mandiri,
        CASE
            WHEN (count(gi.id) > 0) THEN ((sum(
            CASE
                WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
                WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
                WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
                ELSE 0
            END))::double precision / (( SELECT count(gi_1.id) AS count
               FROM public.government_innovations gi_1
              WHERE (gi_1.pemda_id = pp.id)))::double precision)
            ELSE (0)::double precision
        END AS nilai_indeks,
    count(iif.id) AS total_file,
        CASE
            WHEN (count(iif.id) > 10) THEN 120
            WHEN ((count(iif.id) <= 10) AND (count(iif.id) >= 6)) THEN 80
            WHEN ((count(iif.id) <= 5) AND (count(iif.id) > 0)) THEN 40
            ELSE 20
        END AS nilai_indeks_verifikasi,
        CASE
            WHEN (count(iif.id) > 10) THEN 'Sangat Inovatif'::text
            WHEN ((count(iif.id) <= 10) AND (count(iif.id) >= 6)) THEN 'Inovatif'::text
            WHEN ((count(iif.id) <= 5) AND (count(iif.id) > 0)) THEN 'Kurang Inovatif'::text
            ELSE 'Tidak Inovatif'::text
        END AS predikat,
    row_number() OVER (ORDER BY
        CASE
            WHEN (count(gi.id) > 0) THEN ((sum(
            CASE
                WHEN (lower((gi.innovation_phase)::text) = 'inisiatif'::text) THEN 50
                WHEN (lower((gi.innovation_phase)::text) = 'uji coba'::text) THEN 102
                WHEN (lower((gi.innovation_phase)::text) = 'penerapan'::text) THEN 105
                ELSE 0
            END))::double precision / (( SELECT count(gi_1.id) AS count
               FROM public.government_innovations gi_1
              WHERE (gi_1.pemda_id = pp.id)))::double precision)
            ELSE (0)::double precision
        END DESC) AS indeks,
    pp.nominator,
    pp.created_by
   FROM (((public.profil_pemda pp
     LEFT JOIN public.government_innovations gi ON ((gi.pemda_id = pp.id)))
     LEFT JOIN public.inovasi_indikator ii ON ((ii.inovasi_id = gi.id)))
     LEFT JOIN public.inovasi_indikator_file iif ON ((iif.inovasi_indikator_id = ii.id)))
  GROUP BY pp.nama_daerah, pp.id;


--
-- Name: inovasi_indikator_file FK_025c8bd7e171bdd0c1ea9f79f7d; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file
    ADD CONSTRAINT "FK_025c8bd7e171bdd0c1ea9f79f7d" FOREIGN KEY (file_id) REFERENCES public.files(id) ON DELETE CASCADE;


--
-- Name: pemda_indikator_file FK_040d2b80a2dfc976857cb41b745; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file
    ADD CONSTRAINT "FK_040d2b80a2dfc976857cb41b745" FOREIGN KEY (file_id) REFERENCES public.files(id) ON DELETE CASCADE;


--
-- Name: profil_pemda FK_0523adf6bb9e589677b5c0e8740; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.profil_pemda
    ADD CONSTRAINT "FK_0523adf6bb9e589677b5c0e8740" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: government_indicators FK_05f0c48e471e97498625601bac7; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_indicators
    ADD CONSTRAINT "FK_05f0c48e471e97498625601bac7" FOREIGN KEY (scale_id) REFERENCES public.indicator_scales(id);


--
-- Name: pakta_integritas FK_074798a33baacc8554b61be4c03; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pakta_integritas
    ADD CONSTRAINT "FK_074798a33baacc8554b61be4c03" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: evaluasi_review_inovasi_daerah FK_0b4f30ab689118ff1e98a5188e3; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_review_inovasi_daerah
    ADD CONSTRAINT "FK_0b4f30ab689118ff1e98a5188e3" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: pakta_integritas FK_11873710d643b624501458cd106; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pakta_integritas
    ADD CONSTRAINT "FK_11873710d643b624501458cd106" FOREIGN KEY (upload_id) REFERENCES public.files(id);


--
-- Name: pemda_indikator FK_1456ac11390eb1301586e7f9168; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator
    ADD CONSTRAINT "FK_1456ac11390eb1301586e7f9168" FOREIGN KEY (indikator_id) REFERENCES public.indicators(id) ON DELETE CASCADE;


--
-- Name: government_innovations FK_1c9bb9f97ffbfac396f72e6d82c; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT "FK_1c9bb9f97ffbfac396f72e6d82c" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: inovasi_indikator_file FK_1d9ba58cf3f19a7e161bee0585a; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file
    ADD CONSTRAINT "FK_1d9ba58cf3f19a7e161bee0585a" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: inovasi_indikator FK_210db13670a672773e616195e53; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator
    ADD CONSTRAINT "FK_210db13670a672773e616195e53" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: evaluasi_review_inovasi_daerah FK_22d977f61b68c4e017fe8889c26; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_review_inovasi_daerah
    ADD CONSTRAINT "FK_22d977f61b68c4e017fe8889c26" FOREIGN KEY (document_id) REFERENCES public.files(id) ON DELETE CASCADE;


--
-- Name: preview_review_inovasi_daerah FK_2804a37888c97f40df8c3399c25; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.preview_review_inovasi_daerah
    ADD CONSTRAINT "FK_2804a37888c97f40df8c3399c25" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: government_innovations FK_288e6b3a8c94af24062a2c7cfcf; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT "FK_288e6b3a8c94af24062a2c7cfcf" FOREIGN KEY (government_name) REFERENCES public.users(nama_pemda);


--
-- Name: profil_pemda FK_327caac8d657976ad3c04c6b718; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.profil_pemda
    ADD CONSTRAINT "FK_327caac8d657976ad3c04c6b718" FOREIGN KEY (document_id) REFERENCES public.files(id);


--
-- Name: inovasi_indikator FK_363b4bb7f8f0911f9beec59d22c; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator
    ADD CONSTRAINT "FK_363b4bb7f8f0911f9beec59d22c" FOREIGN KEY (inovasi_id) REFERENCES public.government_innovations(id) ON DELETE CASCADE;


--
-- Name: preview_review_inovasi_daerah FK_384d7942fda926ecb4ca19dccba; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.preview_review_inovasi_daerah
    ADD CONSTRAINT "FK_384d7942fda926ecb4ca19dccba" FOREIGN KEY (review_inovasi_daerah_id) REFERENCES public.review_inovasi_daerah(id) ON DELETE CASCADE;


--
-- Name: inovasi_indikator FK_3aa743f1991045cba7da3f29004; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator
    ADD CONSTRAINT "FK_3aa743f1991045cba7da3f29004" FOREIGN KEY (indikator_id) REFERENCES public.indicators(id) ON DELETE CASCADE;


--
-- Name: inovasi_indikator_file FK_440eca64ab5e763629d88aec253; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file
    ADD CONSTRAINT "FK_440eca64ab5e763629d88aec253" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: indicators FK_4a9907cd6c109db4f4743d76010; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.indicators
    ADD CONSTRAINT "FK_4a9907cd6c109db4f4743d76010" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: government_indicators FK_4f2053232ad9c6738bb496e9c51; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_indicators
    ADD CONSTRAINT "FK_4f2053232ad9c6738bb496e9c51" FOREIGN KEY (gov_profile_id) REFERENCES public.government_profiles(id);


--
-- Name: profil_pemda FK_53bb53f0056c6107e1a24b351ce; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.profil_pemda
    ADD CONSTRAINT "FK_53bb53f0056c6107e1a24b351ce" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: pemda_indikator_file FK_57cd4ba613e9be27b0eb32219fe; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file
    ADD CONSTRAINT "FK_57cd4ba613e9be27b0eb32219fe" FOREIGN KEY (indikator_id) REFERENCES public.indicators(id);


--
-- Name: review_inovasi_daerah FK_5dd4d8430ac8eb003d8b66a8ff8; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.review_inovasi_daerah
    ADD CONSTRAINT "FK_5dd4d8430ac8eb003d8b66a8ff8" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: pemda_indikator FK_606c4ace97eee883a99530e826f; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator
    ADD CONSTRAINT "FK_606c4ace97eee883a99530e826f" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: pemda_indikator_file FK_7c51a4955c75861cb75dc1f8b70; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file
    ADD CONSTRAINT "FK_7c51a4955c75861cb75dc1f8b70" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: evaluasi_inovasi_daerah FK_855349c9cdc63b6d5878b7d2f80; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_inovasi_daerah
    ADD CONSTRAINT "FK_855349c9cdc63b6d5878b7d2f80" FOREIGN KEY (inovasi_indikator_id) REFERENCES public.inovasi_indikator(id) ON DELETE CASCADE;


--
-- Name: government_innovations FK_968a0d30380752025979278dfcc; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT "FK_968a0d30380752025979278dfcc" FOREIGN KEY (government_sector_id) REFERENCES public.government_sectors(id);


--
-- Name: evaluasi_inovasi_daerah FK_aace0c20967b6c5664b0edfdf53; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_inovasi_daerah
    ADD CONSTRAINT "FK_aace0c20967b6c5664b0edfdf53" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: inovasi_indikator_file FK_ad719f3ec395107b6de3dda4393; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file
    ADD CONSTRAINT "FK_ad719f3ec395107b6de3dda4393" FOREIGN KEY (inovasi_id) REFERENCES public.government_innovations(id);


--
-- Name: pemda_indikator FK_b0a4adf7c2a69bb6b65bbd22ae9; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator
    ADD CONSTRAINT "FK_b0a4adf7c2a69bb6b65bbd22ae9" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: evaluasi_inovasi_daerah FK_b288a4ddf8baa9bf2b9ecde428b; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_inovasi_daerah
    ADD CONSTRAINT "FK_b288a4ddf8baa9bf2b9ecde428b" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: review_inovasi_daerah FK_b3c59e06af67d91ef4ddb62fa7b; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.review_inovasi_daerah
    ADD CONSTRAINT "FK_b3c59e06af67d91ef4ddb62fa7b" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: pakta_integritas FK_b4a6d2c3ce8426be3c2aefb4a4c; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pakta_integritas
    ADD CONSTRAINT "FK_b4a6d2c3ce8426be3c2aefb4a4c" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: inovasi_indikator_file FK_b4d577cc3bd9e223aec46eb4c36; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file
    ADD CONSTRAINT "FK_b4d577cc3bd9e223aec46eb4c36" FOREIGN KEY (indikator_id) REFERENCES public.indicators(id);


--
-- Name: pemda_indikator_file FK_b9c7f807ed35e665fa5e3a2502f; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file
    ADD CONSTRAINT "FK_b9c7f807ed35e665fa5e3a2502f" FOREIGN KEY (pemda_id) REFERENCES public.profil_pemda(id);


--
-- Name: inovasi_indikator_file FK_bc3f1ef8a6db9857f73f6f4934d; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator_file
    ADD CONSTRAINT "FK_bc3f1ef8a6db9857f73f6f4934d" FOREIGN KEY (inovasi_indikator_id) REFERENCES public.inovasi_indikator(id) ON DELETE CASCADE;


--
-- Name: review_inovasi_daerah FK_cb95158c8b0414606da75476781; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.review_inovasi_daerah
    ADD CONSTRAINT "FK_cb95158c8b0414606da75476781" FOREIGN KEY (inovasi_id) REFERENCES public.government_innovations(id) ON DELETE CASCADE;


--
-- Name: profil_pemda FK_ce897fe3f9da864e4ca66b228f4; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.profil_pemda
    ADD CONSTRAINT "FK_ce897fe3f9da864e4ca66b228f4" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: indicators FK_cea129c576af24a28042809d922; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.indicators
    ADD CONSTRAINT "FK_cea129c576af24a28042809d922" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: tim_penilaian FK_d1df0afea78ee5e9501d6b762bd; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.tim_penilaian
    ADD CONSTRAINT "FK_d1df0afea78ee5e9501d6b762bd" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: preview_review_inovasi_daerah FK_d845f716765a8a000544e7b007c; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.preview_review_inovasi_daerah
    ADD CONSTRAINT "FK_d845f716765a8a000544e7b007c" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: evaluasi_review_inovasi_daerah FK_e6cc0ecd00f208e799660bd4997; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_review_inovasi_daerah
    ADD CONSTRAINT "FK_e6cc0ecd00f208e799660bd4997" FOREIGN KEY (preview_review_inovasi_daerah_id) REFERENCES public.preview_review_inovasi_daerah(id) ON DELETE CASCADE;


--
-- Name: pakta_integritas FK_eafe16ce9febe8a929058c3b2f2; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pakta_integritas
    ADD CONSTRAINT "FK_eafe16ce9febe8a929058c3b2f2" FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: pemda_indikator FK_eb63a42c14f507d861cf7e5ef58; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator
    ADD CONSTRAINT "FK_eb63a42c14f507d861cf7e5ef58" FOREIGN KEY (pemda_id) REFERENCES public.profil_pemda(id) ON DELETE CASCADE;


--
-- Name: pemda_indikator_file FK_eca7e89c794ccc1546a911e0077; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file
    ADD CONSTRAINT "FK_eca7e89c794ccc1546a911e0077" FOREIGN KEY (created_by) REFERENCES public.users(username);


--
-- Name: government_indicators FK_ecf9cdd0c235e3f9a757429c12b; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_indicators
    ADD CONSTRAINT "FK_ecf9cdd0c235e3f9a757429c12b" FOREIGN KEY (indicator_id) REFERENCES public.indicators(id);


--
-- Name: tim_penilaian FK_f02b70a5f890f4f2d58ae5518de; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.tim_penilaian
    ADD CONSTRAINT "FK_f02b70a5f890f4f2d58ae5518de" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: evaluasi_review_inovasi_daerah FK_f116d5f0c6b2cd9b6849da1db4b; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.evaluasi_review_inovasi_daerah
    ADD CONSTRAINT "FK_f116d5f0c6b2cd9b6849da1db4b" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: pemda_indikator_file FK_f9ed22652ade128f5058f78b0b6; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.pemda_indikator_file
    ADD CONSTRAINT "FK_f9ed22652ade128f5058f78b0b6" FOREIGN KEY (pemda_indikator_id) REFERENCES public.pemda_indikator(id) ON DELETE CASCADE;


--
-- Name: inovasi_indikator FK_fd86cd23da2dfa6730ee1efb021; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.inovasi_indikator
    ADD CONSTRAINT "FK_fd86cd23da2dfa6730ee1efb021" FOREIGN KEY (updated_by) REFERENCES public.users(username);


--
-- Name: areas fk_areas_region_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.areas
    ADD CONSTRAINT fk_areas_region_id FOREIGN KEY (region_id) REFERENCES public.regions(id) ON DELETE CASCADE;


--
-- Name: government_innovations fk_budget; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT fk_budget FOREIGN KEY (budget_file_id) REFERENCES public.files(id) ON DELETE SET NULL;


--
-- Name: documents fk_category_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT fk_category_id FOREIGN KEY (category_id) REFERENCES public.document_categories(id) ON DELETE SET NULL;


--
-- Name: cluster_details fk_cluster_details_cluster_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.cluster_details
    ADD CONSTRAINT fk_cluster_details_cluster_id FOREIGN KEY (cluster_id) REFERENCES public.clusters(id) ON DELETE CASCADE;


--
-- Name: cluster_details fk_cluster_details_region_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.cluster_details
    ADD CONSTRAINT fk_cluster_details_region_id FOREIGN KEY (region_id) REFERENCES public.regions(id) ON DELETE CASCADE;


--
-- Name: users fk_created_by_users; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_created_by_users FOREIGN KEY (created_by) REFERENCES public.users(id) ON DELETE SET NULL;


--
-- Name: documents fk_document_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT fk_document_id FOREIGN KEY (document_id) REFERENCES public.files(id) ON DELETE SET NULL;


--
-- Name: government_profiles fk_file_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_profiles
    ADD CONSTRAINT fk_file_id FOREIGN KEY (file_id) REFERENCES public.files(id) ON DELETE SET NULL;


--
-- Name: announcements fk_files; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT fk_files FOREIGN KEY (file_id) REFERENCES public.files(id);


--
-- Name: government_innovations fk_foto; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT fk_foto FOREIGN KEY (foto_id) REFERENCES public.files(id);


--
-- Name: indicator_scales fk_indicator_scales_indicator_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.indicator_scales
    ADD CONSTRAINT fk_indicator_scales_indicator_id FOREIGN KEY (indicator_id) REFERENCES public.indicators(id) ON DELETE CASCADE;


--
-- Name: government_innovations fk_pemda_id_government_innovations; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT fk_pemda_id_government_innovations FOREIGN KEY (pemda_id) REFERENCES public.profil_pemda(id) ON DELETE CASCADE;


--
-- Name: government_innovations fk_profile; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_innovations
    ADD CONSTRAINT fk_profile FOREIGN KEY (profile_file_id) REFERENCES public.files(id) ON DELETE SET NULL;


--
-- Name: government_profiles fk_region_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.government_profiles
    ADD CONSTRAINT fk_region_id FOREIGN KEY (region_id) REFERENCES public.regions(id) ON DELETE SET NULL;


--
-- Name: regional_apparatus fk_regional_apparatus_created_by; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regional_apparatus
    ADD CONSTRAINT fk_regional_apparatus_created_by FOREIGN KEY (created_by) REFERENCES public.users(username) ON DELETE SET NULL;


--
-- Name: regional_apparatus fk_regional_apparatus_updated_by; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regional_apparatus
    ADD CONSTRAINT fk_regional_apparatus_updated_by FOREIGN KEY (updated_by) REFERENCES public.users(username) ON DELETE SET NULL;


--
-- Name: regional_apparatus fk_uptd_created_by; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regional_apparatus
    ADD CONSTRAINT fk_uptd_created_by FOREIGN KEY (created_by) REFERENCES public.users(username) ON DELETE SET NULL;


--
-- Name: uptd fk_uptd_regional_apparatus; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.uptd
    ADD CONSTRAINT fk_uptd_regional_apparatus FOREIGN KEY (regional_apparatus_id) REFERENCES public.regional_apparatus(id) ON DELETE CASCADE;


--
-- Name: regional_apparatus fk_uptd_updated_by; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.regional_apparatus
    ADD CONSTRAINT fk_uptd_updated_by FOREIGN KEY (updated_by) REFERENCES public.users(username) ON DELETE SET NULL;


--
-- Name: users fk_users_role_id; Type: FK CONSTRAINT; Schema: public; Owner: koneksik_siagas.api
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_users_role_id FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

